package learn.hibern.jpwh.jpwh07.associations.manytoone.orphanremoval;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class OrItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @OneToMany(mappedBy = "item",
            cascade = CascadeType.PERSIST,
            orphanRemoval = true)       // Includes CascadeType.REMOVE
    protected Set<OrBid> bids = new HashSet<>();

    public OrItem() {
    }

    public OrItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<OrBid> getBids() {
        return bids;
    }

    public void setBids(Set<OrBid> bids) {
        this.bids = bids;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrItem)) return false;

        OrItem mtoUdItem = (OrItem) o;

        if (!Objects.equals(id, mtoUdItem.id)) return false;
        return Objects.equals(name, mtoUdItem.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
