package learn.hibern.jpwh.jpwh07.associations.manytoone.orphanremoval;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
public class OrBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITEM_ID", nullable = false)
    protected OrItem item;

    @ManyToOne
    protected OrUser bidder;

    public OrBid() {
    }

    public OrBid(@NotNull BigDecimal amount, OrItem item, OrUser bidder) {
        this.amount = amount;
        this.item = item;
        item.getBids().add(this);
        this.bidder = bidder;
        bidder.getBids().add(this);
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public OrItem getItem() {
        return item;
    }

    public void setItem(OrItem item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrBid)) return false;

        OrBid orBid = (OrBid) o;

        if (getId() != null ? !getId().equals(orBid.getId()) : orBid.getId() != null) return false;
        if (getAmount() != null ? !getAmount().equals(orBid.getAmount()) : orBid.getAmount() != null) return false;
        if (getItem() != null ? !getItem().equals(orBid.getItem()) : orBid.getItem() != null) return false;
        return Objects.equals(bidder, orBid.bidder);
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getAmount() != null ? getAmount().hashCode() : 0);
        result = 31 * result + (getItem() != null ? getItem().hashCode() : 0);
        result = 31 * result + (bidder != null ? bidder.hashCode() : 0);
        return result;
    }
}
