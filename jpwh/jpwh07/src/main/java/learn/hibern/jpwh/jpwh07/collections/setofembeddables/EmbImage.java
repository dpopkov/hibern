package learn.hibern.jpwh.jpwh07.collections.setofembeddables;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Embeddable component capturing all the properties of an image.
 */
@Embeddable
public class EmbImage {
    @Column(nullable = false)
    protected String title;
    @Column(nullable = false)
    protected String filename;
    protected int width;
    protected int height;

    public EmbImage() {
    }

    public EmbImage(String title, String filename, int width, int height) {
        this.title = title;
        this.filename = filename;
        this.width = width;
        this.height = height;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmbImage embImage = (EmbImage) o;

        if (width != embImage.width) return false;
        if (height != embImage.height) return false;
        if (!title.equals(embImage.title)) return false;
        return filename.equals(embImage.filename);
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + filename.hashCode();
        result = 31 * result + width;
        result = 31 * result + height;
        return result;
    }
}
