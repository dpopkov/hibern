package learn.hibern.jpwh.jpwh07.collections.embeddablesetofstrings;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EsosUser {
    @Id
    @GeneratedValue
    protected Long id;
    protected String username;
    protected EsosAddress homeAddress;

    public EsosUser() {
    }

    public EsosUser(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public EsosAddress getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(EsosAddress homeAddress) {
        this.homeAddress = homeAddress;
    }
}
