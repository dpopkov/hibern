package learn.hibern.jpwh.jpwh07.associations.manytoone.orphanremoval;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class OrUser {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @OneToMany(mappedBy = "bidder")
    protected Set<OrBid> bids = new HashSet<>();

    protected OrUser() {
    }

    public OrUser(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<OrBid> getBids() {
        return bids;
    }
}
