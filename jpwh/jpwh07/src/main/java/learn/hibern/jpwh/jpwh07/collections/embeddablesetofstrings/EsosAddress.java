package learn.hibern.jpwh.jpwh07.collections.embeddablesetofstrings;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Address containing embeddable set of strings.
 */
@Embeddable
public class EsosAddress {
    @NotNull
    @Column(nullable = false)
    protected String street;
    @NotNull
    @Column(nullable = false, length = 5)
    protected String zipcode;
    @NotNull
    @Column(nullable = false)
    protected String city;
    @ElementCollection
    @CollectionTable(
            name = "CONTACT",                               // defaults to USER_CONTACTS
            joinColumns = @JoinColumn(name = "USER_ID"))    // Default
    @Column(name = "NAME", nullable = false)                // Defaults to CONTACTS
    protected Set<String> contacts = new HashSet<>();

    public EsosAddress() {
    }

    public EsosAddress(@NotNull String street, @NotNull String zipcode, @NotNull String city) {
        this.street = street;
        this.zipcode = zipcode;
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Set<String> getContacts() {
        return contacts;
    }

    public void setContacts(Set<String> contacts) {
        this.contacts = contacts;
    }
}
