package learn.hibern.jpwh.jpwh07.collections.mapofembeddables;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * Item containing map of Embeddable objects as keys.
 */
@Entity
@Table(name = "MOE_ITEMS")
public class MoeItem {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected String name;

    @ElementCollection
    @CollectionTable(name = "MOE_IMAGES")
    protected Map<MoeFilename, MoeImage> images = new HashMap<>();

    public MoeItem() {
    }

    public MoeItem(@NotNull String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<MoeFilename, MoeImage> getImages() {
        return images;
    }


    public void addImage(MoeFilename fname, MoeImage image) {
        images.put(fname, image);
    }

    public boolean containsImage(MoeFilename fname) {
        return images.containsKey(fname);
    }
}
