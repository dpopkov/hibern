package learn.hibern.jpwh.jpwh07.associations.manytoone.bidirectional;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
public class MtoBdBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITEM_ID", nullable = false)
    protected MtoBdItem item;

    public MtoBdBid() {
    }

    public MtoBdBid(@NotNull BigDecimal amount, MtoBdItem item) {
        this.amount = amount;
        this.item = item;
        item.getBids().add(this);
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public MtoBdItem getItem() {
        return item;
    }

    public void setItem(MtoBdItem item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MtoBdBid)) return false;

        MtoBdBid mtoBdBid = (MtoBdBid) o;

        if (!Objects.equals(id, mtoBdBid.id)) return false;
        return amount.equals(mtoBdBid.amount);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + amount.hashCode();
        return result;
    }
}
