package learn.hibern.jpwh.jpwh07.associations.manytoone.unidirectional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class MtoUdItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    public MtoUdItem() {
    }

    public MtoUdItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MtoUdItem)) return false;

        MtoUdItem mtoUdItem = (MtoUdItem) o;

        if (!Objects.equals(id, mtoUdItem.id)) return false;
        return Objects.equals(name, mtoUdItem.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
