package learn.hibern.jpwh.jpwh07.collections.listofstrings;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ItemWithList {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected String name;

    @ElementCollection
    @CollectionTable(name = "IMAGES")
    @OrderColumn
    @Column(name = "FILENAME")
    protected List<String> images = new ArrayList<>();

    public ItemWithList() {
    }

    public ItemWithList(@NotNull String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
