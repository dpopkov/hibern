package learn.hibern.jpwh.jpwh07.collections.mapofstringsembeddables;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * Embeddable component capturing all the properties of an image.
 */
@Embeddable
public class MapStrImage {
    @NotNull
    @Column(nullable = false)
    protected String title;
    @NotNull
    @Transient  // The filename of the image must be persisted as a key in MapStrItem's images map.
    protected String filename;
    protected int width;
    protected int height;

    public MapStrImage() {
    }

    public MapStrImage(String title, String filename, int width, int height) {
        this.title = title;
        this.filename = filename;
        this.width = width;
        this.height = height;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MapStrImage embImage = (MapStrImage) o;

        if (width != embImage.width) return false;
        if (height != embImage.height) return false;
        if (!title.equals(embImage.title)) return false;
        return filename.equals(embImage.filename);
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + filename.hashCode();
        result = 31 * result + width;
        result = 31 * result + height;
        return result;
    }
}
