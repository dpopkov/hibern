package learn.hibern.jpwh.jpwh07.collections.setofembeddables;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "EMB_ITEMS")
public class EmbItem {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected String name;

    @ElementCollection
    @CollectionTable(name = "EMB_IMAGES")
    @AttributeOverride(
            name = "filename",
            column = @Column(name = "FNAME", nullable = false)
    )
    protected Set<EmbImage> images = new HashSet<>();

    public EmbItem() {
    }

    public EmbItem(@NotNull String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<EmbImage> getImages() {
        return images;
    }
}
