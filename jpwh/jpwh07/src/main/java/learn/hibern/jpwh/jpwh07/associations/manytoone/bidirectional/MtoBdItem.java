package learn.hibern.jpwh.jpwh07.associations.manytoone.bidirectional;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class MtoBdItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @OneToMany(mappedBy = "item", fetch = FetchType.LAZY)
    protected Set<MtoBdBid> bids = new HashSet<>();

    public MtoBdItem() {
    }

    public MtoBdItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MtoBdBid> getBids() {
        return bids;
    }

    public void setBids(Set<MtoBdBid> bids) {
        this.bids = bids;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MtoBdItem)) return false;

        MtoBdItem mtoUdItem = (MtoBdItem) o;

        if (!Objects.equals(id, mtoUdItem.id)) return false;
        return Objects.equals(name, mtoUdItem.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
