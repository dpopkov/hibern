package learn.hibern.jpwh.jpwh07.associations.manytoone.unidirectional;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
public class MtoUdBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITEM_ID", nullable = false)
    protected MtoUdItem item;

    public MtoUdBid() {
    }

    public MtoUdBid(@NotNull BigDecimal amount, MtoUdItem item) {
        this.amount = amount;
        this.item = item;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public MtoUdItem getItem() {
        return item;
    }

    public void setItem(MtoUdItem item) {
        this.item = item;
    }
}
