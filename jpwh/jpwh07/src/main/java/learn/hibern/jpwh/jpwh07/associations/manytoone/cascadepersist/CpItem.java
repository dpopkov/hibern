package learn.hibern.jpwh.jpwh07.associations.manytoone.cascadepersist;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class CpItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @OneToMany(mappedBy = "item", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    protected Set<CpBid> bids = new HashSet<>();

    public CpItem() {
    }

    public CpItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CpBid> getBids() {
        return bids;
    }

    public void setBids(Set<CpBid> bids) {
        this.bids = bids;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CpItem)) return false;

        CpItem mtoUdItem = (CpItem) o;

        if (!Objects.equals(id, mtoUdItem.id)) return false;
        return Objects.equals(name, mtoUdItem.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
