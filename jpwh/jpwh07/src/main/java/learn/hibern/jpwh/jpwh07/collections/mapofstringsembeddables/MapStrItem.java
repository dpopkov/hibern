package learn.hibern.jpwh.jpwh07.collections.mapofstringsembeddables;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "MAP_STR_ITEMS")
public class MapStrItem {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected String name;

    @ElementCollection
    @CollectionTable(name = "MAP_STR_IMAGES")
    @MapKeyColumn(name = "FILENAME")
    protected Map<String, MapStrImage> images = new HashMap<>();

    public MapStrItem() {
    }

    public MapStrItem(@NotNull String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, MapStrImage> getImages() {
        return images;
    }

    public void addImage(MapStrImage image) {
        images.put(image.getFilename(), image);
    }

    public boolean containsImage(MapStrImage image) {
        return images.containsKey(image.getFilename());
    }
}
