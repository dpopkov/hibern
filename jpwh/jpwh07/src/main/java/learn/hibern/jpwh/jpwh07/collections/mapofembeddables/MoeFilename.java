package learn.hibern.jpwh.jpwh07.collections.mapofembeddables;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Filename that is used as key in {@link MoeItem}
 */
@Embeddable
public class MoeFilename {
    @Column(name = "FILENAME", nullable = false)
    protected String name;
    @Column(nullable = false)
    protected String extension;

    public MoeFilename() {
    }

    public MoeFilename(String name, String extension) {
        this.name = name;
        this.extension = extension;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoeFilename filename = (MoeFilename) o;

        if (!name.equals(filename.name)) return false;
        return extension.equals(filename.extension);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + extension.hashCode();
        return result;
    }
}
