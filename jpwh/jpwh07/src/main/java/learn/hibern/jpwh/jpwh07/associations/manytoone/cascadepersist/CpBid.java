package learn.hibern.jpwh.jpwh07.associations.manytoone.cascadepersist;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
public class CpBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITEM_ID", nullable = false)
    protected CpItem item;

    public CpBid() {
    }

    public CpBid(@NotNull BigDecimal amount, CpItem item) {
        this.amount = amount;
        this.item = item;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public CpItem getItem() {
        return item;
    }

    public void setItem(CpItem item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CpBid)) return false;

        CpBid mtoBdBid = (CpBid) o;

        if (!Objects.equals(id, mtoBdBid.id)) return false;
        return amount.equals(mtoBdBid.amount);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + amount.hashCode();
        return result;
    }
}
