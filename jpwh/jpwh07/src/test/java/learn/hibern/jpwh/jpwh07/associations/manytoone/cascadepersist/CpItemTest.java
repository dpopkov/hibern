package learn.hibern.jpwh.jpwh07.associations.manytoone.cascadepersist;

import learn.hibern.jpwh.jpwh07.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.testng.Assert.*;

public class CpItemTest {
    private CpItem item;
    private CpBid bid1;
    private CpBid bid2;
    private EntityManager entityManager;

    @BeforeMethod
    public void setup() {
        item = new CpItem("item-name");
        bid1 = new CpBid(BigDecimal.valueOf(1, 2), item);
        bid2 = new CpBid(BigDecimal.valueOf(10, 2), item);
    }

    @Test
    public void testPersistAndFind() {
        final Long itemId = persistTestData();

        beginTransaction();
        CpItem foundItem = entityManager.find(CpItem.class, itemId);
        final Set<CpBid> bids = foundItem.getBids();
        assertEquals(bids.size(), 2);
        assertTrue(bids.contains(bid1));
        assertTrue(bids.contains(bid2));
        commitTransactionAndClose();
    }

    @Test
    public void testRemove() {
        final Long itemId = persistTestData();

        beginTransaction();
        CpItem foundItem = entityManager.find(CpItem.class, itemId);
        Set<CpBid> bids = foundItem.getBids();
        final List<Long> bidIds = bids.stream().map(CpBid::getId).collect(Collectors.toList());
        commitTransactionAndClose();
        bidIds.forEach(Assert::assertNotNull);
        System.out.println("Bid's ID are saved");

        beginTransaction();
        foundItem = entityManager.find(CpItem.class, itemId);
        entityManager.remove(foundItem);
        commitTransactionAndClose();

        beginTransaction();
        bidIds.forEach(id -> {
            CpBid bid = entityManager.find(CpBid.class, id);
            assertNull(bid);
        });
        commitTransactionAndClose();
    }

    @Test
    public void testRemoveByReference() {
        final Long itemId = persistTestData();

        beginTransaction();
        CpItem foundItem = entityManager.find(CpItem.class, itemId);
        Set<CpBid> bids = foundItem.getBids();
        final List<Long> bidIds = bids.stream().map(CpBid::getId).collect(Collectors.toList());
        commitTransactionAndClose();
        bidIds.forEach(Assert::assertNotNull);
        System.out.println("Bid's ID are saved");

        beginTransaction();
        CpItem itemRef = entityManager.getReference(CpItem.class, itemId);
        System.out.println("Item reference is read");
        entityManager.remove(itemRef);
        commitTransactionAndClose();

        beginTransaction();
        bidIds.forEach(id -> {
            CpBid bid = entityManager.find(CpBid.class, id);
            assertNull(bid);
        });
        commitTransactionAndClose();
    }

    private Long persistTestData() {
        beginTransaction();
        entityManager.persist(item);
        item.getBids().add(bid1);
        item.getBids().add(bid2);
        commitTransactionAndClose();
        return item.getId();
    }

    private void beginTransaction() {
        entityManager = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        entityManager.getTransaction().begin();
    }

    private void commitTransactionAndClose() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
