package learn.hibern.jpwh.jpwh07.associations.manytoone.bidirectional;

import learn.hibern.jpwh.jpwh07.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import java.math.BigDecimal;
import java.util.Set;

import static org.testng.Assert.*;

public class MtoBdItemTest {

    @Test
    public void testPersistAndFind() {
        MtoBdItem item = new MtoBdItem("item-name");
        MtoBdBid bid1 = new MtoBdBid(BigDecimal.valueOf(1, 2), item);
        MtoBdBid bid2 = new MtoBdBid(BigDecimal.valueOf(10, 2), item);

        EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(item);
        final Long itemId = item.getId();
        em.persist(bid1);
        em.persist(bid2);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        MtoBdItem foundItem = em.find(MtoBdItem.class, itemId);
        final Set<MtoBdBid> bids = foundItem.getBids();
        assertEquals(bids.size(), 2);
        assertTrue(bids.contains(bid1));
        assertTrue(bids.contains(bid2));
        em.getTransaction().commit();
        em.close();
    }
}
