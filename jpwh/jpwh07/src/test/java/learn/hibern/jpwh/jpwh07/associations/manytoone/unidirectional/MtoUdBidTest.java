package learn.hibern.jpwh.jpwh07.associations.manytoone.unidirectional;

import learn.hibern.jpwh.jpwh07.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.math.BigDecimal;
import java.util.List;

import static org.testng.Assert.*;

public class MtoUdBidTest {

    @Test
    public void testPersistAndFind() {
        MtoUdItem item = new MtoUdItem("item-name");
        MtoUdBid bid1 = new MtoUdBid(BigDecimal.ONE, item);
        MtoUdBid bid2 = new MtoUdBid(BigDecimal.TEN, item);

        EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(item);
        Long itemId = item.getId();
        em.persist(bid1);
        Long bid1Id = bid1.getId();
        em.persist(bid2);
        Long bid2Id = bid2.getId();
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        MtoUdItem foundItem = em.find(MtoUdItem.class, itemId);
        assertEquals(foundItem.getName(), "item-name");
        MtoUdBid foundBid1 = em.find(MtoUdBid.class, bid1Id);
        MtoUdBid foundBid2 = em.find(MtoUdBid.class, bid2Id);
        assertEquals(foundBid1.getItem(), foundItem);
        assertEquals(foundBid2.getItem(), foundItem);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        final Query query = em.createQuery("select b from MtoUdBid b where b.item=:itemParameter");
        query.setParameter("itemParameter", foundItem);
        final List<?> list = query.getResultList();
        assertEquals(list.size(), 2);
        em.getTransaction().commit();
        em.close();
    }
}
