package learn.hibern.jpwh.jpwh07.collections.mapofembeddables;

import learn.hibern.jpwh.jpwh07.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.*;

public class MoeItemTest {

    @Test
    public void testPersistAndFind() {
        MoeImage img1 = new MoeImage("title-1", 10, 20);
        MoeImage img2 = new MoeImage("title-2", 30, 40);
        MoeFilename fname1 = new MoeFilename("fname-1", "png");
        MoeFilename fname2 = new MoeFilename("fname-2", "png");
        MoeItem item = new MoeItem("item-name");
        item.addImage(fname1, img1);
        item.addImage(fname2, img2);

        EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(item);
        final Long itemId = item.getId();
        assertNotNull(itemId);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        MoeItem foundItem = em.find(MoeItem.class, itemId);
        assertNotNull(foundItem);
        assertEquals(foundItem.getImages().size(), 2);
        assertTrue(foundItem.containsImage(fname1));
        assertTrue(foundItem.containsImage(fname2));
        MoeImage img3 = new MoeImage("title-3", 50, 60);
        MoeFilename fname3 = new MoeFilename("fname-3", "gif");
        foundItem.addImage(fname3, img3);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        foundItem = em.find(MoeItem.class, itemId);
        assertEquals(foundItem.getImages().size(), 3);
        assertTrue(foundItem.containsImage(fname3));
        em.getTransaction().commit();
        em.close();
    }
}
