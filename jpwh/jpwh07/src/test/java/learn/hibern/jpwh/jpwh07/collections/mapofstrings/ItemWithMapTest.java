package learn.hibern.jpwh.jpwh07.collections.mapofstrings;

import learn.hibern.jpwh.jpwh07.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.util.Map;

import static org.testng.Assert.*;

public class ItemWithMapTest {

    @Test
    public void testPersistAndFind() {
        ItemWithMap item = new ItemWithMap("item-name");
        item.getImages().put("image-filename-1", "image-1");
        item.getImages().put("image-filename-2", "image-2");

        EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(item);
        Long itemId = item.getId();
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        final ItemWithMap foundItem = em.find(ItemWithMap.class, itemId);
        assertNotNull(foundItem);
        assertEquals(foundItem.getName(), "item-name");
        final Map<String, String> images = foundItem.getImages();
        assertEquals(images.size(), 2);
        assertEquals(images.get("image-filename-1"), "image-1");
        assertEquals(images.get("image-filename-2"), "image-2");
        em.getTransaction().commit();
        em.close();
    }
}
