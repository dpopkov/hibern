package learn.hibern.jpwh.jpwh07.collections.setofstrings;

import learn.hibern.jpwh.jpwh07.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import java.util.Set;

import static org.testng.Assert.*;

public class ItemTest {

    @Test
    public void testPersistAndFind() {
        Item item = new Item("item-name");
        item.getImages().add("image-1");
        item.getImages().add("image-2");

        EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(item);
        Long itemId = item.getId();
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        final Item foundItem = em.find(Item.class, itemId);
        assertNotNull(foundItem);
        assertEquals(foundItem.getName(), "item-name");
        final Set<String> images = foundItem.getImages();
        assertEquals(images.size(), 2);
        assertTrue(images.contains("image-1"));
        assertTrue(images.contains("image-2"));
        em.getTransaction().commit();
        em.close();
    }
}
