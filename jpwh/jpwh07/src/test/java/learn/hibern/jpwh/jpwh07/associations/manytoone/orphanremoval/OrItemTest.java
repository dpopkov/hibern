package learn.hibern.jpwh.jpwh07.associations.manytoone.orphanremoval;

import learn.hibern.jpwh.jpwh07.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.testng.Assert.*;

public class OrItemTest {
    private static final BigDecimal SECOND_AMOUNT = BigDecimal.valueOf(10, 2);

    private OrUser bidder;
    private OrItem item;
    private OrBid bid1;
    private OrBid bid2;
    private EntityManager entityManager;
    private Long bidderId;
    private Long itemId;

    @BeforeMethod
    public void setup() {
        bidder = new OrUser("Jane Doe");
        item = new OrItem("item-name");
        bid1 = new OrBid(BigDecimal.valueOf(1, 2), item, bidder);
        bid2 = new OrBid(SECOND_AMOUNT, item, bidder);
    }

    @Test
    public void testRemove() {
        persistTestData();

        beginTransaction();
        OrItem foundItem = entityManager.find(OrItem.class, itemId);
        Set<OrBid> bids = foundItem.getBids();
        final List<Long> bidIds = bids.stream().map(OrBid::getId).collect(Collectors.toList());
        commitTransactionAndClose();
        bidIds.forEach(Assert::assertNotNull);

        beginTransaction();
        foundItem = entityManager.find(OrItem.class, itemId);
        entityManager.remove(foundItem);
        commitTransactionAndClose();

        beginTransaction();
        bidIds.forEach(id -> {
            OrBid bid = entityManager.find(OrBid.class, id);
            assertNull(bid);
        });
        commitTransactionAndClose();
    }

    private void persistTestData() {
        beginTransaction();
        entityManager.persist(bidder);
        bidderId = bidder.getId();
        entityManager.persist(item);
        itemId = item.getId();
        item.getBids().add(bid1);
        item.getBids().add(bid2);
        commitTransactionAndClose();
    }

    @Test
    public void testOrphanRemove() {
        persistTestData();

        beginTransaction();
        OrItem foundItem = entityManager.find(OrItem.class, itemId);
        final OrBid firstBid = foundItem.getBids().stream()
                .filter(b -> b.getId().equals(bid1.getId()))
                .findFirst().orElseThrow();
        foundItem.getBids().remove(firstBid);
        commitTransactionAndClose();

        beginTransaction();
        foundItem = entityManager.find(OrItem.class, itemId);
        assertEquals(foundItem.getBids().size(), 1);
        final OrBid secondBid = foundItem.getBids().stream()
                .filter(b -> b.getId().equals(bid2.getId()))
                .findFirst().orElseThrow();
        commitTransactionAndClose();
        assertEquals(secondBid.getAmount(), SECOND_AMOUNT);
    }

    @Test
    public void testOrphanRemoveConflict() {
        persistTestData();

        beginTransaction();
        OrUser foundBidder = entityManager.find(OrUser.class, bidderId);
        OrItem foundItem = entityManager.find(OrItem.class, itemId);
        final OrBid firstBid = foundItem.getBids().stream()
                .filter(b -> b.getId().equals(bid1.getId()))
                .findFirst().orElseThrow();
        foundItem.getBids().remove(firstBid);
        assertEquals(foundItem.getBids().size(), 1);
        assertEquals(foundBidder.getBids().size(), 2);
        commitTransactionAndClose();

        beginTransaction();
        foundBidder = entityManager.find(OrUser.class, bidderId);
        foundItem = entityManager.find(OrItem.class, itemId);
        assertEquals(foundItem.getBids().size(), 1);
        assertEquals(foundBidder.getBids().size(), 1);
        final OrBid secondBid = foundItem.getBids().stream()
                .filter(b -> b.getId().equals(bid2.getId()))
                .findFirst().orElseThrow();
        commitTransactionAndClose();
        assertEquals(secondBid.getAmount(), SECOND_AMOUNT);
    }

    private void beginTransaction() {
        entityManager = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        entityManager.getTransaction().begin();
    }

    private void commitTransactionAndClose() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
