package learn.hibern.jpwh.jpwh07.collections.listofstrings;

import learn.hibern.jpwh.jpwh07.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.util.List;

import static org.testng.Assert.*;

public class ItemWithListTest {

    @Test
    public void testPersistAndFind() {
        ItemWithList item = new ItemWithList("item-name");
        item.getImages().add("image-1");
        item.getImages().add("image-2");

        EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(item);
        Long itemId = item.getId();
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        final ItemWithList foundItem = em.find(ItemWithList.class, itemId);
        assertNotNull(foundItem);
        assertEquals(foundItem.getName(), "item-name");
        final List<String> images = foundItem.getImages();
        assertEquals(images.size(), 2);
        assertEquals(images.get(0), "image-1");
        assertEquals(images.get(1), "image-2");
        em.getTransaction().commit();
        em.close();
    }
}
