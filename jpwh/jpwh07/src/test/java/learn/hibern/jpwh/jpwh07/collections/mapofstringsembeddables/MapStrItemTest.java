package learn.hibern.jpwh.jpwh07.collections.mapofstringsembeddables;

import learn.hibern.jpwh.jpwh07.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.*;

public class MapStrItemTest {

    @Test
    public void testPersistAndFind() {
        MapStrImage img1 = new MapStrImage("title-1", "fname-1", 10, 20);
        MapStrImage img2 = new MapStrImage("title-2", "fname-2", 30, 40);
        MapStrItem item = new MapStrItem("item-name");
        item.addImage(img1);
        item.addImage(img2);

        EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(item);
        final Long itemId = item.getId();
        assertNotNull(itemId);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        MapStrItem foundItem = em.find(MapStrItem.class, itemId);
        assertNotNull(foundItem);
        assertEquals(foundItem.getImages().size(), 2);
        assertTrue(foundItem.containsImage(img1));
        assertTrue(foundItem.containsImage(img2));
        MapStrImage img3 = new MapStrImage("title-3", "fname-3", 50, 60);
        foundItem.getImages().put(img3.getFilename(), img3);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        foundItem = em.find(MapStrItem.class, itemId);
        assertEquals(foundItem.getImages().size(), 3);
        assertTrue(foundItem.containsImage(img3));
        em.getTransaction().commit();
        em.close();
    }
}
