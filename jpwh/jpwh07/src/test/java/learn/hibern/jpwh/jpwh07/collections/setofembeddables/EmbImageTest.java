package learn.hibern.jpwh.jpwh07.collections.setofembeddables;

import learn.hibern.jpwh.jpwh07.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import java.util.Set;

import static org.testng.Assert.*;

public class EmbImageTest {

    @Test
    public void testPersistAndFind() {
        EmbImage img1 = new EmbImage("title-1", "fname-1", 10, 20);
        EmbImage img2 = new EmbImage("title-2", "fname-2", 30, 40);
        EmbItem item = new EmbItem("item-name");
        item.getImages().add(img1);
        item.getImages().add(img2);

        EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(item);
        final Long itemId = item.getId();
        assertNotNull(itemId);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        EmbItem foundItem = em.find(EmbItem.class, itemId);
        assertNotNull(foundItem);
        final Set<EmbImage> images = foundItem.getImages();
        assertEquals(images.size(), 2);
        assertTrue(images.contains(img1));
        assertTrue(images.contains(img2));
        EmbImage img3 = new EmbImage("title-3", "fname-3", 50, 60);
        foundItem.getImages().add(img3);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        foundItem = em.find(EmbItem.class, itemId);
        assertEquals(foundItem.getImages().size(), 3);
        assertTrue(foundItem.getImages().contains(img3));
        em.getTransaction().commit();
        em.close();
    }
}
