package learn.hibern.jpwh.jpwh07.collections.embeddablesetofstrings;

import learn.hibern.jpwh.jpwh07.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.*;

public class EsosUserTest {

    @Test
    public void testPersistAndFind() {
        EsosUser user = new EsosUser("user-name");
        final EsosAddress homeAddress = new EsosAddress("street", "12345", "city");
        homeAddress.getContacts().add("contact-1");
        homeAddress.getContacts().add("contact-2");
        user.setHomeAddress(homeAddress);

        EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(user);
        final Long userId = user.getId();
        assertNotNull(userId);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        EsosUser foundUser = em.find(EsosUser.class, userId);
        assertNotNull(foundUser);
        assertEquals(foundUser.getHomeAddress().getZipcode(), "12345");
        assertTrue(foundUser.getHomeAddress().getContacts().contains("contact-1"));
        assertTrue(foundUser.getHomeAddress().getContacts().contains("contact-2"));
        em.getTransaction().commit();
        em.close();
    }
}
