package learn.hibern.jpwh.examples.test.simple;

import learn.hibern.jpwh.model.simple.Bid;
import learn.hibern.jpwh.model.simple.Item;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import static org.testng.Assert.*;

public class ModelOperations {

    @Test
    public void linkBidAndItem() {
        Item item = new Item();
        Bid firstBid = new Bid();

        item.getBids().add(firstBid);
        firstBid.setItem(item);

        assertEquals(item.getBids().size(), 1);
        assertTrue(item.getBids().contains(firstBid));
        assertEquals(firstBid.getItem(), item);

        // Again with convenience method
        Bid secondBid = new Bid();
        item.addBid(secondBid);

        assertEquals(2, item.getBids().size());
        assertTrue(item.getBids().contains(secondBid));
        assertEquals(item, secondBid.getItem());
    }

    @Test
    public void validateItem() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Item item = new Item();
        item.setName("Some Item");
        item.setAuctionEnd(new Date());

        Set<ConstraintViolation<Item>> violations = validator.validate(item);

        // We have one validation error, auction end date was not in the future!
        assertEquals(1, violations.size());

        ConstraintViolation<Item> violation = violations.iterator().next();
        String failedPropertyName =
                violation.getPropertyPath().iterator().next().getName();

        assertEquals(failedPropertyName, "auctionEnd");

        if (Locale.getDefault().getLanguage().equals("en"))
            assertEquals(violation.getMessage(), "must be in the future");
    }
}
