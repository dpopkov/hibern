package learn.hibern.jpwh.jpwh10.entitymanager.detached;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DETACHED_ITEM")
public class DetachedItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;

    public DetachedItem() {
    }

    public DetachedItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DetachedItem)) return false;
        DetachedItem that = (DetachedItem) o;
        return getName() != null ? getName().equals(that.getName()) : that.getName() == null;
    }

    @Override
    public int hashCode() {
        return getName() != null ? getName().hashCode() : 0;
    }
}
