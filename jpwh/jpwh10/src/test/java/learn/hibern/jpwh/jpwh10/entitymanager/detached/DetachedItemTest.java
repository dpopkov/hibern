package learn.hibern.jpwh.jpwh10.entitymanager.detached;

import learn.hibern.jpwh.jpwh10.AbstractJpaTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.*;

public class DetachedItemTest extends AbstractJpaTest {

    Long itemId;

    @BeforeMethod
    public void setUp() {
        DetachedItem persisted = new DetachedItem("InitialName");
        createEntityManagerAndBeginTransaction();
        entityManager.persist(persisted);
        itemId = persisted.getId();
        commitTransactionAndCloseEntityManager();
    }


    @Test
    public void testIdentityInDifferentPersistenceContexts() {
        createEntityManagerAndBeginTransaction();
        DetachedItem a = entityManager.find(DetachedItem.class, itemId);
        DetachedItem b = entityManager.find(DetachedItem.class, itemId);
        assertSame(a, b);
        assertEquals(b, a);
        assertEquals(a.getId(), b.getId());
        commitTransactionAndCloseEntityManager();

        createEntityManagerAndBeginTransaction();
        DetachedItem c = entityManager.find(DetachedItem.class, itemId);
        //noinspection SimplifiedTestNGAssertion
        assertTrue(a != c);
        assertEquals(a, c);
        assertEquals(a.getId(), c.getId());
        commitTransactionAndCloseEntityManager();

        Set<DetachedItem> set = new HashSet<>();
        set.add(a);
        set.add(b);
        set.add(c);
        assertEquals(set.size(), 1);
    }
}
