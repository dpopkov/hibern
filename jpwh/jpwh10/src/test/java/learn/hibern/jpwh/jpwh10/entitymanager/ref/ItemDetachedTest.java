package learn.hibern.jpwh.jpwh10.entitymanager.ref;

import learn.hibern.jpwh.jpwh10.AbstractJpaTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.*;

public class ItemDetachedTest extends AbstractJpaTest {

    Long itemId;

    @BeforeMethod
    public void setUp() {
        Item persisted = new Item("InitialName");
        createEntityManagerAndBeginTransaction();
        entityManager.persist(persisted);
        itemId = persisted.getId();
        commitTransactionAndCloseEntityManager();
    }

    @Test
    public void testIdentityInDifferentPersistenceContexts() {
        createEntityManagerAndBeginTransaction();
        Item a = entityManager.find(Item.class, itemId);
        Item b = entityManager.find(Item.class, itemId);
        assertSame(a, b);
        assertEquals(b, a);
        assertEquals(a.getId(), b.getId());
        commitTransactionAndCloseEntityManager();

        createEntityManagerAndBeginTransaction();
        Item c = entityManager.find(Item.class, itemId);
        assertNotSame(a, c);
        assertNotEquals(a, c);
        assertEquals(a.getId(), c.getId());
        commitTransactionAndCloseEntityManager();

        Set<Item> set = new HashSet<>();
        set.add(a);
        set.add(b);
        set.add(c);
        assertEquals(set.size(), 2);
        /*
            Whenever you work with instances in detached state and you test them for equality (usually in
            hash-based collections), you need to supply your own implementation of the equals() and
            hashCode() methods for your mapped entity class.
         */
    }

    @Test
    public void testIdentityInDifferentTransactionsButSamePersistenceContext() {
        createEntityManagerAndBeginTransaction();
        Item a = entityManager.find(Item.class, itemId);
        Item b = entityManager.find(Item.class, itemId);
        assertSame(a, b);
        assertEquals(b, a);
        assertEquals(a.getId(), b.getId());
        commitTransaction();

        beginTransaction();
        Item c = entityManager.find(Item.class, itemId);
        assertSame(a, c);
        assertEquals(a, c);
        assertEquals(a.getId(), c.getId());
        commitTransactionAndCloseEntityManager();

        Set<Item> set = new HashSet<>();
        set.add(a);
        set.add(b);
        set.add(c);
        assertEquals(set.size(), 1);
    }
}
