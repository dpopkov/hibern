package learn.hibern.jpwh.jpwh10;

import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;

import javax.persistence.EntityManager;

public abstract class AbstractJpaTest {

    protected EntityManager entityManager;

    protected void createEntityManagerAndBeginTransaction() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_UNIT_NAME);
    }

    protected void createEntityManagerAndBeginTransaction(String persistenceUnit) {
        entityManager = JpaEntityManagerUtil.getEntityManager(persistenceUnit);
        entityManager.getTransaction().begin();
    }

    protected void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    protected void commitTransaction() {
        entityManager.getTransaction().commit();
    }

    protected void closeEntityManager() {
        entityManager.close();
    }

    protected void commitTransactionAndCloseEntityManager() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
