package learn.hibern.jpwh.jpwh10.entitymanager.ref;

import learn.hibern.jpwh.jpwh10.AbstractJpaTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.FlushModeType;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;

import static org.testng.Assert.*;

public class ItemTest extends AbstractJpaTest {

    Long itemId;

    @BeforeMethod
    public void setUp() {
        Item persisted = new Item("InitialName");
        createEntityManagerAndBeginTransaction();
        entityManager.persist(persisted);
        itemId = persisted.getId();
        commitTransactionAndCloseEntityManager();
    }

    @Test
    public void testGetReferenceReturnsProxiedItem() {
        createEntityManagerAndBeginTransaction();
        PersistenceUnitUtil persistenceUtil = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();

        Item proxiedItem = entityManager.getReference(Item.class, itemId);
        assertFalse(persistenceUtil.isLoaded(proxiedItem));
        final String className = proxiedItem.getClass().getName();
        assertTrue(className.contains("Proxy"));
        System.out.println("Class name: " + className);

        Item managedItem = entityManager.find(Item.class, itemId);
        assertTrue(persistenceUtil.isLoaded(managedItem));

        Item cachedItem = entityManager.getReference(Item.class, itemId);
        assertTrue(persistenceUtil.isLoaded(cachedItem));
        assertSame(managedItem, cachedItem);

        commitTransactionAndCloseEntityManager();
    }

    @Test
    public void testFindReturnsItem() {
        createEntityManagerAndBeginTransaction();
        Item found = entityManager.find(Item.class, itemId);
        PersistenceUnitUtil persistenceUtil = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
        assertTrue(persistenceUtil.isLoaded(found));
        final String className = found.getClass().getName();
        assertFalse(className.contains("Proxy"));
        System.out.println("Class name: " + className);
        commitTransactionAndCloseEntityManager();
    }

    @Test
    public void testFlushingModeAuto() {
        createEntityManagerAndBeginTransaction();
        Item found = entityManager.find(Item.class, itemId);
        found.setName("ChangedName");
        final Query query = entityManager.createQuery("select i.name from Item i where i.id = :id")
                .setParameter("id", itemId);
        final Object result = query.getSingleResult();
        assertEquals(result, "ChangedName");
        commitTransactionAndCloseEntityManager();
    }

    @Test
    public void testFlushingModeCommit() {
        createEntityManagerAndBeginTransaction();
        Item found = entityManager.find(Item.class, itemId);
        found.setName("ChangedName");
        entityManager.setFlushMode(FlushModeType.COMMIT);
        final Query query = entityManager.createQuery("select i.name from Item i where i.id = :id")
                .setParameter("id", itemId);
        final Object result = query.getSingleResult();
        assertEquals(result, "InitialName");
        commitTransactionAndCloseEntityManager();
    }
}
