package learn.hibern.jpwh.jpwh06.inheritance.mappedsuper;

import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.*;

public class BillingDetailsTest {

    public static final String PERSISTENCE_UNIT_NAME = "ch06inherit";

    @Test
    public void testPersistAndFind() {
        BillingDetails card = new CreditCard("Owner", "cc-number",
                "exp-month", "exp-year");
        BillingDetails account = new BankAccount("Owner", "account", "bankname", "swift");
        EntityManager em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(card);
        Long cardId = card.getId();
        assertNotNull(cardId);
        em.persist(account);
        Long accountId = account.getId();
        assertNotNull(accountId);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        CreditCard foundCard = em.find(CreditCard.class, cardId);
        assertNotNull(foundCard);
        assertEquals(foundCard.getCardNumber(), "cc-number");
        BankAccount foundAccount = em.find(BankAccount.class, accountId);
        assertNotNull(foundAccount);
        assertEquals(foundAccount.getBankname(), "bankname");
        em.getTransaction().commit();
        em.close();
    }
}
