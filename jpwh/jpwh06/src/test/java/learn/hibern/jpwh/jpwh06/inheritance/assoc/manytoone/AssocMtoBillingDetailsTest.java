package learn.hibern.jpwh.jpwh06.inheritance.assoc.manytoone;

import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.*;

public class AssocMtoBillingDetailsTest {
    public static final String PERSISTENCE_UNIT_NAME = "ch06inherit";

    @Test
    public void testPersistFindAndPolymorphicCall() {
        AssocMtoBillingDetails account = new AssocMtoBankAccount("account-owner", "account",
                "bankname", "swift");
        AssocMtoBillingDetails card = new AssocMtoCreditCard("card-owner", "card-number",
                "exp-month", "exp-year");
        User user = new User("janedoe");
        user.setDefaultBilling(card);

        EntityManager em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(account);
        em.persist(card);
        em.persist(user);
        Long userId = user.getId();
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        User foundUser = em.find(User.class, userId);
        assertNotNull(foundUser);
        // because the defaultBilling property is mapped with FetchType.LAZY,
        // Hibernate will proxy the association target
        final AssocMtoBillingDetails defaultBilling = foundUser.getDefaultBilling();
        assertNotNull(defaultBilling);
        // It isn't a AssocMtoCreditCard, it's a runtime-generated special subclass of BillingDetails, a Hibernate proxy
        assertFalse(defaultBilling instanceof AssocMtoCreditCard);
        assertEquals(defaultBilling.pay(), "AssocMtoCreditCard.pay()");
        // To perform a proxy-safe typecast, use em.getReference()
        AssocMtoCreditCard referenceToCard = em.getReference(AssocMtoCreditCard.class, defaultBilling.getId());
        assertNotSame(referenceToCard, defaultBilling);
        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testAvoidLazyFetching() {
        AssocMtoBillingDetails account = new AssocMtoBankAccount("account-owner", "account",
                "bankname", "swift");
        AssocMtoBillingDetails card = new AssocMtoCreditCard("card-owner", "card-number",
                "exp-month", "exp-year");
        User user = new User("janedoe");
        user.setDefaultBilling(card);

        EntityManager em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(account);
        em.persist(card);
        em.persist(user);
        Long userId = user.getId();
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        User foundUser = (User) em.createQuery("select u from User u left join fetch u.defaultBilling where u.id = :id")
                .setParameter("id", userId)
                .getSingleResult();
        // No proxy has been used:
        // BillingDetails instance fetched eagerly
        assertTrue(foundUser.getDefaultBilling() instanceof AssocMtoCreditCard);

        em.getTransaction().commit();
        em.close();
    }
}
