package learn.hibern.jpwh.jpwh06.inheritance.singletable;

import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import java.util.List;

import static org.testng.Assert.*;

public class SnglBillingDetailsTest {

    private static final String PERSISTENCE_UNIT_NAME = "ch06inherit";

    @Test
    public void testPersistAndFind() {
        SnglBillingDetails card = new SnglCreditCard("card-owner", "card-number", "" +
                "exp-month", "exp-year");
        SnglBillingDetails account = new SnglBankAccount("account-owner", "account",
                "bankName", "swift");
        EntityManager em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(card);
        Long cardId = card.getId();
        assertNotNull(cardId);
        em.persist(account);
        Long accountId = account.getId();
        assertNotNull(accountId);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        TypedQuery<SnglBillingDetails> query = em.createQuery(
                "select bd from SnglBillingDetails bd", SnglBillingDetails.class);
        List<SnglBillingDetails> resultList = query.getResultList();
        assertEquals(resultList.size(), 2);
        resultList.forEach(b -> {
            if (b instanceof SnglCreditCard) {
                SnglCreditCard cc = (SnglCreditCard) b;
                assertEquals(cc.getOwner(), "card-owner");
            } else if (b instanceof SnglBankAccount) {
                SnglBankAccount acc = (SnglBankAccount) b;
                assertEquals(acc.getOwner(), "account-owner");
            }
        });
        em.getTransaction().commit();
        em.close();
    }
}
