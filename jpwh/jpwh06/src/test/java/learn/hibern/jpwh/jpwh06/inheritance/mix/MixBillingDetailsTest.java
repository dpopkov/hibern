package learn.hibern.jpwh.jpwh06.inheritance.mix;

import learn.hibern.jpwh.jpwh06.inheritance.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

import static org.testng.Assert.*;

public class MixBillingDetailsTest {

    @Test
    public void testPersistAndFind() {
        MixBillingDetails card = new MixCreditCard("card-owner",
                "card-number", "exp-month", "exp-year");
        MixBillingDetails account = new MixBankAccount("account-owner", "account",
                "bankname", "swift");
        MixBillingDetails virtual = new MixVirtualBilling("virtual-billing");

        EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(card);
        em.persist(account);
        em.persist(virtual);
        Long virtualId = virtual.getId();
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        final TypedQuery<MixOwnedBillingDetails> query = em.createQuery(
                "select bd from MixOwnedBillingDetails bd", MixOwnedBillingDetails.class);
        final List<MixOwnedBillingDetails> list = query.getResultList();
        assertEquals(list.size(), 2);
        list.forEach(b -> {
            if (b instanceof MixCreditCard) {
                MixCreditCard cc = (MixCreditCard) b;
                assertEquals(cc.getOwner(), "card-owner");
            } else if (b instanceof MixBankAccount) {
                MixBankAccount acc = (MixBankAccount) b;
                assertEquals(acc.getOwner(), "account-owner");
            }
        });
        MixVirtualBilling foundVirtual = em.find(MixVirtualBilling.class, virtualId);
        assertNotNull(foundVirtual);
        assertEquals(foundVirtual.getDescription(), "virtual-billing");
        em.getTransaction().commit();
        em.close();
    }
}
