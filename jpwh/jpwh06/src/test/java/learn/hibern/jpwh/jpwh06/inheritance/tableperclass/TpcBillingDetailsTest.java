package learn.hibern.jpwh.jpwh06.inheritance.tableperclass;

import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import java.util.List;

import static org.testng.Assert.*;

public class TpcBillingDetailsTest {

    public static final String PERSISTENCE_UNIT_NAME = "ch06inherit";

    @Test
    public void testPersistAndFind() {
        TpcBillingDetails card = new TpcCreditCard("Card-owner", "card-number",
                "exp-month", "exp-year");
        TpcBillingDetails account = new TpcBankAccount("Account-owner", "account",
                "bankname", "swift");
        EntityManager em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(card);
        Long cardId = card.getId();
        assertNotNull(cardId);
        em.persist(account);
        Long accountId = account.getId();
        assertNotNull(accountId);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        TypedQuery<TpcBillingDetails> query = em.createQuery(
                "select bd from TpcBillingDetails bd", TpcBillingDetails.class);
        List<TpcBillingDetails> resultList = query.getResultList();
        assertEquals(resultList.size(), 2);
        resultList.forEach(b -> {
            if (b instanceof TpcCreditCard) {
                TpcCreditCard cc = (TpcCreditCard) b;
                assertEquals(cc.getOwner(), "Card-owner");
            } else if (b instanceof TpcBankAccount) {
                TpcBankAccount acc = (TpcBankAccount) b;
                assertEquals(acc.getOwner(), "Account-owner");
            }
        });
        em.getTransaction().commit();
        em.close();
    }
}
