package learn.hibern.jpwh.jpwh06.inheritance.joined;

import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

import static org.testng.Assert.*;

public class JndBillingDetailsTest {

    @Test
    public void testPersistAndFind() {
        JndBillingDetails card = new JndCreditCard("Card-owner", "card-number",
                "exp-month", "exp-year");
        JndBillingDetails account = new JndBankAccount("Account-owner", "account",
                "bankname", "swift");
        EntityManager em = JpaEntityManagerUtil.getEntityManager("ch06inherit");
        em.getTransaction().begin();
        em.persist(card);
        assertNotNull(card.getId());
        em.persist(account);
        assertNotNull(account.getId());
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager("ch06inherit");
        em.getTransaction().begin();
        TypedQuery<JndBillingDetails> query = em.createQuery(
                "select bd from JndBillingDetails bd", JndBillingDetails.class);
        List<JndBillingDetails> resultList = query.getResultList();
        assertEquals(resultList.size(), 2);
        resultList.forEach(b -> {
            if (b instanceof JndCreditCard) {
                JndCreditCard cc = (JndCreditCard) b;
                assertEquals(cc.getOwner(), "Card-owner");
            } else if (b instanceof JndBankAccount) {
                JndBankAccount acc = (JndBankAccount) b;
                assertEquals(acc.getOwner(), "Account-owner");
            }
        });
        em.getTransaction().commit();
        em.close();
    }
}
