package learn.hibern.jpwh.jpwh06.inheritance.assoc.manytoone;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class AssocMtoCreditCard extends AssocMtoBillingDetails {

    @NotNull
    protected String cardNumber;
    @NotNull
    protected String expMonth;
    @NotNull
    protected String expYear;

    protected AssocMtoCreditCard() {
    }

    public AssocMtoCreditCard(@NotNull String owner, @NotNull String cardNumber,
                              @NotNull String expMonth, @NotNull String expYear) {
        super(owner);
        this.cardNumber = cardNumber;
        this.expMonth = expMonth;
        this.expYear = expYear;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    public String getExpYear() {
        return expYear;
    }

    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }

    @Override
    public String pay() {
        return "AssocMtoCreditCard.pay()";
    }
}
