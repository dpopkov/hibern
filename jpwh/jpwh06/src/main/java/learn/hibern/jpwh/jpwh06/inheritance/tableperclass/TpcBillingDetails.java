package learn.hibern.jpwh.jpwh06.inheritance.tableperclass;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class TpcBillingDetails {
    @Id
    @GeneratedValue
    protected Long id;
    @NotNull
    protected String owner;

    protected TpcBillingDetails() {
    }

    public TpcBillingDetails(@NotNull String owner) {
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
