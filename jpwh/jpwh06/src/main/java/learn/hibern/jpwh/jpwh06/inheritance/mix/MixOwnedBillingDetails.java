package learn.hibern.jpwh.jpwh06.inheritance.mix;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class MixOwnedBillingDetails extends MixBillingDetails {
    @NotNull
    protected String owner;

    public MixOwnedBillingDetails() {
    }

    public MixOwnedBillingDetails(@NotNull String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
