package learn.hibern.jpwh.jpwh06.inheritance.mappedsuper;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public abstract class BillingDetails {
    @Id
    @GeneratedValue
    protected Long id;
    @NotNull
    protected String owner;

    protected BillingDetails() {
    }

    public BillingDetails(@NotNull String owner) {
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
