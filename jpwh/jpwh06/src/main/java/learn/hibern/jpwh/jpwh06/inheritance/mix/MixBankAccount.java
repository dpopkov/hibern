package learn.hibern.jpwh.jpwh06.inheritance.mix;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class MixBankAccount extends MixOwnedBillingDetails {

    @NotNull
    protected String account;
    @NotNull
    protected String bankname;
    @NotNull
    protected String swift;

    protected MixBankAccount() {
    }

    public MixBankAccount(@NotNull String owner, @NotNull String account,
                          @NotNull String bankname, @NotNull String swift) {
        super(owner);
        this.account = account;
        this.bankname = bankname;
        this.swift = swift;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }
}
