package learn.hibern.jpwh.jpwh06.inheritance.singletable;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class SnglBankAccount extends SnglBillingDetails {

    @NotNull
    protected String account;
    @NotNull
    protected String bankname;
    @NotNull
    protected String swift;

    protected SnglBankAccount() {
    }

    public SnglBankAccount(@NotNull String owner, @NotNull String account,
                           @NotNull String bankname, @NotNull String swift) {
        super(owner);
        this.account = account;
        this.bankname = bankname;
        this.swift = swift;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }
}
