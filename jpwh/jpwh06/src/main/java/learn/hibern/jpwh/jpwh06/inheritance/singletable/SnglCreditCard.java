package learn.hibern.jpwh.jpwh06.inheritance.singletable;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class SnglCreditCard extends SnglBillingDetails {

    @NotNull
    protected String cardNumber;
    @NotNull
    protected String expMonth;
    @NotNull
    protected String expYear;

    protected SnglCreditCard() {
    }

    public SnglCreditCard(@NotNull String owner, @NotNull String cardNumber,
                          @NotNull String expMonth, @NotNull String expYear) {
        super(owner);
        this.cardNumber = cardNumber;
        this.expMonth = expMonth;
        this.expYear = expYear;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    public String getExpYear() {
        return expYear;
    }

    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }
}
