package learn.hibern.jpwh.jpwh06.inheritance.singletable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "BD_TYPE")
public abstract class SnglBillingDetails {
    @Id
    @GeneratedValue
    protected Long id;
    @NotNull
    protected String owner;

    protected SnglBillingDetails() {
    }

    public SnglBillingDetails(@NotNull String owner) {
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
