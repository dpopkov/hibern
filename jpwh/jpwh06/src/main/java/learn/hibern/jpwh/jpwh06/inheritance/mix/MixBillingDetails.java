package learn.hibern.jpwh.jpwh06.inheritance.mix;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class MixBillingDetails {
    @Id
    @GeneratedValue
    protected Long id;

    protected MixBillingDetails() {
    }

    public Long getId() {
        return id;
    }
}
