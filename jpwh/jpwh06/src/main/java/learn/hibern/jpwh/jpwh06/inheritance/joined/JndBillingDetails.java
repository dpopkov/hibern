package learn.hibern.jpwh.jpwh06.inheritance.joined;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class JndBillingDetails {
    @Id
    @GeneratedValue
    protected Long id;
    @NotNull
    protected String owner;

    protected JndBillingDetails() {
    }

    public JndBillingDetails(@NotNull String owner) {
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
