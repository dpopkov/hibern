package learn.hibern.jpwh.jpwh06.inheritance.mix;

import javax.persistence.Entity;

@Entity
public class MixVirtualBilling extends MixBillingDetails {
    protected String description;

    public MixVirtualBilling() {
    }

    public MixVirtualBilling(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
