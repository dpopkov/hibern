package learn.hibern.jpwh.jpwh06.inheritance.assoc.manytoone;

import javax.persistence.*;

@Entity
@Table(name = "USERS")
public class User {
    @Id
    @GeneratedValue
    protected Long id;
    protected String username;
    @ManyToOne(fetch = FetchType.LAZY)
    protected AssocMtoBillingDetails defaultBilling;

    public User() {
    }

    public User(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public AssocMtoBillingDetails getDefaultBilling() {
        return defaultBilling;
    }

    public void setDefaultBilling(AssocMtoBillingDetails defaultBilling) {
        this.defaultBilling = defaultBilling;
    }
}
