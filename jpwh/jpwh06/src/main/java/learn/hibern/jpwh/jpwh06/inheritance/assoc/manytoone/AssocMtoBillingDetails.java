package learn.hibern.jpwh.jpwh06.inheritance.assoc.manytoone;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AssocMtoBillingDetails {

    protected Long id;
    @NotNull
    protected String owner;

    protected AssocMtoBillingDetails() {
    }

    public AssocMtoBillingDetails(@NotNull String owner) {
        this.owner = owner;
    }

    // Using Property instead of Field access, so calling getId() doesn't initialize a proxy.
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public abstract String pay();
}
