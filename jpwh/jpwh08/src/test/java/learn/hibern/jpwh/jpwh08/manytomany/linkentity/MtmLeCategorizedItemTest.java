package learn.hibern.jpwh.jpwh08.manytomany.linkentity;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MtmLeCategorizedItemTest extends AbstractJpaTest {

    @Test
    public void testPersistAndFind() {
        MtmLeCategory someCategory = new MtmLeCategory("Some Category");
        MtmLeCategory otherCategory = new MtmLeCategory("Other Category");
        MtmLeItem someItem = new MtmLeItem("Some Item");
        MtmLeItem otherItem = new MtmLeItem("Other Item");

        beginTransactionManyToMany();
        entityManager.persist(someCategory);
        entityManager.persist(otherCategory);
        entityManager.persist(someItem);
        entityManager.persist(otherItem);
        MtmLeCategorizedItem link1 = new MtmLeCategorizedItem("janedoe", someCategory, someItem);
        MtmLeCategorizedItem link2 = new MtmLeCategorizedItem("janedoe", someCategory, otherItem);
        MtmLeCategorizedItem link3 = new MtmLeCategorizedItem("janedoe", otherCategory, someItem);
        entityManager.persist(link1);
        entityManager.persist(link2);
        entityManager.persist(link3);
        final MtmLeCategorizedItemId id1 = link1.getId();
        final MtmLeCategorizedItemId id3 = link3.getId();
        commitTransactionAndClose();

        beginTransactionManyToMany();
        MtmLeCategorizedItem foundLink1 = entityManager.find(MtmLeCategorizedItem.class, id1);
        assertEquals(foundLink1.getCategory().getName(), "Some Category");
        assertEquals(foundLink1.getItem().getName(), "Some Item");
        MtmLeCategorizedItem foundLink3 = entityManager.find(MtmLeCategorizedItem.class, id3);
        assertEquals(foundLink3.getCategory().getName(), "Other Category");
        assertEquals(foundLink3.getItem().getName(), "Some Item");
        commitTransactionAndClose();
    }
}
