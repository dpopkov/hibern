package learn.hibern.jpwh.jpwh08.onetoone.foreignkey;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class OtoFkUserTest extends AbstractJpaTest {

    @Test
    public void testPersistAndFind() {
        OtoFkUser user = new OtoFkUser("username");
        OtoFkAddress address = new OtoFkAddress("street", "12345", "city");
        user.setShippingAddress(address);

        beginTransaction();
        entityManager.persist(user);
        final Long userId = user.getId();
        commitTransactionAndClose();

        beginTransaction();
        OtoFkUser foundUser = entityManager.find(OtoFkUser.class, userId);
        assertEquals(foundUser.getShippingAddress().getZipcode(), "12345");
        commitTransactionAndClose();
    }
}
