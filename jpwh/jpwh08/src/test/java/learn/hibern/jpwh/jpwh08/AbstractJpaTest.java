package learn.hibern.jpwh.jpwh08;

import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;

import javax.persistence.EntityManager;

public abstract class AbstractJpaTest {

    protected EntityManager entityManager;

    protected void beginTransaction() {
        beginTransaction(Constants.PERSISTENCE_UNIT_NAME);
    }

    protected void beginTransactionManyToMany() {
        beginTransaction(Constants.PERSISTENCE_UNIT_MANY_TO_MANY);
    }

    protected void beginTransactionMaps() {
        beginTransaction(Constants.PERSISTENCE_UNIT_MANY_MAPS);
    }

    protected void beginTransaction(String persistenceUnit) {
        entityManager = JpaEntityManagerUtil.getEntityManager(persistenceUnit);
        entityManager.getTransaction().begin();
    }

    protected void commitTransactionAndClose() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
