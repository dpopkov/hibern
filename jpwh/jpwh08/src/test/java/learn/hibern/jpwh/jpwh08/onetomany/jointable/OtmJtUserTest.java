package learn.hibern.jpwh.jpwh08.onetomany.jointable;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import java.util.Set;

import static org.testng.Assert.*;

public class OtmJtUserTest extends AbstractJpaTest {

    @Test
    public void testPersistAndFind() {
        OtmJtUser user = new OtmJtUser("user-name");
        OtmJtItem item1 = new OtmJtItem("item1");
        OtmJtItem item2 = new OtmJtItem("item2");
        user.addBoughtItem(item1);
        user.addBoughtItem(item2);

        beginTransaction();
        entityManager.persist(item1);
        entityManager.persist(item2);
        entityManager.persist(user);
        final Long userId = user.getId();
        commitTransactionAndClose();

        beginTransaction();
        OtmJtUser foundUser = entityManager.find(OtmJtUser.class, userId);
        final Set<OtmJtItem> boughtItems = foundUser.getBoughtItems();
        assertTrue(boughtItems.contains(item1));
        assertTrue(boughtItems.contains(item2));
        commitTransactionAndClose();
    }
}
