package learn.hibern.jpwh.jpwh08.manytomany.bidirectional;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import java.util.Set;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MtmBdCategoryTest extends AbstractJpaTest {

    @Test
    public void testPersistAndFind() {
        MtmBdCategory someCategory = new MtmBdCategory("Some Category");
        MtmBdCategory otherCategory = new MtmBdCategory("Other Category");
        MtmBdItem someItem = new MtmBdItem("Some Item");
        MtmBdItem otherItem = new MtmBdItem("Other Item");
        someCategory.addItem(someItem);
        someCategory.addItem(otherItem);
        otherCategory.addItem(someItem);

        beginTransactionManyToMany();
        entityManager.persist(someCategory);
        final Long catId = someCategory.getId();
        entityManager.persist(otherCategory);
        commitTransactionAndClose();

        beginTransactionManyToMany();
        MtmBdCategory foundSomeCategory = entityManager.find(MtmBdCategory.class, catId);
        final Set<MtmBdItem> items = foundSomeCategory.getItems();
        assertEquals(items.size(), 2);
        assertTrue(items.contains(someItem));
        assertTrue(items.contains(otherItem));
        commitTransactionAndClose();
    }
}
