package learn.hibern.jpwh.jpwh08.onetomany.list.unidirectional;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.testng.Assert.*;

public class OtmLstUdItemTest extends AbstractJpaTest {

    private static final BigDecimal AMOUNT_1 = BigDecimal.valueOf(10, 2);
    private static final BigDecimal AMOUNT_2 = BigDecimal.valueOf(20, 2);
    private static final BigDecimal AMOUNT_3 = BigDecimal.valueOf(30, 2);

    @Test
    public void testPersistAndFind() {
        beginTransaction();
        OtmLstUdItem item = new OtmLstUdItem("item-name");
        OtmLstUdBid bid1 = new OtmLstUdBid(AMOUNT_1);
        OtmLstUdBid bid2 = new OtmLstUdBid(AMOUNT_2);
        item.getBids().add(bid1);
        item.getBids().add(bid2);
        entityManager.persist(item);
        final Long itemId = item.getId();
        entityManager.persist(bid1);
        entityManager.persist(bid2);
        commitTransactionAndClose();

        beginTransaction();
        OtmLstUdItem foundItem = entityManager.find(OtmLstUdItem.class, itemId);
        final List<OtmLstUdBid> bids = foundItem.getBids();
        assertEquals(bids.get(0).getAmount(), AMOUNT_1);
        assertEquals(bids.get(1).getAmount(), AMOUNT_2);
        commitTransactionAndClose();

        beginTransaction();
        foundItem = entityManager.find(OtmLstUdItem.class, itemId);
        OtmLstUdBid bid3 = new OtmLstUdBid(AMOUNT_3);
        foundItem.getBids().add(bid3);
        entityManager.persist(bid3);
        commitTransactionAndClose();

        beginTransaction();
        foundItem = entityManager.find(OtmLstUdItem.class, itemId);
        assertEquals(foundItem.getBids().size(), 3);
        commitTransactionAndClose();
    }
}
