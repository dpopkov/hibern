package learn.hibern.jpwh.jpwh08.onetoone.jointable;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class OtoJtShipmentTest extends AbstractJpaTest {

    @Test
    public void testPersistAndFind() {
        beginTransaction();
        OtoJtShipment shipment1 = new OtoJtShipment();
        entityManager.persist(shipment1);
        final Long shipmentId1 = shipment1.getId();
        OtoJtItem item = new OtoJtItem("some-item");
        entityManager.persist(item);
        OtoJtShipment shipment2 = new OtoJtShipment(item);
        entityManager.persist(shipment2);
        final Long shipmentId2 = shipment2.getId();
        commitTransactionAndClose();

        beginTransaction();
        OtoJtShipment foundShipment1 = entityManager.find(OtoJtShipment.class, shipmentId1);
        assertNull(foundShipment1.getAuction());
        OtoJtShipment foundShipment2 = entityManager.find(OtoJtShipment.class, shipmentId2);
        OtoJtItem foundItem = foundShipment2.getAuction();
        assertEquals(foundItem.getName(), "some-item");
        commitTransactionAndClose();
    }
}
