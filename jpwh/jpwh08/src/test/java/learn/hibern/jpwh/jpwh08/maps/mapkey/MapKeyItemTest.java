package learn.hibern.jpwh.jpwh08.maps.mapkey;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Map;

import static org.testng.Assert.*;

public class MapKeyItemTest extends AbstractJpaTest {

    private static final BigDecimal AMOUNT_1 = BigDecimal.valueOf(10, 2);
    private static final BigDecimal AMOUNT_2 = BigDecimal.valueOf(20, 2);

    @Test
    public void testPersistAndFind() {
        MapKeyItem item = new MapKeyItem("item-name");
        MapKeyBid bid1 = new MapKeyBid(AMOUNT_1, item);
        MapKeyBid bid2 = new MapKeyBid(AMOUNT_2, item);

        beginTransactionMaps();
        entityManager.persist(bid1);
        entityManager.persist(bid2);
        final Long bid1Id = bid1.getId();
        final Long bid2Id = bid2.getId();
        item.getBids().put(bid1Id, bid1);
        item.getBids().put(bid2Id, bid2);
        entityManager.persist(item);
        final Long itemId = item.getId();
        commitTransactionAndClose();

        beginTransactionMaps();
        MapKeyItem foundItem = entityManager.find(MapKeyItem.class, itemId);
        final Map<Long, MapKeyBid> bids = foundItem.getBids();
        assertEquals(bids.size(), 2);
        assertEquals(bids.get(bid1Id).getAmount(), AMOUNT_1);
        assertEquals(bids.get(bid2Id).getAmount(), AMOUNT_2);
        commitTransactionAndClose();
    }
}
