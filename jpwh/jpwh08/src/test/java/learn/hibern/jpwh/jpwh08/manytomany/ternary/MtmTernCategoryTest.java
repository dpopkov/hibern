package learn.hibern.jpwh.jpwh08.manytomany.ternary;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MtmTernCategoryTest extends AbstractJpaTest {

    @Test
    public void testPersistAndFind() {
        MtmTernCategory someCategory = new MtmTernCategory("Some Category");
        MtmTernCategory otherCategory = new MtmTernCategory("Other Category");
        MtmTernItem someItem = new MtmTernItem("Some Item");
        MtmTernItem otherItem = new MtmTernItem("Other Item");
        MtmTernUser user = new MtmTernUser("janedoe");

        beginTransactionManyToMany();
        entityManager.persist(someCategory);
        entityManager.persist(otherCategory);
        entityManager.persist(someItem);
        entityManager.persist(otherItem);
        entityManager.persist(user);
        someCategory.linkCategorizedItem(someItem, user);
        someCategory.linkCategorizedItem(otherItem, user);
        otherCategory.linkCategorizedItem(someItem, user);
        final Long id1 = someCategory.getId();
        final Long id2 = otherCategory.getId();
        commitTransactionAndClose();

        beginTransactionManyToMany();
        MtmTernCategory foundCat1 = entityManager.find(MtmTernCategory.class, id1);
        assertEquals(foundCat1.getCategorizedItems().size(), 2);
        MtmTernCategory foundCat2 = entityManager.find(MtmTernCategory.class, id2);
        assertEquals(foundCat2.getCategorizedItems().size(), 1);
        commitTransactionAndClose();
    }
}
