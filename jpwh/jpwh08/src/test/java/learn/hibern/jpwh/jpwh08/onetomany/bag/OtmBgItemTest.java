package learn.hibern.jpwh.jpwh08.onetomany.bag;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.*;

public class OtmBgItemTest extends AbstractJpaTest {

    @Test
    public void testPersistAndFind() {
        OtmBgItem item = new OtmBgItem("item-name");
        OtmBgBid bid1 = new OtmBgBid(BigDecimal.valueOf(20, 2), item);
        item.getBids().add(bid1);
        OtmBgBid bid2 = new OtmBgBid(BigDecimal.valueOf(40, 2), item);
        item.getBids().add(bid2);

        beginTransaction();
        entityManager.persist(item);
        entityManager.persist(bid1);
        entityManager.persist(bid2);
        final Long itemId = item.getId();
        commitTransactionAndClose();

        beginTransaction();
        OtmBgItem foundItem = entityManager.find(OtmBgItem.class, itemId);
        OtmBgBid bid3 = new OtmBgBid(BigDecimal.valueOf(50, 2), foundItem);
        entityManager.persist(bid3);
        foundItem.getBids().add(bid3);  // No SELECT of bids here
        commitTransactionAndClose();

        beginTransaction();
        foundItem = entityManager.find(OtmBgItem.class, itemId);
        assertEquals(foundItem.getBids().size(), 3);
        commitTransactionAndClose();
    }
}
