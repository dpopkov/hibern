package learn.hibern.jpwh.jpwh08.onetoone.sharedprimarykey;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class OtoSpkUserTest extends AbstractJpaTest {

    @Test
    public void testPersistAndFind() {
        final String zipcode = "12345";
        final String username = "user-name";
        OtoSpkAddress address = new OtoSpkAddress("street", zipcode, "city");

        beginTransaction();
        entityManager.persist(address);
        final Long addressId = address.getId();
        OtoSpkUser user = new OtoSpkUser(addressId, username);
        entityManager.persist(user);
        // user.setShippingAddress(address);    // Optional
        commitTransactionAndClose();

        beginTransaction();
        OtoSpkUser foundUser = entityManager.find(OtoSpkUser.class, addressId);
        assertEquals(foundUser.getUsername(), username);
        assertEquals(foundUser.getShippingAddress().getZipcode(), zipcode);
        commitTransactionAndClose();
    }
}
