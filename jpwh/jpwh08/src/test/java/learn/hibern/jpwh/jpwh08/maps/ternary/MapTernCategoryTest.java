package learn.hibern.jpwh.jpwh08.maps.ternary;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MapTernCategoryTest extends AbstractJpaTest {

    @Test
    public void testPersistAndFind() {
        MapTernCategory category = new MapTernCategory("Some Category");
        MapTernItem item1 = new MapTernItem("item-1");
        MapTernItem item2 = new MapTernItem("item-2");
        MapTernUser user1 = new MapTernUser("user-1");
        MapTernUser user2 = new MapTernUser("user-2");

        beginTransactionMaps();
        entityManager.persist(item1);
        entityManager.persist(item2);
        entityManager.persist(user1);
        entityManager.persist(user2);
        category.addItemByUser(item1, user1);
        category.addItemByUser(item2, user2);
        entityManager.persist(category);
        final Long catId = category.getId();
        commitTransactionAndClose();

        beginTransactionMaps();
        MapTernCategory foundCategory = entityManager.find(MapTernCategory.class, catId);
        assertEquals(foundCategory.getItemAddedBy().get(item1).getName(), "user-1");
        assertEquals(foundCategory.getItemAddedBy().get(item2).getName(), "user-2");
        commitTransactionAndClose();
    }
}
