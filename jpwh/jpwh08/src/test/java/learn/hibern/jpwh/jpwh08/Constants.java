package learn.hibern.jpwh.jpwh08;

public class Constants {
    public static final String PERSISTENCE_UNIT_NAME = "ch08advancedMappings";
    public static final String PERSISTENCE_UNIT_MANY_TO_MANY = "ch08manyToMany";
    public static final String PERSISTENCE_UNIT_MANY_MAPS = "ch08maps";
}
