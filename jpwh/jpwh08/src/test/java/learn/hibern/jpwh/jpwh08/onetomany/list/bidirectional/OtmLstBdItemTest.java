package learn.hibern.jpwh.jpwh08.onetomany.list.bidirectional;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.*;

public class OtmLstBdItemTest extends AbstractJpaTest {

    private static final BigDecimal AMOUNT_1 = BigDecimal.valueOf(10, 2);
    private static final BigDecimal AMOUNT_2 = BigDecimal.valueOf(20, 2);

    @Test
    public void testPersistAndFind() {
        OtmLstBdItem item = new OtmLstBdItem("item-name");
        OtmLstBdBid bid1 = new OtmLstBdBid(AMOUNT_1, item);
        OtmLstBdBid bid2 = new OtmLstBdBid(AMOUNT_2, item);
        item.getBids().add(bid1);
        item.getBids().add(bid2);

        beginTransaction();
        entityManager.persist(item);
        final Long itemId = item.getId();
        entityManager.persist(bid1);
        entityManager.persist(bid2);
        commitTransactionAndClose();

        beginTransaction();
        OtmLstBdItem foundItem = entityManager.find(OtmLstBdItem.class, itemId);
        assertEquals(foundItem.getBids().get(0).getAmount(), AMOUNT_1);
        assertEquals(foundItem.getBids().get(1).getAmount(), AMOUNT_2);
        commitTransactionAndClose();
    }
}
