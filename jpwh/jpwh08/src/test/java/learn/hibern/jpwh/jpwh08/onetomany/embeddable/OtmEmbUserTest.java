package learn.hibern.jpwh.jpwh08.onetomany.embeddable;

import learn.hibern.jpwh.jpwh08.AbstractJpaTest;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.Set;

import static org.testng.Assert.*;

public class OtmEmbUserTest extends AbstractJpaTest {

    @Test
    public void testPersistAndFind() {
        final OtmEmbUser user = new OtmEmbUser("user-name");
        final OtmEmbAddress address = new OtmEmbAddress("street", "12345", "'city");
        final Date createdOn1 = new Date();
        final OtmEmbShipment shipment1 = new OtmEmbShipment(createdOn1);
        final OtmEmbShipment shipment2 = new OtmEmbShipment(new Date(createdOn1.getTime() + 1000));
        address.getDeliveries().add(shipment1);
        address.getDeliveries().add(shipment2);
        user.setShippingAddress(address);

        beginTransaction();
        entityManager.persist(user);
        entityManager.persist(shipment1);
        entityManager.persist(shipment2);
        final Long userId = user.getId();
        commitTransactionAndClose();

        beginTransaction();
        final OtmEmbUser foundUser = entityManager.find(OtmEmbUser.class, userId);
        assertEquals(foundUser.getShippingAddress().getZipcode(), "12345");
        final Set<OtmEmbShipment> deliveries = foundUser.getShippingAddress().getDeliveries();
        assertEquals(deliveries.size(), 2);
        assertTrue(deliveries.contains(shipment1));
        assertTrue(deliveries.contains(shipment2));
        commitTransactionAndClose();
    }
}
