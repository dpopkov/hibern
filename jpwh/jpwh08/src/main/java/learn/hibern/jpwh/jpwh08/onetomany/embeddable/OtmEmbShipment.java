package learn.hibern.jpwh.jpwh08.onetomany.embeddable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "OTM_EMB_SHIPMENT")
public class OtmEmbShipment {
    @Id
    @GeneratedValue
    protected Long id;
    @NotNull
    protected Date createdOn;

    public OtmEmbShipment() {
    }

    public OtmEmbShipment(@NotNull Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OtmEmbShipment)) return false;

        OtmEmbShipment that = (OtmEmbShipment) o;

        if (!Objects.equals(id, that.id)) return false;
        return Objects.equals(createdOn, that.createdOn);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (createdOn != null ? createdOn.hashCode() : 0);
        return result;
    }
}
