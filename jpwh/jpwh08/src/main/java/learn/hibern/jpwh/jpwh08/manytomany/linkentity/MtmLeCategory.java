package learn.hibern.jpwh.jpwh08.manytomany.linkentity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MTM_LE_CATEGORY")
public class MtmLeCategory {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @OneToMany(mappedBy = "category")
    protected Set<MtmLeCategorizedItem> categorizedItems = new HashSet<>();

    public MtmLeCategory() {
    }

    public MtmLeCategory(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MtmLeCategorizedItem> getCategorizedItems() {
        return categorizedItems;
    }

    public void setCategorizedItems(Set<MtmLeCategorizedItem> categorizedItems) {
        this.categorizedItems = categorizedItems;
    }
}
