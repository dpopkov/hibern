package learn.hibern.jpwh.jpwh08.onetomany.bag;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "OTM_BG_BID")
public class OtmBgBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected BigDecimal amount;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    protected OtmBgItem item;

    public OtmBgBid() {
    }

    public OtmBgBid(@NotNull BigDecimal amount, OtmBgItem item) {
        this.amount = amount;
        this.item = item;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public OtmBgItem getItem() {
        return item;
    }

    public void setItem(OtmBgItem item) {
        this.item = item;
    }
}
