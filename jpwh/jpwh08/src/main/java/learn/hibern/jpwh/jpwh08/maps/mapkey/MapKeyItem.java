package learn.hibern.jpwh.jpwh08.maps.mapkey;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "MAP_KEY_ITEM")
public class MapKeyItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;

    @MapKey(name = "id")
    @OneToMany(mappedBy = "item")
    protected Map<Long, MapKeyBid> bids = new HashMap<>();

    public MapKeyItem() {
    }

    public MapKeyItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Long, MapKeyBid> getBids() {
        return bids;
    }

    public void setBids(Map<Long, MapKeyBid> bids) {
        this.bids = bids;
    }
}
