package learn.hibern.jpwh.jpwh08.manytomany.linkentity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "MTM_LE_CATEGORY_ITEM")
@org.hibernate.annotations.Immutable
public class MtmLeCategorizedItem {

    /** maps identifier property and composite key columns. */
    @EmbeddedId
    protected final MtmLeCategorizedItemId id = new MtmLeCategorizedItemId();

    /** Maps username. */
    @Column(updatable = false)
    @NotNull
    protected String addedBy;

    /** Maps timestamp. */
    @Column(updatable = false)
    @NotNull
    protected Date addedOn = new Date();

    @ManyToOne
    @JoinColumn(
            name = "CATEGORY_ID",
            insertable = false, updatable = false)
    protected MtmLeCategory category;

    @ManyToOne
    @JoinColumn(
            name = "ITEM_ID",
            insertable = false, updatable = false)
    protected MtmLeItem item;

    public MtmLeCategorizedItem() {
    }

    public MtmLeCategorizedItem(@NotNull String addedBy, MtmLeCategory category, MtmLeItem item) {
        this.addedBy = addedBy;
        this.category = category;
        this.item = item;

        this.id.categoryId = category.getId();
        this.id.itemId = item.getId();

        category.getCategorizedItems().add(this);
        item.getCategorizedItems().add(this);
    }

    public MtmLeCategorizedItemId getId() {
        return id;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public MtmLeCategory getCategory() {
        return category;
    }

    public MtmLeItem getItem() {
        return item;
    }
}
