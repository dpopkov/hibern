package learn.hibern.jpwh.jpwh08.manytomany.ternary;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MTM_TERN_USER")
public class MtmTernUser {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    public MtmTernUser() {
    }

    public MtmTernUser(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
