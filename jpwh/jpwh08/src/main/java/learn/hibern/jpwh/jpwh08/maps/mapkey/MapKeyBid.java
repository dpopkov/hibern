package learn.hibern.jpwh.jpwh08.maps.mapkey;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "MAP_KEY_BID")
public class MapKeyBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected BigDecimal amount;

    @ManyToOne
    protected MapKeyItem item;

    public MapKeyBid() {
    }

    public MapKeyBid(@NotNull BigDecimal amount, MapKeyItem item) {
        this.amount = amount;
        this.item = item;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public MapKeyItem getItem() {
        return item;
    }

    public void setItem(MapKeyItem item) {
        this.item = item;
    }
}
