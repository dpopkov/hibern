package learn.hibern.jpwh.jpwh08.manytomany.ternary;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "MTM_TERN_ITEM")
public class MtmTernItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;

    public MtmTernItem() {
    }

    public MtmTernItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MtmTernItem)) return false;

        MtmTernItem mtmBdItem = (MtmTernItem) o;

        if (!Objects.equals(id, mtmBdItem.id)) return false;
        return Objects.equals(name, mtmBdItem.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
