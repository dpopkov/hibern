package learn.hibern.jpwh.jpwh08.onetomany.list.bidirectional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "OTM_LST_BD_ITEM")
public class OtmLstBdItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    /*
    Hibernate now has to store the index of each element.
    If the Bid#item side was the owner of the relation ship,
    Hibernate would ignore the collection when storing data and not write the element indexes.
    You have to map the @JoinColumn twice and then disable writing on the @ManyToOne side
    with updatable=false and insertable=false.
    Hibernate now considers the collection side when storing data, including the index of each element.
     */
    @OneToMany
    @JoinColumn(
            name = "ITEM_ID",
            nullable = false
    )
    @OrderColumn(
            name = "BID_POSITION",
            nullable = false
    )
    protected List<OtmLstBdBid> bids = new ArrayList<>();

    public OtmLstBdItem() {
    }

    public OtmLstBdItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OtmLstBdBid> getBids() {
        return bids;
    }

    public void setBids(List<OtmLstBdBid> bids) {
        this.bids = bids;
    }
}
