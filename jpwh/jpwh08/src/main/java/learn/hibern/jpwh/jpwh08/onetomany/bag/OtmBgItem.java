package learn.hibern.jpwh.jpwh08.onetomany.bag;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "OTM_BG_ITEM")
public class OtmBgItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @OneToMany(mappedBy = "item")
    protected Collection<OtmBgBid> bids = new ArrayList<>();

    public OtmBgItem() {
    }

    public OtmBgItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<OtmBgBid> getBids() {
        return bids;
    }

    public void setBids(Collection<OtmBgBid> bids) {
        this.bids = bids;
    }
}
