package learn.hibern.jpwh.jpwh08.onetoone.sharedprimarykey;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "OTO_SPK_ADDRESS")
public class OtoSpkAddress {
    @Id
    @GeneratedValue
    protected Long id;
    @NotNull
    protected String street;
    @NotNull
    protected String zipcode;
    @NotNull
    protected String city;

    public OtoSpkAddress() {
    }

    public OtoSpkAddress(@NotNull String street, @NotNull String zipcode, @NotNull String city) {
        this.street = street;
        this.zipcode = zipcode;
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
