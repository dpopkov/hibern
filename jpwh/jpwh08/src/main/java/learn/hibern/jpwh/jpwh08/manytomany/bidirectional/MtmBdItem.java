package learn.hibern.jpwh.jpwh08.manytomany.bidirectional;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "MTM_BD_ITEM")
public class MtmBdItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;
    @ManyToMany(mappedBy = "items")
    protected Set<MtmBdCategory> categories = new HashSet<>();

    public MtmBdItem() {
    }

    public MtmBdItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MtmBdCategory> getCategories() {
        return categories;
    }

    public void setCategories(Set<MtmBdCategory> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MtmBdItem)) return false;

        MtmBdItem mtmBdItem = (MtmBdItem) o;

        if (!Objects.equals(id, mtmBdItem.id)) return false;
        return Objects.equals(name, mtmBdItem.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
