package learn.hibern.jpwh.jpwh08.onetoone.foreignkey;

import javax.persistence.*;

@Entity
@Table(name = "OTO_FK_USER")
public class OtoFkUser {
    @Id
    @GeneratedValue
    protected Long id;

    protected String username;

    @OneToOne(
            fetch = FetchType.LAZY,
            optional = false,
            cascade = CascadeType.PERSIST
    )
    @JoinColumn(unique = true)  // Defaults to SHIPPINGADDRESS_ID
    protected OtoFkAddress shippingAddress; // Unidirectional association

    public OtoFkUser() {
    }

    public OtoFkUser(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OtoFkAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(OtoFkAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
}
