package learn.hibern.jpwh.jpwh08.onetomany.list.unidirectional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "OTM_LST_UD_ITEM")
public class OtmLstUdItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @OneToMany
    @JoinColumn(
            name = "ITEM_ID",
            nullable = false
    )
    @OrderColumn(
            name = "BID_POSITION",
            nullable = false
    )
    protected List<OtmLstUdBid> bids = new ArrayList<>();

    public OtmLstUdItem() {
    }

    public OtmLstUdItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OtmLstUdBid> getBids() {
        return bids;
    }

    public void setBids(List<OtmLstUdBid> bids) {
        this.bids = bids;
    }
}
