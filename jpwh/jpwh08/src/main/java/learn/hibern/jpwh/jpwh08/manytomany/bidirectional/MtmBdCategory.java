package learn.hibern.jpwh.jpwh08.manytomany.bidirectional;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MTM_BD_CATEGORY")
public class MtmBdCategory {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "MTM_BD_CATEGORY_ITEM",
            joinColumns = @JoinColumn(name = "CATEGORY_ID"),
            inverseJoinColumns = @JoinColumn(name = "ITEM_ID")
    )
    protected Set<MtmBdItem> items = new HashSet<>();

    public MtmBdCategory() {
    }

    public MtmBdCategory(String name) {
        this.name = name;
    }

    public void addItem(MtmBdItem item) {
        items.add(item);
        item.getCategories().add(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MtmBdItem> getItems() {
        return items;
    }

    public void setItems(Set<MtmBdItem> items) {
        this.items = items;
    }
}
