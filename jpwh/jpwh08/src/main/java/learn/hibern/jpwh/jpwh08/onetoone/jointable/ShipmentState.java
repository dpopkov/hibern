package learn.hibern.jpwh.jpwh08.onetoone.jointable;

public enum ShipmentState {
    TRANSIT,
    CONFIRMED
}
