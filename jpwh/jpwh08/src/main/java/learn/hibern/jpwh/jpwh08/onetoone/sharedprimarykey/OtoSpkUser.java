package learn.hibern.jpwh.jpwh08.onetoone.sharedprimarykey;

import javax.persistence.*;

@Entity
@Table(name = "OTO_SPK_USER")
public class OtoSpkUser {
    @Id
    protected Long id;  // uses application assigned identifier

    protected String username;

    @OneToOne(
            fetch = FetchType.LAZY,
            optional = false
    )
    @PrimaryKeyJoinColumn   // selects shared primary key strategy
    protected OtoSpkAddress shippingAddress;

    public OtoSpkUser() {
    }

    public OtoSpkUser(Long id, String username) {
        this.id = id;
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OtoSpkAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(OtoSpkAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
}
