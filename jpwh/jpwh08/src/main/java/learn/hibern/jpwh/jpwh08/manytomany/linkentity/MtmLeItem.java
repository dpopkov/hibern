package learn.hibern.jpwh.jpwh08.manytomany.linkentity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "MTM_LE_ITEM")
public class MtmLeItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;

    @OneToMany(mappedBy = "item")
    protected Set<MtmLeCategorizedItem> categorizedItems = new HashSet<>();

    public MtmLeItem() {
    }

    public MtmLeItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MtmLeCategorizedItem> getCategorizedItems() {
        return categorizedItems;
    }

    public void setCategorizedItems(Set<MtmLeCategorizedItem> categorizedItems) {
        this.categorizedItems = categorizedItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MtmLeItem)) return false;

        MtmLeItem mtmBdItem = (MtmLeItem) o;

        if (!Objects.equals(id, mtmBdItem.id)) return false;
        return Objects.equals(name, mtmBdItem.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
