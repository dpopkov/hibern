package learn.hibern.jpwh.jpwh08.onetoone.jointable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "OTO_JT_SHIPMENT")
public class OtoJtShipment {
    @Id
    @GeneratedValue
    protected Long id;
    @NotNull
    protected Date createdOn = new Date();
    @NotNull
    @Enumerated(EnumType.STRING)
    protected ShipmentState shipmentState = ShipmentState.TRANSIT;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinTable(
            name = "OTO_JT_ITEM_SHIPMENT",
            joinColumns = @JoinColumn(name = "SHIPMENT_ID"),     // Defaults to ID
            inverseJoinColumns = @JoinColumn(name = "ITEM_ID", // Defaults to AUCTION_ID
                    nullable = false,
                    unique = true)
    )
    protected OtoJtItem auction;

    public OtoJtShipment() {
    }

    public OtoJtShipment(OtoJtItem auction) {
        this.auction = auction;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public ShipmentState getShipmentState() {
        return shipmentState;
    }

    public void setShipmentState(ShipmentState shipmentState) {
        this.shipmentState = shipmentState;
    }

    public OtoJtItem getAuction() {
        return auction;
    }

    public void setAuction(OtoJtItem auction) {
        this.auction = auction;
    }
}
