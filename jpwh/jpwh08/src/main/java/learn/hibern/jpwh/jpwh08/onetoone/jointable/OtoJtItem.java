package learn.hibern.jpwh.jpwh08.onetoone.jointable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "OTO_JT_ITEM")
public class OtoJtItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;

    public OtoJtItem() {
    }

    public OtoJtItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
