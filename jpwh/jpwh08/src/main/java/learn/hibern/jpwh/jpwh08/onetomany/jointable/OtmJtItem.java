package learn.hibern.jpwh.jpwh08.onetomany.jointable;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "OTM_JT_ITEM")
public class OtmJtItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(
            name = "OTM_JT_ITEM_BUYER",
            joinColumns = @JoinColumn(name = "ITEM_ID"),        // Defaults to ID
            inverseJoinColumns = @JoinColumn(nullable = false)  // Defaults to BUYER_ID
    )
    protected OtmJtUser buyer;

    public OtmJtItem() {
    }

    public OtmJtItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OtmJtUser getBuyer() {
        return buyer;
    }

    public void setBuyer(OtmJtUser buyer) {
        this.buyer = buyer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OtmJtItem)) return false;

        OtmJtItem otmJtItem = (OtmJtItem) o;

        if (!Objects.equals(id, otmJtItem.id)) return false;
        return Objects.equals(name, otmJtItem.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
