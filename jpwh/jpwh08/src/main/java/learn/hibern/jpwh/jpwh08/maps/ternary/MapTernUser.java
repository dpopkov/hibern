package learn.hibern.jpwh.jpwh08.maps.ternary;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MAP_TERN_USER")
public class MapTernUser {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    public MapTernUser() {
    }

    public MapTernUser(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
