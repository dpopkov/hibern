package learn.hibern.jpwh.jpwh08.onetomany.list.bidirectional;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "OTM_LST_BD_BID")
public class OtmLstBdBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected BigDecimal amount;

    @ManyToOne
    @JoinColumn(
            name = "ITEM_ID",
            updatable = false,
            insertable = false      // Disable writing! (see comment in OtmLstBdItem)
    )
    @NotNull
    protected OtmLstBdItem item;

    public OtmLstBdBid() {
    }

    public OtmLstBdBid(@NotNull BigDecimal amount, @NotNull OtmLstBdItem item) {
        this.amount = amount;
        this.item = item;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public OtmLstBdItem getItem() {
        return item;
    }

    public void setItem(OtmLstBdItem item) {
        this.item = item;
    }
}
