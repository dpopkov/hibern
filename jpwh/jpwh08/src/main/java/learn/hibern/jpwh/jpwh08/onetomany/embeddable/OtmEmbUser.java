package learn.hibern.jpwh.jpwh08.onetomany.embeddable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "OTM_EMB_USER")
public class OtmEmbUser {
    @Id
    @GeneratedValue
    protected Long id;

    protected String username;

    protected OtmEmbAddress shippingAddress;

    public OtmEmbUser() {
    }

    public OtmEmbUser(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OtmEmbAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(OtmEmbAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
}
