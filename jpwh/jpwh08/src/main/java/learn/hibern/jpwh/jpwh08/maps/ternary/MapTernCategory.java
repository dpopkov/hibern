package learn.hibern.jpwh.jpwh08.maps.ternary;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "MAP_TERN_CATEGORY")
public class MapTernCategory {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @MapKeyJoinColumn(name = "ITEM_ID")
    @JoinTable(
            name = "MAP_TERN_CATEGORY_ITEM_USER",
            joinColumns = @JoinColumn(name = "CATEGORY_ID"),
            inverseJoinColumns = @JoinColumn(name = "USER_ID")
    )
    protected Map<MapTernItem, MapTernUser> itemAddedBy = new HashMap<>();

    public MapTernCategory() {
    }

    public MapTernCategory(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<MapTernItem, MapTernUser> getItemAddedBy() {
        return itemAddedBy;
    }

    public void setItemAddedBy(Map<MapTernItem, MapTernUser> itemAddedBy) {
        this.itemAddedBy = itemAddedBy;
    }

    public void addItemByUser(MapTernItem item, MapTernUser user) {
        itemAddedBy.put(item, user);
    }
}
