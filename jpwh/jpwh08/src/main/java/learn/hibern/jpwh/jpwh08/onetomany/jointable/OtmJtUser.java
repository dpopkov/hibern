package learn.hibern.jpwh.jpwh08.onetomany.jointable;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "OTM_JT_USER")
public class OtmJtUser {
    @Id
    @GeneratedValue
    protected Long id;

    protected String username;

    @OneToMany(mappedBy = "buyer")
    protected Set<OtmJtItem> boughtItems = new HashSet<>();

    public OtmJtUser() {
    }

    public OtmJtUser(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<OtmJtItem> getBoughtItems() {
        return boughtItems;
    }

    public void setBoughtItems(Set<OtmJtItem> items) {
        this.boughtItems = items;
    }

    public void addBoughtItem(OtmJtItem item) {
        boughtItems.add(item);
        item.setBuyer(this);
    }
}
