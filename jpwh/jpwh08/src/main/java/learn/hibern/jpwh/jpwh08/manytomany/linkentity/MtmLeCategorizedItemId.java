package learn.hibern.jpwh.jpwh08.manytomany.linkentity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class MtmLeCategorizedItemId implements Serializable {

    @Column(name = "CATEGORY_ID")
    protected Long categoryId;

    @Column(name = "ITEM_ID")
    protected Long itemId;

    public MtmLeCategorizedItemId() {
    }

    public MtmLeCategorizedItemId(Long categoryId, Long itemId) {
        this.categoryId = categoryId;
        this.itemId = itemId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public Long getItemId() {
        return itemId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MtmLeCategorizedItemId)) return false;
        MtmLeCategorizedItemId id = (MtmLeCategorizedItemId) o;
        if (!Objects.equals(categoryId, id.categoryId)) return false;
        return Objects.equals(itemId, id.itemId);
    }

    @Override
    public int hashCode() {
        int result = categoryId != null ? categoryId.hashCode() : 0;
        result = 31 * result + (itemId != null ? itemId.hashCode() : 0);
        return result;
    }
}
