package learn.hibern.jpwh.jpwh08.manytomany.ternary;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Embeddable
public class MtmTernCategorizedItem {

    @ManyToOne
    @JoinColumn(
            name = "ITEM_ID",
            updatable = false)
    protected MtmTernItem item;

    @ManyToOne
    @JoinColumn(
            name = "USER_ID",
            updatable = false)
    @NotNull
    protected MtmTernUser addedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @NotNull
    protected Date addedOn = new Date();

    public MtmTernCategorizedItem() {
    }

    public MtmTernCategorizedItem(MtmTernItem item, @NotNull MtmTernUser addedBy) {
        this.item = item;
        this.addedBy = addedBy;
    }

    public MtmTernItem getItem() {
        return item;
    }

    public void setItem(MtmTernItem item) {
        this.item = item;
    }

    public MtmTernUser getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(MtmTernUser addedBy) {
        this.addedBy = addedBy;
    }

    public Date getAddedOn() {
        return addedOn;
    }
}
