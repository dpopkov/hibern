package learn.hibern.jpwh.jpwh08.manytomany.ternary;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MTM_TERN_CATEGORY")
public class MtmTernCategory {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @ElementCollection
    @CollectionTable(
            name = "MTM_TERN_CATEGORY_ITEM",
            joinColumns = @JoinColumn(name = "CATEGORY_ID")
    )
    protected Set<MtmTernCategorizedItem> categorizedItems = new HashSet<>();

    public MtmTernCategory() {
    }

    public MtmTernCategory(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MtmTernCategorizedItem> getCategorizedItems() {
        return categorizedItems;
    }

    public void setCategorizedItems(Set<MtmTernCategorizedItem> categorizedItems) {
        this.categorizedItems = categorizedItems;
    }

    public void linkCategorizedItem(MtmTernItem item, MtmTernUser addedBy) {
        MtmTernCategorizedItem link = new MtmTernCategorizedItem(item, addedBy);
        categorizedItems.add(link);
    }
}
