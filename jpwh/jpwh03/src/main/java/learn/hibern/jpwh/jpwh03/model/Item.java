package learn.hibern.jpwh.jpwh03.model;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @NotNull
    @Column(nullable = false)
    @Size(
            min = 2,
            max = 255,
            message = "Name is required, maximum 255 characters"
    )
    protected String name;
    @Future
    protected Date auctionEnd;
    @OneToMany(mappedBy = "item")
    protected final Set<Bid> bids = new HashSet<>();

    public Item() {
    }

    public Item(@NotNull @Size(
            min = 2,
            max = 255,
            message = "Name is required, maximum 255 characters"
    ) String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getAuctionEnd() {
        return auctionEnd;
    }

    public void setAuctionEnd(Date auctionEnd) {
        this.auctionEnd = auctionEnd;
    }

    public Set<Bid> getBids() {
        return Collections.unmodifiableSet(bids);
    }

    public void addBid(Bid bid) {
        if (bid == null) {
            throw new NullPointerException("Cannot add null Bid");
        }
        if (bid.getItem() != null) {
            throw new IllegalStateException("Bid is already assigned to an Item");
        }
        bids.add(bid);
        bid.setItem(this);
    }
}
