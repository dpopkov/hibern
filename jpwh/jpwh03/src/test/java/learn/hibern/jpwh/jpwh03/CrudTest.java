package learn.hibern.jpwh.jpwh03;

import learn.hibern.jpwh.jpwh03.model.Bid;
import learn.hibern.jpwh.jpwh03.model.Item;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.math.BigDecimal;

import static org.testng.Assert.*;

public class CrudTest {

    public static final String PERSISTENCE_UNIT_NAME = "ch03domain";

    @Test
    public void testPersistFind() {
        Item item = new Item("TestPersist");
        Bid bid1 = new Bid(BigDecimal.valueOf(100));
        Bid bid2 = new Bid(BigDecimal.valueOf(200));
        item.addBid(bid1);
        item.addBid(bid2);

        EntityManager em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(item);
        em.persist(bid1);
        em.persist(bid2);
        final Long itemId = item.getId();
        assertNotNull(itemId);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        final Item found = em.find(Item.class, itemId);
        assertNotNull(found);
        assertEquals(found.getBids().size(), 2);
        em.getTransaction().commit();
        em.close();
    }
}
