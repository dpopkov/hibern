package learn.hibern.jpwh.jpwh03;

import learn.hibern.jpwh.jpwh03.model.Item;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.Instant;
import java.util.Date;
import java.util.Set;

import static org.testng.Assert.assertEquals;

public class ValidationTest {

    @Test
    public void testValidateItemAuctionEnd() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Item item = new Item("Some Item");
        item.setAuctionEnd(new Date());

        Set<ConstraintViolation<Item>> violations = validator.validate(item);

        assertEquals(violations.size(), 1);
        ConstraintViolation<Item> violation = violations.iterator().next();
        String failedPropertyName = violation.getPropertyPath().iterator().next().getName();
        assertEquals(failedPropertyName, "auctionEnd");
    }

    @Test
    public void testValidateItemName() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Item item = new Item("A");
        item.setAuctionEnd(new Date(Instant.now().plusSeconds(1000).toEpochMilli()));

        Set<ConstraintViolation<Item>> violations = validator.validate(item);

        assertEquals(violations.size(), 1);
        ConstraintViolation<Item> violation = violations.iterator().next();
        String failedPropertyName = violation.getPropertyPath().iterator().next().getName();
        assertEquals(failedPropertyName, "name");
    }
}
