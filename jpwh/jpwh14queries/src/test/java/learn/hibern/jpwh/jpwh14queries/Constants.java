package learn.hibern.jpwh.jpwh14queries;

public class Constants {
    public static final String PERSISTENCE_UNIT_NAME_CREATING = "ch14creatingQueries";
    public static final String PERSISTENCE_UNIT_NAME_PREPARING = "ch14preparingQueries";
    public static final String PERSISTENCE_UNIT_NAME_NAMING = "ch14namingQueries";
}
