package learn.hibern.jpwh.jpwh14queries.executing;

import learn.hibern.jpwh.jpwh14queries.AbstractJpaTest;
import learn.hibern.jpwh.jpwh14queries.Constants;
import learn.hibern.jpwh.jpwh14queries.preparing.PreparingItem;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;

import static org.testng.Assert.*;

public class SingleResultTest extends AbstractJpaTest {

    private static Long itemId;

    @BeforeMethod
    public void setUp() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_UNIT_NAME_PREPARING);
    }

    @AfterMethod
    public void tearDown() {
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 1)
    public void testPopulate() {
        PreparingItem item = new PreparingItem("item-1");
        entityManager.persist(item);
        entityManager.persist(new PreparingItem("item-2"));
        itemId = item.getId();
        assertNotNull(itemId);
    }

    @Test(priority = 2)
    public void testGetSingleResult() {
        TypedQuery<PreparingItem> query = entityManager.createQuery(
                "select i from PreparingItem i where i.id = :id",
                PreparingItem.class)
                .setParameter("id", itemId);
        PreparingItem item = query.getSingleResult();
        assertEquals(item.getId(), itemId);

        TypedQuery<String> queryName = entityManager.createQuery(
                "select i.name from PreparingItem i where i.id = :id",
                String.class)
                .setParameter("id", itemId);
        String name = queryName.getSingleResult();
        assertEquals(name, "item-1");
    }

    @Test(
            priority = 2,
            expectedExceptions = NoResultException.class
    )
    public void testSingleResultNotFound() {
        TypedQuery<PreparingItem> query = entityManager.createQuery(
                "select i from PreparingItem i where i.id = :id",
                PreparingItem.class)
                .setParameter("id", 1234567L);
        query.getSingleResult();
    }

    @Test(
            priority = 2,
            expectedExceptions = NonUniqueResultException.class
    )
    public void testNonUniqueResult() {
        TypedQuery<PreparingItem> query = entityManager.createQuery(
                "select i from PreparingItem i where i.name like :namePattern",
                PreparingItem.class)
                .setParameter("namePattern", "item-%");
        query.getSingleResult();
    }
}
