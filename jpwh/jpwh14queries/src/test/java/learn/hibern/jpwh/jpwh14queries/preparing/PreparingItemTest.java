package learn.hibern.jpwh.jpwh14queries.preparing;

import learn.hibern.jpwh.jpwh14queries.AbstractJpaTest;
import learn.hibern.jpwh.jpwh14queries.Constants;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.testng.Assert.*;

public class PreparingItemTest extends AbstractJpaTest {

    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final LocalDateTime TOMORROW = NOW.plus(1L, ChronoUnit.DAYS);
    private static Long item1Id;
    private static Long item2Id;

    @BeforeMethod
    public void setUp() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_UNIT_NAME_PREPARING);
    }

    @AfterMethod
    public void tearDown() {
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 1)
    public void populate() {
        PreparingItem item1 = new PreparingItem("item-1", NOW);
        PreparingItem item2 = new PreparingItem("item-2", TOMORROW);
        PreparingBid bid11 = new PreparingBid(BigDecimal.ONE, item1);
        PreparingBid bid12 = new PreparingBid(BigDecimal.TEN, item1);
        PreparingBid bid21 = new PreparingBid(BigDecimal.ONE, item2);
        entityManager.persist(item1);
        item1Id = item1.getId();
        entityManager.persist(item2);
        item2Id = item2.getId();
        entityManager.persist(bid11);
        entityManager.persist(bid12);
        entityManager.persist(bid21);
        assertNotNull(item1.getId());
        assertNotNull(item2.getId());
    }

    @Test(priority = 2)
    public void testBindingParameter() {
        String searchString = "item-1";
        Query query = entityManager.createQuery("select i from PreparingItem i where i.name = :itemName");
        query.setParameter("itemName", searchString);
        /*
            Verify that all parameters are bound properly.
         */
        for (Parameter<?> parameter : query.getParameters()) {
            assertTrue(query.isBound(parameter));
        }
        final List<?> list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testBindingTemporalParameter() {
        Query query = entityManager.createQuery("select i from PreparingItem i where i.auctionEnd > :endDate");
        query.setParameter("endDate", NOW);
        final List<?> list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testBindingEntityParameter() {
        Query query = entityManager.createQuery("select b from PreparingBid b where b.item = :item");
        PreparingItem owningItem;
        // Bids of item-1
        owningItem = entityManager.getReference(PreparingItem.class, item1Id);
        query.setParameter("item", owningItem);
        final List<?> list1 = query.getResultList();
        assertEquals(list1.size(), 2);
        // Bids of item-2
        owningItem = entityManager.getReference(PreparingItem.class, item2Id);
        query.setParameter("item", owningItem);
        final List<?> list2 = query.getResultList();
        assertEquals(list2.size(), 1);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Test(priority = 2)
    public void testBindingParameterWithCriteria() {
        String searchString = "item-1";
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery criteria = cb.createQuery();
        Root<PreparingItem> root = criteria.from(PreparingItem.class);

        Query query = entityManager.createQuery(
                criteria.select(root).where(
                        cb.equal(
                                root.get("name"),
                                cb.parameter(String.class, "itemName")
                        )
                )
        ).setParameter("itemName", searchString);
        List list = query.getResultList();
        assertEquals(list.size(), 1);

        /*
            Alternatively, with a ParameterExpression , you don’t have to name the placeholder,
            and the binding of the argument is type-safe
            (you can’t bind an Integer to a ParameterExpression<String>)
         */
        ParameterExpression<String> itemNameParameter = cb.parameter(String.class);
        query = entityManager.createQuery(
                criteria.select(root).where(
                        cb.equal(
                                root.get("name"),
                                itemNameParameter
                        )
                )
        ).setParameter(itemNameParameter, searchString);
        list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testBindingPositionalParameter() {
        String searchString = "%-2";
        Query query = entityManager.createQuery(
                "select i from PreparingItem i where i.name like ?1 and i.auctionEnd > ?2");
        query.setParameter(1, searchString);
        query.setParameter(2, NOW);
        final List<?> list = query.getResultList();
        assertEquals(list.size(), 1);
    }
}
