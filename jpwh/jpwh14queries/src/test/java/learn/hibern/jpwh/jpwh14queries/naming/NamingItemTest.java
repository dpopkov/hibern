package learn.hibern.jpwh.jpwh14queries.naming;

import learn.hibern.jpwh.jpwh14queries.AbstractJpaTest;
import learn.hibern.jpwh.jpwh14queries.Constants;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.Query;

import java.util.List;

import static org.testng.Assert.*;

public class NamingItemTest extends AbstractJpaTest {

    private static Long itemId;

    @BeforeMethod
    public void setUp() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_UNIT_NAME_NAMING);
    }

    @AfterMethod
    public void tearDown() {
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 1)
    public void testPopulate() {
        NamingItem item1 = new NamingItem("item-1");
        NamingItem item2 = new NamingItem("item-2");
        entityManager.persist(item1);
        itemId = item1.getId();
        entityManager.persist(item2);
    }

    @Test(priority = 2)
    @SuppressWarnings("rawtypes")
    public void testNamedQuery() {
        Query query = entityManager.createNamedQuery("findItems");
        final List list = query.getResultList();
        assertEquals(list.size(), 2);
    }

    @Test(priority = 2)
    @SuppressWarnings("rawtypes")
    public void testNamedNativeQuery() {
        Query query = entityManager.createNamedQuery("findItemsSQL");
        final List list = query.getResultList();
        assertEquals(list.size(), 2);
    }

    @Test(priority = 2)
    public void testNamedWithAnnotation() {
        Query query = entityManager.createNamedQuery("findItemById")
                .setParameter("id", itemId);
        Object result = query.getSingleResult();
        NamingItem item = (NamingItem) result;
        assertEquals(item.getId(), itemId);
    }
}
