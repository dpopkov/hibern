package learn.hibern.jpwh.jpwh14queries.creating;

import learn.hibern.jpwh.jpwh14queries.AbstractJpaTest;
import learn.hibern.jpwh.jpwh14queries.Constants;
import org.testng.annotations.Test;


import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.List;

import static org.testng.Assert.*;

public class CreatingItemTest extends AbstractJpaTest {

    private static final String PERSISTENCE_UNIT = Constants.PERSISTENCE_UNIT_NAME_CREATING;
    private static Long itemId;

    @Test(priority = 1)
    public void testCreateJpqlQuery() {
        populateTable();

        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);
        Query query = entityManager.createQuery("select i from CreatingItem i");
        final List<?> list = query.getResultList();
        assertEquals(list.size(), 1);
        commitTransactionAndCloseEntityManager();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Test(priority = 2)
    public void testCreateCriteriaQuery() {
        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);
        /*
        The CriteriaQuery API will appear seamless in your application, without string manipulation.
        It’s the best choice when you can’t fully specify the query at development time
        and the application must create it dynamically at runtime.
        Imagine that you have to implement a search mask in your application, with many check boxes,
        input fields, and switches the user can enable.
        You must dynamically create a database query from the user’s chosen search options.
        With JPQL and string concatenation, such code would be difficult to write and maintain.
         */
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery criteria = cb.createQuery();
        /*
            Each CriteriaQuery has at least one root class specified with from();
            This is called selection.
         */
        criteria.select(criteria.from(CreatingItem.class));
        Query query = entityManager.createQuery(criteria);
        final List<?> list = query.getResultList();
        assertEquals(list.size(), 1);
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 3)
    public void testCreateNativeQuery() {
        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);
        Query query = entityManager.createNativeQuery("select * from CREATING_ITEM", CreatingItem.class);
        final List<?> list = query.getResultList();
        assertEquals(list.size(), 1);
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 4)
    public void testCreateTypedJpqlQuery() {
        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);
        TypedQuery<CreatingItem> query = entityManager.createQuery(
                "select i from CreatingItem i where i.id = :id", CreatingItem.class);
        query.setParameter("id", itemId);
        CreatingItem result = query.getSingleResult();
        assertNotNull(result);
        assertEquals(result.getId(), itemId);
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 5)
    public void testCreateTypedCriteriaQuery() {
        // Given
        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<CreatingItem> criteria = cb.createQuery(CreatingItem.class);
        Root<CreatingItem> i = criteria.from(CreatingItem.class);
        Predicate predicate = cb.equal(i.get("id"), itemId);
        criteria.select(i).where(predicate);
        TypedQuery<CreatingItem> query = entityManager.createQuery(criteria);
        // When
        CreatingItem result = query.getSingleResult();
        // Then
        assertNotNull(result);
        assertEquals(result.getId(), itemId);
        commitTransactionAndCloseEntityManager();
    }

    private void populateTable() {
        CreatingItem item = new CreatingItem("item-1");
        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);
        entityManager.persist(item);
        itemId = item.getId();
        commitTransactionAndCloseEntityManager();
    }
}
