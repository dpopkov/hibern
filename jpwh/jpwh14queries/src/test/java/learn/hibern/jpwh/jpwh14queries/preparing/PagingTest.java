package learn.hibern.jpwh.jpwh14queries.preparing;

import learn.hibern.jpwh.jpwh14queries.AbstractJpaTest;
import learn.hibern.jpwh.jpwh14queries.Constants;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.Query;
import java.util.List;

import static org.testng.Assert.*;

public class PagingTest extends AbstractJpaTest {

    private static final int NUM_ITEMS = 30;

    @BeforeMethod
    public void setUp() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_UNIT_NAME_PREPARING);
    }

    @AfterMethod
    public void tearDown() {
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 1)
    public void populateTable() {
        for (int i = 1; i <= NUM_ITEMS; i++) {
            PreparingItem item = new PreparingItem("item-" + i);
            entityManager.persist(item);
        }
    }

    @SuppressWarnings("rawtypes")
    @Test(priority = 2)
    public void testStartOfResultSet() {
        Query query = entityManager.createQuery("select i from PreparingItem i order by i.id asc");
        List list;
        final int resultSize = 3;
        query.setFirstResult(0).setMaxResults(resultSize);
        list = query.getResultList();
        assertEquals(list.size(), resultSize);
        assertName(list, 0, "item-1");
        assertName(list, 1, "item-2");
        assertName(list, 2, "item-3");
    }

    @SuppressWarnings("rawtypes")
    @Test(priority = 2)
    public void testMiddleOfResultSet() {
        Query query = entityManager.createQuery("select i from PreparingItem i order by i.id asc");
        List list;
        final int firstResultSize = 4;
        query.setFirstResult(10).setMaxResults(firstResultSize);
        list = query.getResultList();
        assertEquals(list.size(), firstResultSize);
        assertName(list, 0, "item-11");
        assertName(list, 1, "item-12");
        assertName(list, 2, "item-13");
        assertName(list, 3, "item-14");

        final int secondResultSize = 5;
        query.setFirstResult(20).setMaxResults(secondResultSize);
        list = query.getResultList();
        assertEquals(list.size(), secondResultSize);
        assertName(list, 0, "item-21");
        assertName(list, 1, "item-22");
        assertName(list, 2, "item-23");
        assertName(list, 3, "item-24");
        assertName(list, 4, "item-25");
    }

    @SuppressWarnings("rawtypes")
    @Test(priority = 2)
    public void testEndOfResultSet() {
        Query query = entityManager.createQuery("select i from PreparingItem i order by i.id asc");
        List list;
        final int resultSize = 3;
        query.setFirstResult(NUM_ITEMS - resultSize).setMaxResults(resultSize);
        list = query.getResultList();
        assertEquals(list.size(), resultSize);
        assertName(list, 0, "item-28");
        assertName(list, 1, "item-29");
        assertName(list, 2, "item-30");
    }

    @SuppressWarnings("rawtypes")
    private void assertName(List list, int i, String expectedName) {
        assertEquals(((PreparingItem) list.get(i)).getName(), expectedName);
    }
}
