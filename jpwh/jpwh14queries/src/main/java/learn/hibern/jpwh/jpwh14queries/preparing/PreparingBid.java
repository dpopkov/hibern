package learn.hibern.jpwh.jpwh14queries.preparing;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "PREPARING_BID")
public class PreparingBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    protected PreparingItem item;

    public PreparingBid() {
    }

    public PreparingBid(@NotNull BigDecimal amount, PreparingItem item) {
        this.amount = amount;
        this.item = item;
    }
}
