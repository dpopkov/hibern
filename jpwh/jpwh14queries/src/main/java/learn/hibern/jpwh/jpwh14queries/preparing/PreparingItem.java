package learn.hibern.jpwh.jpwh14queries.preparing;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "PREPARING_ITEM")
public class PreparingItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;
    protected LocalDateTime auctionEnd;

    public PreparingItem() {
    }

    public PreparingItem(String name) {
        this.name = name;
    }

    public PreparingItem(String name, LocalDateTime auctionEnd) {
        this.name = name;
        this.auctionEnd = auctionEnd;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getAuctionEnd() {
        return auctionEnd;
    }
}
