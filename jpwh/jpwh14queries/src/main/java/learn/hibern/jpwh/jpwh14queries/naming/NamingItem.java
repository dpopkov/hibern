package learn.hibern.jpwh.jpwh14queries.naming;

import javax.persistence.*;
import java.time.LocalDateTime;

@NamedQueries({
    @NamedQuery(
        name = "findItemById",
        query = "select i from NamingItem i where i.id = :id"
    )
})
@Entity
@Table(name = "NAMING_ITEM")
public class NamingItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;
    protected LocalDateTime auctionEnd;

    public NamingItem() {
    }

    public NamingItem(String name) {
        this.name = name;
    }

    public NamingItem(String name, LocalDateTime auctionEnd) {
        this.name = name;
        this.auctionEnd = auctionEnd;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getAuctionEnd() {
        return auctionEnd;
    }
}
