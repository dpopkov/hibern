package learn.hibern.jpwh.jpwh14queries.creating;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CREATING_ITEM")
public class CreatingItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;

    public CreatingItem() {
    }

    public CreatingItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }
}
