package learn.hibern.jpwh.jpwh11tx.forcing;

public class ForcingInvalidBidException extends Exception {
    public ForcingInvalidBidException(String message) {
        super(message);
    }
}
