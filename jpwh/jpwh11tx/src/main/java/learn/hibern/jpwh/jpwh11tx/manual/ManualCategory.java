package learn.hibern.jpwh.jpwh11tx.manual;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MANUAL_CATEGORY")
public class ManualCategory {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;

    public ManualCategory() {
    }

    public ManualCategory(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
