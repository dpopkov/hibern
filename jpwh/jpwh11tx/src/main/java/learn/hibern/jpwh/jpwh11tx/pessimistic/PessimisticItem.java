package learn.hibern.jpwh.jpwh11tx.pessimistic;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "PESSIMISTIC_ITEM")
public class PessimisticItem {
    @Id
    @GeneratedValue
    protected Long id;

    @Version
    protected long version;

    protected String name;

    protected BigDecimal price;

    @ManyToOne(fetch = FetchType.LAZY)
    protected PessimisticCategory category;

    public PessimisticItem() {
    }

    public PessimisticItem(String name) {
        this.name = name;
    }

    public PessimisticItem(String name, PessimisticCategory category) {
        this.name = name;
        this.category = category;
    }

    public PessimisticItem(String name, BigDecimal price, PessimisticCategory category) {
        this.name = name;
        this.price = price;
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public long getVersion() {
        return version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PessimisticCategory getCategory() {
        return category;
    }

    public void setCategory(PessimisticCategory category) {
        this.category = category;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "PessimisticItem{" +
                "id=" + id +
                ", version=" + version +
                ", name='" + name + '\'' +
                ", categoryId=" + category.getId() +
                '}';
    }
}
