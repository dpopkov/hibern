package learn.hibern.jpwh.jpwh11tx.forcing;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "FORCING_ITEM")
public class ForcingItem {
    @Id
    @GeneratedValue
    protected Long id;

    @Version
    protected long version;

    protected String name;

    protected BigDecimal price;

    public ForcingItem() {
    }

    public ForcingItem(String name) {
        this.name = name;
    }


    public ForcingItem(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public long getVersion() {
        return version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ManualItem{" +
                "id=" + id +
                ", version=" + version +
                ", name='" + name + '\'' +
                '}';
    }
}
