package learn.hibern.jpwh.jpwh11tx.pessimistic;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PESSIMISTIC_CATEGORY")
public class PessimisticCategory {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;

    public PessimisticCategory() {
    }

    public PessimisticCategory(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
