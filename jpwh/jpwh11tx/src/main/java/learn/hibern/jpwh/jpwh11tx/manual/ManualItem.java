package learn.hibern.jpwh.jpwh11tx.manual;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "MANUAL_ITEM")
public class ManualItem {
    @Id
    @GeneratedValue
    protected Long id;

    @Version
    protected long version;

    protected String name;

    protected BigDecimal price;

    @ManyToOne(fetch = FetchType.LAZY)
    protected ManualCategory category;

    public ManualItem() {
    }

    public ManualItem(String name) {
        this.name = name;
    }

    public ManualItem(String name, ManualCategory category) {
        this.name = name;
        this.category = category;
    }

    public ManualItem(String name, BigDecimal price, ManualCategory category) {
        this.name = name;
        this.price = price;
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public long getVersion() {
        return version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ManualCategory getCategory() {
        return category;
    }

    public void setCategory(ManualCategory category) {
        this.category = category;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ManualItem{" +
                "id=" + id +
                ", version=" + version +
                ", name='" + name + '\'' +
                ", categoryId=" + category.getId() +
                '}';
    }
}
