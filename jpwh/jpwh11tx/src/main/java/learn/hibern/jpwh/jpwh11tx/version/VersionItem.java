package learn.hibern.jpwh.jpwh11tx.version;

import javax.persistence.*;

@Entity
@Table(name = "VERSION_ITEM")
public class VersionItem {
    @Id
    @GeneratedValue
    protected Long id;

    @Version
    protected long version;

    protected String name;

    public VersionItem() {
    }

    public VersionItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public long getVersion() {
        return version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
