package learn.hibern.jpwh.jpwh11tx.forcing;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "FORCING_BID")
public class ForcingBid {
    @Id
    @GeneratedValue
    protected Long id;

    protected BigDecimal amount;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    protected ForcingItem item;

    protected ForcingBid() {
    }

    public ForcingBid(BigDecimal amount, ForcingItem item, ForcingBid lastBid) throws ForcingInvalidBidException {
        if (lastBid != null && amount.compareTo(lastBid.getAmount()) < 0) {
            throw new ForcingInvalidBidException(
                    "Bid amount '" + amount + "' too low, last bid was: " + lastBid.getAmount());
        }
        this.amount = amount;
        this.item = item;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public ForcingItem getItem() {
        return item;
    }

    @Override
    public String toString() {
        return "ForcingBid{" +
                "id=" + id +
                ", amount=" + amount +
                ", itemId=" + item.getId() +
                '}';
    }
}
