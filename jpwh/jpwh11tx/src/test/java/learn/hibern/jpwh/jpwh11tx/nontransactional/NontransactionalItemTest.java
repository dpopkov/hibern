package learn.hibern.jpwh.jpwh11tx.nontransactional;

import learn.hibern.jpwh.jpwh11tx.AbstractJpaTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class NontransactionalItemTest extends AbstractJpaTest {
    private static final String ORIGINAL_NAME = "Original Name";
    private static final String NEW_NAME = "New Name";

    @Test
    public void testFindChangeAndRefresh() {
        createEntityManagerAndBeginTransaction();
        NontransactionalItem newItem = new NontransactionalItem(ORIGINAL_NAME);
        entityManager.persist(newItem);
        final Long itemId = newItem.getId();
        commitTransactionAndCloseEntityManager();

        /*
            No transaction is active when you create the EntityManager.
            The persistence context is now in a special unsynchronized mode;
            Hibernate won’t flush automatically.
         */
        createEntityManager();

        // This operation executes a SELECT, sent to the database in auto-commit mode.
        NontransactionalItem item = entityManager.find(NontransactionalItem.class, itemId);
        item.setName(NEW_NAME);

        /*
            Usually Hibernate flushes the persistence context when you execute a Query .
            But because the context is unsynchronized, flushing doesn’t occur,
            and the query returns the old, original database value.
            Queries with scalar results aren’t repeatable:
            you see whatever values are in the database and given to Hibernate in the ResultSet.
            This also isn’t a repeatable read if you’re in synchronized mode.
         */
        String queriedItemName = entityManager.createQuery(
                "select i.name from NontransactionalItem i where i.id = :itemId",
                String.class)
                .setParameter("itemId", itemId)
                .getSingleResult();
        assertEquals(queriedItemName, ORIGINAL_NAME);

        /*
            Retrieving a managed entity instance involves a lookup during JDBC result-set marshaling,
            in the current persistence context.
            The already-loaded Item instance with the changed name is returned from the persistence context;
            values from the database are ignored.
            This is a repeatable read of an entity instance, even without a system transaction.
         */
        NontransactionalItem queriedItem = entityManager.createQuery(
                "select i from NontransactionalItem i where i.id = :itemId",
                NontransactionalItem.class)
                .setParameter("itemId", itemId)
                .getSingleResult();
        assertEquals(queriedItem.getName(), NEW_NAME);
        assertEquals(item.getName(), NEW_NAME);
        //noinspection SimplifiedTestNGAssertion
        assertTrue(queriedItem == item);

        /*
            You can roll back the change you made with the refresh() method.
            It loads the current Item state from the database and overwrites the change you made in memory.
         */
        entityManager.refresh(item);
        assertEquals(item.getName(), ORIGINAL_NAME);

        closeEntityManager();
    }
}
