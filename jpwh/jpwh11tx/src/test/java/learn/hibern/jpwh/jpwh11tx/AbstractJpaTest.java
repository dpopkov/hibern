package learn.hibern.jpwh.jpwh11tx;

import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;

import javax.persistence.EntityManager;

public abstract class AbstractJpaTest {

    protected EntityManager entityManager;

    protected void createEntityManager() {
        createEntityManager(Constants.PERSISTENCE_UNIT_NAME);
    }

    protected void createEntityManager(String persistenceUnit) {
        entityManager = JpaEntityManagerUtil.getEntityManager(persistenceUnit);
    }

    protected void createEntityManagerAndBeginTransaction() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_UNIT_NAME);
    }

    protected void createEntityManagerAndBeginTransaction(String persistenceUnit) {
        createEntityManager(persistenceUnit);
        beginTransaction();
    }

    protected void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    protected void commitTransaction() {
        entityManager.getTransaction().commit();
    }

    protected void closeEntityManager() {
        entityManager.close();
    }

    protected void commitTransactionAndCloseEntityManager() {
        commitTransaction();
        entityManager.close();
    }
}
