package learn.hibern.jpwh.jpwh11tx.forcing;

import learn.hibern.jpwh.jpwh11tx.AbstractJpaTest;
import learn.hibern.jpwh.jpwh11tx.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

import java.math.BigDecimal;

import static org.testng.Assert.*;

public class ForcingItemTest extends AbstractJpaTest {

    private static final int SCALE = 2;

    @Test
    public void testVersionIncrementWhenReading() {
        ForcingItem item = new ForcingItem("item-1");

        createEntityManagerAndBeginTransaction();
        entityManager.persist(item);
        final Long itemId = item.getId();
        commitTransactionAndCloseEntityManager();
        assertEquals(item.getVersion(), 0);

        createEntityManagerAndBeginTransaction();
        ForcingItem found = entityManager.find(ForcingItem.class, itemId, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
        ForcingItem found2 = entityManager.find(ForcingItem.class, itemId, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
        assertEquals(found.getVersion(), 0);
        assertEquals(found2.getVersion(), 0);
        commitTransactionAndCloseEntityManager();
        assertEquals(found.getVersion(), 1);    // the version updates when flushing the persistence context
        assertEquals(found2.getVersion(), 1);
        //noinspection SimplifiedTestNGAssertion
        assertTrue(found == found2);
    }

    @Test(expectedExceptions = javax.persistence.RollbackException.class)   // caused by OptimisticLockException
    public void testForcingIncrement() {
        ForcingItem item = new ForcingItem("item-1");
        ForcingBid[] bids = createBids(item, 4);
        final Long itemId = persistTestData(item, bids);

        createEntityManagerAndBeginTransaction();
        ForcingItem foundItem = entityManager.find(ForcingItem.class, itemId, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
        ForcingBid highestBid = findHighestBid(foundItem, entityManager);
        final long nextBidAmount = 50;
        final int expectedItemVersion = 0;
        assertEquals(item.getVersion(), expectedItemVersion);

        // Now a concurrent transaction will place a bid for this item, and succeed because the first commit wins!
        placeBidInConcurrentTransaction(BigDecimal.valueOf(nextBidAmount + 10, SCALE),
                itemId, expectedItemVersion + 1);

        /*
            The code tries to persists a new Bid instance; this does not affect any values of the Item instance.
            Hibernate would not detect concurrently made bids at all without a forced version increment of the Item.
        */
        try {
            ForcingBid newestBid = new ForcingBid(BigDecimal.valueOf(nextBidAmount, SCALE), foundItem, highestBid);
            entityManager.persist(newestBid);
        } catch (ForcingInvalidBidException e) {
            throw new RuntimeException("Failed to create the newest Bid", e);
        }
        /*
            When flushing the persistence context, Hibernate will execute an INSERT for the new Bid and force an
            UPDATE of the Item with a version check. If someone modified the Item concurrently, or placed a
            Bid concurrently with this procedure, Hibernate throws an exception.
        */
        commitTransaction();    // throws javax.persistence.RollbackException caused by OptimisticLockException
        closeEntityManager();
    }

    private Long persistTestData(ForcingItem item, ForcingBid[] bids) {
        createEntityManagerAndBeginTransaction();
        entityManager.persist(item);
        final Long itemId = item.getId();
        for (ForcingBid bid : bids) {
            entityManager.persist(bid);
        }
        commitTransactionAndCloseEntityManager();
        return itemId;
    }

    private void placeBidInConcurrentTransaction(BigDecimal newAmount, Long itemId, int expectedItemVersion) {
        EntityManager otherManager = null;
        try {
            otherManager = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
            otherManager.getTransaction().begin();
            ForcingItem item = otherManager.find(ForcingItem.class, itemId, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
            ForcingBid currentHighest = findHighestBid(item, otherManager);

            try {
                ForcingBid newBid = new ForcingBid(newAmount, item, currentHighest);
                otherManager.persist(newBid);
            } catch (ForcingInvalidBidException e) {
                throw new RuntimeException("Failed to create new Bid", e);  // This should not happen
            }
            otherManager.flush();
            otherManager.getTransaction().commit();
            assertEquals(item.getVersion(), expectedItemVersion);
        } catch (Exception exception) {
            // This should not happen, this commit is the first and should win.
            if (otherManager != null) {
                otherManager.getTransaction().rollback();
            }
            throw new RuntimeException("Concurrent operation failure: " + exception, exception);
        } finally {
            if (otherManager != null) {
                otherManager.close();
            }
        }
    }

    private ForcingBid findHighestBid(ForcingItem item, EntityManager manager) {
        final TypedQuery<ForcingBid> query = manager.createQuery(
                "select b from ForcingBid b where b.item = :item order by b.amount desc",
                    ForcingBid.class)
                .setParameter("item", item)
                .setMaxResults(1);
        return query.getSingleResult();
    }

    private ForcingBid[] createBids(ForcingItem item, int numBids) {
        ForcingBid[] bids = new ForcingBid[numBids];
        long amountValue = 10;
        ForcingBid lastBid = null;
        try {
            for (int i = 0; i < numBids; i++) {
                BigDecimal amount = BigDecimal.valueOf(amountValue, SCALE);
                amountValue++;
                ForcingBid bid = new ForcingBid(amount, item, lastBid);
                lastBid = bid;
                bids[i] = bid;
            }
        } catch (ForcingInvalidBidException e) {
            throw new RuntimeException("Creation of bid failed", e);
        }
        return bids;
    }
}
