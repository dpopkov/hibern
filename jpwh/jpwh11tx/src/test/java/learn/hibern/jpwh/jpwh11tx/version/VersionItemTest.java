package learn.hibern.jpwh.jpwh11tx.version;

import learn.hibern.jpwh.jpwh11tx.AbstractJpaTest;
import learn.hibern.jpwh.jpwh11tx.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.sql.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import static org.testng.Assert.*;

public class VersionItemTest extends AbstractJpaTest {

    @Test
    public void testIncrementingVersion() {
        VersionItem item = new VersionItem("item-1");
        createEntityManagerAndBeginTransaction();
        entityManager.persist(item);
        final Long id = item.getId();
        commitTransaction();

        beginTransaction();
        VersionItem found = entityManager.find(VersionItem.class, id);
        assertEquals(0, found.getVersion());
        found.setName("item-1-changed");
        entityManager.flush();

        VersionItem found2 = entityManager.find(VersionItem.class, id);
        assertEquals(1, found2.getVersion());
        commitTransactionAndCloseEntityManager();
    }

    @Test
    public void testIncrementingVersionWithException() throws SQLException {
        VersionItem item = new VersionItem("item-1");
        createEntityManagerAndBeginTransaction();
        entityManager.persist(item);
        final Long id = item.getId();
        commitTransactionAndCloseEntityManager();

        createEntityManagerAndBeginTransaction();
        VersionItem found = entityManager.find(VersionItem.class, id);
        assertEquals(0, found.getVersion());
        found.setName("item-1-changed");
        entityManager.flush();

//        new VersionChanger("version_item").change();  // org.h2.jdbc.JdbcSQLTimeoutException

        VersionItem found2 = entityManager.find(VersionItem.class, id);
        assertEquals(1, found2.getVersion());
        commitTransactionAndCloseEntityManager();
    }

    @Ignore("Tests not Item, but VersionChanger")
    @Test
    public void testEnv() throws SQLException {
        new VersionChanger("version_item").change();
    }

    @Test(expectedExceptions = {javax.persistence.OptimisticLockException.class})
    public void testOptimisticLockException() throws ExecutionException, InterruptedException {
        VersionItem item = new VersionItem("item-1");

        EntityManager entityManager = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        entityManager.getTransaction().begin();
        entityManager.persist(item);
        final Long itemId = item.getId();
        entityManager.getTransaction().commit();
        entityManager.close();

        entityManager = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        entityManager.getTransaction().begin();
        VersionItem found = entityManager.find(VersionItem.class, itemId);
        assertEquals(found.getVersion(), 0);
        found.setName("item-1-changed");

        Executors.newSingleThreadExecutor().submit(() -> {
            final EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
            try {
                em.getTransaction().begin();
                VersionItem other = em.find(VersionItem.class, itemId);
                assertEquals(other.getVersion(), 0);
                other.setName("item-1-other");
                /*
                    This commit is actually the First Commit and
                    it should win.
                 */
                em.getTransaction().commit();
            } catch (Exception ex) {
                // This should not happen
                em.getTransaction().rollback();
                throw new RuntimeException("Concurrent operation failure: " + ex, ex);
            }
            return null;
        }).get();

        /*
            This commit is the Second Commit and it attempts to update data with changed version.
            It should throw OptimisticLockException.
         */
        entityManager.flush();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    private static class VersionChanger {
        private final String tableName;
        private final String url;

        private VersionChanger(String tableName) {
            this.tableName = tableName;
            String userDir = System.getProperty("user.dir");
            url = "jdbc:h2:" + userDir + "/ch11tx";
        }

        public void change() throws SQLException {
            try (Connection connection = DriverManager.getConnection(url, "sa", "")) {
                printIsolationLevel(connection);
                connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
                printIsolationLevel(connection);
                final boolean success = decrementVersion(connection, 1L);
                System.out.println("Change " + (success ? " successful" : " failed"));
            }
        }

        private boolean decrementVersion(Connection connection, Long id) throws SQLException {
            final String selectSql = "SELECT version FROM " + tableName + " WHERE id = ?";
            final PreparedStatement selectSt = connection.prepareStatement(selectSql);
            selectSt.setLong(1, id);
            final ResultSet resultSet = selectSt.executeQuery();
            long oldVersion;
            if (resultSet.next()) {
                oldVersion = resultSet.getLong("version");
            } else {
                throw new IllegalStateException("Failed to read old value");
            }
            String sql = "UPDATE " + tableName + " SET version = ? where id = ? and version = ?";
            final PreparedStatement st = connection.prepareStatement(sql);
            final long newVersion = oldVersion - 1;
            st.setLong(1, newVersion);
            st.setLong(2, id);
            st.setLong(3, oldVersion);
            final int count = st.executeUpdate();
            return count == 1;
        }

        public static void printIsolationLevel(Connection connection) throws SQLException {
            final int isolation = connection.getTransactionIsolation();
            String level;
            switch (isolation) {
                case Connection.TRANSACTION_READ_UNCOMMITTED:
                    level = "TRANSACTION_READ_UNCOMMITTED";
                    break;
                case Connection.TRANSACTION_READ_COMMITTED:
                    level = "TRANSACTION_READ_COMMITTED";
                    break;
                case Connection.TRANSACTION_REPEATABLE_READ:
                    level = "TRANSACTION_REPEATABLE_READ";
                    break;
                case Connection.TRANSACTION_SERIALIZABLE:
                    level = "TRANSACTION_SERIALIZABLE";
                    break;
                case Connection.TRANSACTION_NONE:
                    level = "TRANSACTION_NONE";
                    break;
                default:
                    level = "Unknown isolation level";
                    break;
            }
            System.out.println("Isolation level: " + level);
        }
    }
}
