package learn.hibern.jpwh.jpwh11tx.manual;

import learn.hibern.jpwh.jpwh11tx.AbstractJpaTest;
import learn.hibern.jpwh.jpwh11tx.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import static org.testng.Assert.assertEquals;

public class ManualItemTest extends AbstractJpaTest {

    @Test
    public void testPersistAndQuery() {
        ManualCategory category = new ManualCategory("category-1");
        ManualItem item1 = new ManualItem("item-1", category);
        ManualItem item2 = new ManualItem("item-2", category);

        createEntityManagerAndBeginTransaction();
        entityManager.persist(category);
        entityManager.persist(item1);
        entityManager.persist(item2);
        final Long categoryId = category.getId();
        commitTransactionAndCloseEntityManager();

        createEntityManagerAndBeginTransaction();
        ManualCategory categoryProxy = entityManager.getReference(ManualCategory.class, categoryId);
        final TypedQuery<ManualItem> query = entityManager.createQuery(
                "select i from ManualItem i join fetch i.category where i.category = :category",
                ManualItem.class);
        query.setParameter("category", categoryProxy);
        final List<ManualItem> resultList = query.getResultList();
        assertEquals(resultList.size(), 2);
        commitTransactionAndCloseEntityManager();
    }

    // RollbackException is caused by: javax.persistence.OptimisticLockException
    @Test(expectedExceptions = javax.persistence.RollbackException.class)
    public void testManualCheck() throws ExecutionException, InterruptedException {
        BigDecimal price = BigDecimal.valueOf(27, 2);
        ManualCategory category1 = new ManualCategory("category-1");
        ManualCategory category2 = new ManualCategory("category-2");
        ManualItem item11 = new ManualItem("item-11", price, category1);
        ManualItem item12 = new ManualItem("item-12", price, category1);
        ManualItem item21 = new ManualItem("item-11", price, category2);
        ManualItem item22 = new ManualItem("item-12", price, category2);
        ManualItem item23 = new ManualItem("item-13", price, category2);

        EntityManager entityManager = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        entityManager.getTransaction().begin();
        entityManager.persist(category1);
        entityManager.persist(category2);
        final Long firstCategoryId = category1.getId();
        final Long lastCategoryId = category2.getId();
        final List<Long> categoryIds = new ArrayList<>();
        categoryIds.add(firstCategoryId);
        categoryIds.add(lastCategoryId);
        entityManager.persist(item11);
        final Long item11Id = item11.getId();
        entityManager.persist(item12);
        entityManager.persist(item21);
        entityManager.persist(item22);
        entityManager.persist(item23);
        entityManager.getTransaction().commit();
        entityManager.close();

        entityManager = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        entityManager.getTransaction().begin();
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (Long id : categoryIds) {
            /*
                For each Category , query all Item instances with an OPTIMISTIC lock mode.
                Hibernate now knows it has to check each Item at flush time.
             */
            List<ManualItem> items = entityManager.createQuery(
                    "select i from ManualItem  i where i.category.id = :categoryId",
                    ManualItem.class)
                    .setLockMode(LockModeType.OPTIMISTIC)
                    .setParameter("categoryId", id)
                    .getResultList();
            for (ManualItem item : items) {
                totalPrice = totalPrice.add(item.getPrice());
            }
            if (id.equals(firstCategoryId)) {
                moveItemToOtherCategory(item11Id, lastCategoryId);
            }
        }
        /*
            For each Item loaded earlier with the locking query, Hibernate executes a SELECT during flushing.
            It checks whether the database version of each ITEM row is still the same as when it was loaded.
            If any ITEM row has a different version or the row no longer exists,
            an OptimisticLockException is thrown.
         */
        /*
            This commit will throw javax.persistence.RollbackException
            Caused by: javax.persistence.OptimisticLockException
         */
        entityManager.getTransaction().commit();
        entityManager.close();

        assertEquals(BigDecimal.valueOf(135, 2), totalPrice);
    }

    private void moveItemToOtherCategory(Long item, Long categoryId) throws InterruptedException, ExecutionException {
        Executors.newSingleThreadExecutor().submit(() -> {
            EntityManager em2 = null;
            try {
                em2 = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
                em2.getTransaction().begin();
                ManualItem foundFirstItem = em2.find(ManualItem.class, item);
                ManualCategory lastCategoryRef = em2.getReference(ManualCategory.class, categoryId);
                foundFirstItem.setCategory(lastCategoryRef);
                em2.flush();
                em2.getTransaction().commit();
            } catch (Exception ex) {    // This should not happen
                if (em2 != null) {
                    em2.getTransaction().rollback();
                    throw new RuntimeException("Concurrent operation failure: " + ex, ex);
                }
            } finally {
                if (em2 != null) {
                    em2.close();
                }
            }
            return null;
        }).get();
    }
}
