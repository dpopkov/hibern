package learn.hibern.jpwh.jpwh11tx.pessimistic;

import learn.hibern.jpwh.jpwh11tx.AbstractJpaTest;
import learn.hibern.jpwh.jpwh11tx.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import static org.testng.Assert.*;

public class PessimisticItemTest extends AbstractJpaTest {

    @Test
    public void testManualCheck() throws ExecutionException, InterruptedException {
        BigDecimal price = BigDecimal.valueOf(27, 2);
        PessimisticCategory category1 = new PessimisticCategory("category-1");
        PessimisticCategory category2 = new PessimisticCategory("category-2");
        PessimisticItem item11 = new PessimisticItem("item-11", price, category1);
        PessimisticItem item12 = new PessimisticItem("item-12", price, category1);
        PessimisticItem item21 = new PessimisticItem("item-21", price, category2);
        PessimisticItem item22 = new PessimisticItem("item-22", price, category2);
        PessimisticItem item23 = new PessimisticItem("item-23", price, category2);

        EntityManager entityManager = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        entityManager.getTransaction().begin();
        entityManager.persist(category1);
        entityManager.persist(category2);
        final Long firstCategoryId = category1.getId();
        final Long lastCategoryId = category2.getId();
        final List<Long> categoryIds = new ArrayList<>();
        categoryIds.add(firstCategoryId);
        categoryIds.add(lastCategoryId);
        entityManager.persist(item11);
        final Long item11Id = item11.getId();
        entityManager.persist(item12);
        entityManager.persist(item21);
        entityManager.persist(item22);
        entityManager.persist(item23);
        entityManager.getTransaction().commit();
        entityManager.close();

        entityManager = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
        entityManager.getTransaction().begin();
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (Long id : categoryIds) {
            /*
                For each Category , query all Item instances with an PESSIMISTIC_READ lock mode.
                Hibernate locks the rows in the database with the SQL query.
                If possible, wait (if supported by dialect) 3 seconds if another transaction holds a conflicting lock.
                If the lock can’t be obtained, the query throws an exception.
             */
            List<PessimisticItem> items = entityManager.createQuery(
                    "select i from PessimisticItem  i where i.category.id = :categoryId",
                    PessimisticItem.class)
                    .setLockMode(LockModeType.PESSIMISTIC_READ)
                    .setHint("javax.persistence.lock.timeout", 3000)
                    .setParameter("categoryId", id)
                    .getResultList();
            /*
                If the query returns successfully, you know that you hold an exclusive lock on the data
                and no other transaction can access it with an exclusive lock or modify it until this
                transaction commits.
             */
            for (PessimisticItem item : items) {
                totalPrice = totalPrice.add(item.getPrice());
            }
             /*
                 Now a concurrent transaction will try to obtain a write lock,
                 it fails because we hold a read lock on the data already.
                 Note that on H2 there actually are no read or write locks, only exclusive locks.
             */
            if (id.equals(firstCategoryId)) {
                moveItemToOtherCategory(item11Id, lastCategoryId);
            }
        }
        /*
            Our locks will be released after commit, when the transaction completes.
         */
        entityManager.getTransaction().commit();
        entityManager.close();

        assertEquals(BigDecimal.valueOf(135, 2), totalPrice);
    }

    private void moveItemToOtherCategory(Long item, Long categoryId) throws InterruptedException, ExecutionException {
        Executors.newSingleThreadExecutor().submit(() -> {
            EntityManager em2 = null;
            try {
                em2 = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
                em2.getTransaction().begin();
                /*
                    Moving the first item from the first category into the last category
                    This query should fail as someone else holds a lock on the rows.
                */
                Map<String, Object> hints = Map.of("javax.persistence.lock.timeout", 1000);
                PessimisticItem foundFirstItem = em2.find(PessimisticItem.class, item,
                        LockModeType.PESSIMISTIC_WRITE, hints);
                PessimisticCategory lastCategoryRef = em2.getReference(PessimisticCategory.class, categoryId);
                foundFirstItem.setCategory(lastCategoryRef);
                em2.flush();
                em2.getTransaction().commit();
            } catch (Exception ex) {    // This should fail, as the data is already locked
                if (em2 != null) {
                    em2.getTransaction().rollback();
                    // On h2 we get javax.persistence.LockTimeoutException
                    assertTrue(ex instanceof javax.persistence.LockTimeoutException);
                }
            } finally {
                if (em2 != null) {
                    em2.close();
                }
            }
            return null;
        }).get();
    }
}
