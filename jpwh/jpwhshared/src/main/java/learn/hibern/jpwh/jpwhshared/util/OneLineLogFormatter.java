package learn.hibern.jpwh.jpwhshared.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class OneLineLogFormatter extends Formatter {

    public String format(LogRecord record) {
        StringBuilder buf = new StringBuilder(180);
        DateFormat dateFormat = new SimpleDateFormat("kk:mm:ss,SS");

        buf.append("[").append(pad(Thread.currentThread().getName(), 32)).append("] ");
        buf.append(pad(record.getLevel().toString(), 12));
        buf.append(" - ");
        buf.append(pad(dateFormat.format(new Date(record.getMillis())), 24));
        buf.append(" - ");
        buf.append(truncate(record.getLoggerName(), 30));
        buf.append(": ");
        buf.append(formatMessage(record));
        buf.append("\n");

        Throwable throwable = record.getThrown();
        if (throwable != null) {
            StringWriter sink = new StringWriter();
            throwable.printStackTrace(new PrintWriter(sink, true));
            buf.append(sink.toString());
        }

        return buf.toString();
    }

    public String pad(String s, int size) {
        if (s.length() < size) {
            StringBuilder sBuilder = new StringBuilder(s);
            sBuilder.append(" ".repeat(Math.max(0, size - sBuilder.length() - sBuilder.length())));
            s = sBuilder.toString();
        }
        return s;
    }

    public String truncate(String name, int maxLength) {
        return name.length() > maxLength ? name.substring(name.length() - maxLength) : name;
    }

}
