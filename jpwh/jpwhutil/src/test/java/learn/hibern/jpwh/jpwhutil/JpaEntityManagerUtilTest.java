package learn.hibern.jpwh.jpwhutil;

import learn.hibern.jpwh.jpwhutil.model.Thing;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import java.util.Random;

import static org.testng.Assert.*;

public class JpaEntityManagerUtilTest {

    public static final String PERSISTENCE_UNIT_NAME = "jpwhutil";

    @Test
    public void testGetEntityManager() {
        EntityManager em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        assertNotNull(em);
        em.close();
    }

    @Test(expectedExceptions = {javax.persistence.PersistenceException.class})
    public void nonexistentEntityManagerName() {
        JpaEntityManagerUtil.getEntityManager("nonexistent");
        fail("We shouldn't be able to acquire an EntityManager here");
    }

    @Test
    public void testEntityManager() {
        EntityManager em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        Thing thing = new Thing();
        String name = randomName();
        thing.setName(name);
        em.persist(thing);
        final Integer thingId = thing.getId();
        assertNotNull(thingId);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        TypedQuery<Thing> q = em.createQuery("from Thing t where t.name=:name", Thing.class);
        q.setParameter("name", name);
        Thing result = q.getSingleResult();
        assertNotNull(result);
        assertEquals(thing, result);

        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void getSession() {
        Session session = JpaEntityManagerUtil.getSession(PERSISTENCE_UNIT_NAME);
        assertNotNull(session);
        session.close();
    }

    @Test(expectedExceptions = {javax.persistence.PersistenceException.class})
    public void nonexistentSessionName() {
        JpaEntityManagerUtil.getSession("nonexistent");
        fail("We shouldn't be able to acquire a Session here");
    }

    @Test
    public void testSession() {
        Integer thingId;
        String name = randomName();
        try (Session session = JpaEntityManagerUtil.getSession(PERSISTENCE_UNIT_NAME)) {
            final Transaction tx = session.beginTransaction();
            final Thing thing = new Thing();
            thing.setName(name);
            session.save(thing);
            thingId = thing.getId();
            assertNotNull(thingId);
            tx.commit();
        }

        try (Session session = JpaEntityManagerUtil.getSession(PERSISTENCE_UNIT_NAME)) {
            final Transaction tx = session.beginTransaction();
            final Thing found = session.get(Thing.class, thingId);
            assertNotNull(found);
            assertEquals(found.getName(), name);
            tx.commit();
        }
    }

    private String randomName() {
        return "Thing " + new Random().nextInt();
    }
}
