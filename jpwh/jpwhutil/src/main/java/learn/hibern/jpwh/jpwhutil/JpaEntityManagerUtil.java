package learn.hibern.jpwh.jpwhutil;

import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

public class JpaEntityManagerUtil {

    private static final Map<String, EntityManagerFactory> persistenceUnits = new HashMap<>();

    public static synchronized EntityManager getEntityManager(String persistenceUnitName) {
        EntityManagerFactory entityManagerFactory = persistenceUnits.get(persistenceUnitName);
        if (entityManagerFactory == null) {
            entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnitName);
            persistenceUnits.put(persistenceUnitName, entityManagerFactory);
        }
        return entityManagerFactory.createEntityManager();
    }

    public static Session getSession(String persistenceUnitName) {
        return getEntityManager(persistenceUnitName).unwrap(Session.class);
    }
}
