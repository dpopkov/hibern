Java Persistence with Hibernate
===============================

History
-------
* Modules jpwhenv, jpwhshared, jpwhmodel, jpwhexamples.
* Ch 3 Domain models -- module jpwh03: 
    * Simple entities: Item, Bid.
    * Bean Validation.
* Ch 5 Mapping value types -- module jpwh05:
    * Temporal properties
        * @javax.persistence.Temporal
        * javax.persistence.TemporalType
        * @org.hibernate.annotations.CreationTimestamp
        * @org.hibernate.annotations.UpdateTimestamp
    * Mapping enumerations
        * @javax.persistence.Enumerated
        * javax.persistence.EnumType (STRING, ORDINAL)
    * Making classes embeddable
        * @javax.persistence.Embeddable
    * Overriding embedded attributes
        * @javax.persistence.AttributeOverrides
        * @javax.persistence.AttributeOverride
    * Mapping nested embedded components
    * Mapping Java and SQL types with converters
        * Built-in types
            * The default length for all _java.lang.String_ properties is __255__
            * Lazy loading without bytecode instrumentation
                * java.sql.Clob
                * java.sql.Blob
        * Creating custom JPA converters
            * javax.persistence.AttributeConverter
            * @javax.persistence.Converter
            * @javax.persistence.Convert
            * Converting properties of components 
* Ch 6 Mapping inheritance
    * Table per concrete class with __implicit polymorphism__
        * @javax.persistence.MappedSuperclass
        * @javax.persistence.AttributeOverride
    * Table per concrete class with __unions__
        * @javax.persistence.Entity
        * @javax.persistence.Inheritance
        * javax.persistence.InheritanceType.TABLE_PER_CLASS
    * Single table per __class hierarchy__
        * @javax.persistence.Entity
        * @javax.persistence.Inheritance
        * javax.persistence.InheritanceType.SINGLE_TABLE
        * @javax.persistence.DiscriminatorColumn
    * Table per subclass with __joins__
        * @javax.persistence.Entity
        * @javax.persistence.Inheritance
        * javax.persistence.InheritanceType.JOINED
    * Polymorphic associations
        * Polymorphic ManyToOne associations
    * Research Mixing Strategies of Inheritance mapping
        * @MappedSuperclass - then InheritanceType.JOINED
* Ch 7 Mapping collections and entity associations
    * Sets, bags, lists, and maps of value types
        * Mapping a set
        * Mapping a list
        * Mapping a map
    * Collections of components
        * Equality of component instances
        * Set of components
        * Map of component values
        * Components as map keys
        * Collection in an embeddable component
    * Mapping entity associations
        * The simplest possible association - unidirectional many-to-one
        * Bidirectional one-to-many
        * Cascading state
            * Enabling transitive persistence: CascadeType.PERSIST
            * Cascading deletion: CascadeType.REMOVE
            * Enabling orphan removal 
            * Orphan removal conflict with shared references
* Ch 8 Advanced entity association mappings
    * One-to-one associations
        * Sharing a primary key
        * Using a foreign key join column
        * Using a join table
    * One-to-many associations
        * Considering one-to-many __bags__ (bags have the most efficient performance characteristics)
        * Unidirectional and bidirectional __list__ mappings (with @OrderColumn)
            * Unidirectional:
                * Item has bids associations (uses @JoinColumn and @OrderColumn)
                * Bid has no item association
            * Bidirectional:
                * @JoinColumn is used on both sides
                * On "Many" side set `updatable = false, insertable = false`
        * Optional one-to-many with a join table
            * @JoinTable(name, joinColumns, inverseColumns) must be on Many side
        * One-to-many association in an embeddable class
            * Put @JoinColumn in the embeddable - it creates a column in the entity on Many side
    * Many-to-many associations and ternary associations
        * Unidirectional and bidirectional many-to-many associations
        * Many-to-many with an intermediate entity
            * @EmbeddedId - maps identifier and composite key columns
        * Ternary association with components
            * @Embeddable component has associations with 2 entities and is part of the 3rd entity
    * Entity associations with Maps
        * One-to-many with a property key
            * @javax.persistence.MapKey
        * Key/Value ternary relationship
            * @ManyToMany(cascade = CascadeType.PERSIST)
            * @MapKeyJoinColumn(name = "ITEM_ID")
            * @JoinTable(
                * name = "MAP_TERN_CATEGORY_ITEM_USER",
                * joinColumns = @JoinColumn(name = "CATEGORY_ID"),
                * inverseJoinColumns = @JoinColumn(name = "USER_ID"))
* Ch 10 Managing data
    * The EntityManager interface
        * Getting a reference
        * Flushing the persistence context (using flush mode)
    * Working with detached state
        * The identity of detached instances
        * Implementing equality methods

* Ch 11 Transactions and concurrency
    * Controlling concurrent access
        * Optimistic concurrency control
            * Enabling versioning: `@javax.persistence.Version`
            * Manual version checking
                * Using Query#setLockMode(javax.persistence.LockModeType)
                * LockModeType.OPTIMISTIC
            * Forcing a version increment
                * LockModeType.OPTIMISTIC_FORCE_INCREMENT
        * Explicit pessimistic locking
            * LockModeType.PESSIMISTIC_READ
            * javax.persistence.lock.timeout
            * LockModeType.PESSIMISTIC_WRITE
    * Non-transactional data access
        * Reading data in auto-commit mode
* Ch 12 Fetch plans, strategies, and profiles
    * Lazy and eager loading
        * Understanding entity proxies
        * Lazy persistent collections
    * Selecting a fetch strategy
        * Dynamic eager fetching
            * In query
                * `select i from Item i join fetch i.seller`
                * `select i from Item i left join fetch i.bids`
            * Using CriteriaQuery
* Ch 13 Filtering data
    * Cascading state transitions
        * Transitive detachment and merging
        * Cascading refresh: [model](jpwh13filter/src/main/java/learn/hibern/jpwh/jpwh13filter/cascade/refresh), [test](jpwh13filter/src/test/java/learn/hibern/jpwh/jpwh13filter/cascade/refresh/RefreshUserTest.java)
    * Listening to and intercepting events
        * JPA event listeners and callbacks: [model](jpwh13filter/src/main/java/learn/hibern/jpwh/jpwh13filter/callback), [test](jpwh13filter/src/test/java/learn/hibern/jpwh/jpwh13filter/callback/CallbackUserTest.java)
        * Implementing Hibernate interceptors (need to fix): [model](jpwh13filter/src/main/java/learn/hibern/jpwh/jpwh13filter/interceptor), [test](jpwh13filter/src/test/java/learn/hibern/jpwh/jpwh13filter/interceptor)
    * Auditing and versioning with Hibernate Envers: [model](jpwh13filter/src/main/java/learn/hibern/jpwh/jpwh13filter/envers), [test](jpwh13filter/src/test/java/learn/hibern/jpwh/jpwh13filter/envers/EnversUserTest.java)
        * Enabling audit logging
        * Creating an audit trail
        * Finding revisions
        * Accessing historical data
            * Loading historical versions of entity instances
            * Querying historical entity instances
            * Rolling back an entity to older version
    * Dynamic data filters: [model](jpwh13filter/src/main/java/learn/hibern/jpwh/jpwh13filter/dynamic), [test](jpwh13filter/src/test/java/learn/hibern/jpwh/jpwh13filter/dynamic/DynamicUserTest.java)
        * Defining dynamic filters
        * Applying the filter
        * Enabling the filter
* Ch 14 Creating and executing queries: [chapter-14](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511)
    * Creating Queries ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#4e48bc79ebc845db966b44e22d964d87))
        * The JPA query interfaces ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#f1a55b3134f24809b081213e7bff8e6f))
        * Typed query results ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#5943a3e4df34419285304e66ef83d499))
    * Preparing Queries ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#4ac9edb18a3f43e1b434af33385a99d2))
        * Binding named parameters ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#ee5013a769094e44b15c89ce9e581ee7))
        * Using positional parameters ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#abffaf6197f04afd8ae46a315c76d5b2))
        * Paging through large result sets ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#6bf1a41da08443f7ad2f63f094225905))
    * Executing Queries ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#4630e99c3bc74e4a93a0d465368316fe))
        * Listing all results ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#734615713b594d439d6b7cf52542c618))
        * Getting a single result ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#3ab9aa3ebf424815bcc8d9fe456355bf))
    * Naming and externalizing queries ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#586fd9b9849746eea6581924e1a76d50))
        * Defining queries in XML metadata ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#3e9d00afacfb483db4151acb23304bda))
        * Defining queries with annotations ([note](https://www.notion.so/14-Creating-and-executing-queries-b9f19700d59f431a8c40f03e738ee511#43bdd2e301094c6d99374d9982a05103))
* Ch 15 The query languages ([note](https://www.notion.so/15-The-query-languages-b4feb49bbfcb48fc899fc9ce8c2c178f))
    * Selection ([note](https://www.notion.so/15-The-query-languages-b4feb49bbfcb48fc899fc9ce8c2c178f#daa4faa407a94aab97383c7ca6d7552a))
        * Assigning aliases and query roots ([note](https://www.notion.so/15-The-query-languages-b4feb49bbfcb48fc899fc9ce8c2c178f#26bfab8fe404417e8dc3abe82b2c3572))
        * Polymorphic queries ([note](https://www.notion.so/15-The-query-languages-b4feb49bbfcb48fc899fc9ce8c2c178f#586c63075aad47cbb4e86a8a12e83008))
    * Restriction ([note](https://www.notion.so/15-The-query-languages-b4feb49bbfcb48fc899fc9ce8c2c178f#ee80ab90fe42405d84627ca790a064d6))
        * Comparison expressions ([note](https://www.notion.so/15-The-query-languages-b4feb49bbfcb48fc899fc9ce8c2c178f#5fab840aa84b43cd94c93d9d603ba690))
        * Expressions with collections ([note](https://www.notion.so/15-The-query-languages-b4feb49bbfcb48fc899fc9ce8c2c178f#5304ec7c83eb47a6860c85ecf05354f4))
    * Projection ([note](https://www.notion.so/15-The-query-languages-b4feb49bbfcb48fc899fc9ce8c2c178f#7c1c394e1249463fa0cffc4f96fd96c6))
        * Projection of entities and scalar values ([note](https://www.notion.so/15-The-query-languages-b4feb49bbfcb48fc899fc9ce8c2c178f#bad4276b9be743d998f775dfcc2264dd))
