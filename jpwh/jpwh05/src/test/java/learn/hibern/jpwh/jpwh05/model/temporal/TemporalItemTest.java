package learn.hibern.jpwh.jpwh05.model.temporal;

import learn.hibern.jpwh.jpwh05.AbstractJpaTest;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.testng.Assert.*;

public class TemporalItemTest extends AbstractJpaTest {

    @Test
    public void testCreationTimestamp() {
        TemporalItem item = new TemporalItem("item-1");
        assertNull(item.getCreatedOn());

        createEntityManagerAndBeginTransaction();
        entityManager.persist(item);
        commitTransactionAndCloseEntityManager();

        assertNotNull(item.getCreatedOn());
        System.out.println("item.getCreatedOn() = " + item.getCreatedOn());
    }

    @Test
    public void testModificationTimestamp() {
        TemporalItem item = new TemporalItem("item-2");
        assertNull(item.getModifiedOn());

        createEntityManagerAndBeginTransaction();
        entityManager.persist(item);
        final Long itemId = item.getId();
        final LocalDateTime modifiedOn1 = item.getModifiedOn();
        assertNotNull(modifiedOn1);
        commitTransaction();

        beginTransaction();
        TemporalItem found = entityManager.find(TemporalItem.class, itemId);
        found.setName("item-2-modified");
        commitTransaction();

        beginTransaction();
        found = entityManager.find(TemporalItem.class, itemId);
        final LocalDateTime modifiedOn2 = found.getModifiedOn();
        System.out.println("modifiedOn1 = " + modifiedOn1);
        System.out.println("modifiedOn2 = " + modifiedOn2);
        assertNotNull(modifiedOn2);
        commitTransactionAndCloseEntityManager();
        assertNotEquals(modifiedOn1, modifiedOn2);
    }
}
