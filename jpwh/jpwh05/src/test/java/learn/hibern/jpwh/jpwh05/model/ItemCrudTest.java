package learn.hibern.jpwh.jpwh05.model;

import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.*;

public class ItemCrudTest {

    public static final String PERSISTENCE_UNIT_NAME = "ch05basic";

    @Test
    public void testPersistFind() {
        Item item = new Item("TestPersist");
        item.setBuyNowPrice(MonetaryAmount.from("123.40 USD"));
        Bid bid1 = new Bid(BigDecimal.valueOf(100));
        Bid bid2 = new Bid(BigDecimal.valueOf(200));
        item.addBid(bid1);
        item.addBid(bid2);

        EntityManager em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(item);
        assertNotNull(item.getCreatedOn());
        em.persist(bid1);
        em.persist(bid2);
        final Long itemId = item.getId();
        assertNotNull(itemId);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        final Item found = em.find(Item.class, itemId);
        assertNotNull(found);
        assertEquals(found.getBids().size(), 2);
        final MonetaryAmount price = found.getBuyNowPrice();
        assertNotNull(price);
        assertEquals(price.getValue(), new BigDecimal("123.40"));
        assertEquals(price.getCurrency(), Currency.getInstance("USD"));
        em.getTransaction().commit();
        em.close();
    }
}
