package learn.hibern.jpwh.jpwh05.model;

import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.*;

public class UserCrudTest {

    public static final String PERSISTENCE_UNIT_NAME = "ch05basic";

    @Test
    public void testPersistFind() {
        User user = new User("Jane Dow");
        user.setHomeAddress(new Address("Street", new City(new GermanZipcode("12345"),"City", "DE")));
        user.setBillingAddress(new Address("b_street", new City(new SwissZipcode("4321"), "b_city", "FR")));
        EntityManager em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(user);
        Long userId = user.getId();
        assertNotNull(userId);
        em.getTransaction().commit();
        em.close();

        em = JpaEntityManagerUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        User found = em.find(User.class, userId);
        assertNotNull(found);
        assertEquals(user.getUsername(), "Jane Dow");
        Address homeAddress = user.getHomeAddress();
        assertNotNull(homeAddress);
        City homeCity = homeAddress.getCity();
        assertNotNull(homeCity);
        assertEquals(homeCity.getZipcode(), new GermanZipcode("12345"));
        Address billingAddress = user.getBillingAddress();
        assertNotNull(billingAddress);
        assertEquals(billingAddress.getStreet(), "b_street");
        assertEquals(billingAddress.getCity().getZipcode(), new SwissZipcode("4321"));
        em.getTransaction().commit();
        em.close();
    }
}
