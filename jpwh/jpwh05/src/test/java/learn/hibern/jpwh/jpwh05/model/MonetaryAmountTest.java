package learn.hibern.jpwh.jpwh05.model;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.*;

public class MonetaryAmountTest {

    @Test
    public void testToString() {
        MonetaryAmount ma = new MonetaryAmount(new BigDecimal(1000), Currency.getInstance("USD"));
        assertEquals(ma.toString(), "1000 USD");
    }

    @Test
    public void testFrom() {
        MonetaryAmount ma = MonetaryAmount.from("2000 EUR");
        assertEquals(ma.getCurrency().getCurrencyCode(), "EUR");
        assertEquals(ma.getValue(), BigDecimal.valueOf(2000));
    }
}
