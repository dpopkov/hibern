package learn.hibern.jpwh.jpwh05.model.converters;

import learn.hibern.jpwh.jpwh05.model.MonetaryAmount;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/*
    Later, when your DBA upgrades the database schema and offers you separate columns
    for the monetary amount value and currency, you only have to change your
    application in a few places.
    Drop the MonetaryAmountConverter from your project and make MonetaryAmount an @Embeddable ;
    it then maps automatically to two database columns.
 */
@Converter(autoApply = true)
public class MonetaryAmountConverter implements AttributeConverter<MonetaryAmount, String> {
    @Override
    public String convertToDatabaseColumn(MonetaryAmount monetaryAmount) {
        return monetaryAmount.toString();
    }

    @Override
    public MonetaryAmount convertToEntityAttribute(String dbData) {
        return MonetaryAmount.from(dbData);
    }
}
