package learn.hibern.jpwh.jpwh05.model;

public enum AuctionType {
    HIGHEST_BID,
    LOWEST_BID,
    FIXED_PRICE
}
