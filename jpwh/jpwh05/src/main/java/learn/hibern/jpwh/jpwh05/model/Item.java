package learn.hibern.jpwh.jpwh05.model;

import learn.hibern.jpwh.jpwh05.model.converters.MonetaryAmountConverter;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @NotNull
    @Column(nullable = false)
    @Size(
            min = 2,
            max = 255,
            message = "Name is required, maximum 255 characters"
    )
    protected String name;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false, name = "CREATED_ON")
    @org.hibernate.annotations.CreationTimestamp
    protected Date createdOn;
    @Future
    protected Date auctionEnd;
    @NotNull
    @Column(nullable = false, name = "ACTION_TYPE")
    @Enumerated(EnumType.STRING)
    protected AuctionType auctionType = AuctionType.HIGHEST_BID;
    @OneToMany(mappedBy = "item")
    protected final Set<Bid> bids = new HashSet<>();
    @NotNull
    @Convert(
            converter = MonetaryAmountConverter.class,
            disableConversion = false)
    @Column(name = "PRICE", length = 63)
    protected MonetaryAmount buyNowPrice;

    protected Item() {
    }

    public Item(@NotNull @Size(
            min = 2,
            max = 255,
            message = "Name is required, maximum 255 characters"
    ) String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public Date getAuctionEnd() {
        return auctionEnd;
    }

    public void setAuctionEnd(Date auctionEnd) {
        this.auctionEnd = auctionEnd;
    }

    public AuctionType getAuctionType() {
        return auctionType;
    }

    public void setAuctionType(AuctionType auctionType) {
        this.auctionType = auctionType;
    }

    public Set<Bid> getBids() {
        return Collections.unmodifiableSet(bids);
    }

    public void addBid(Bid bid) {
        if (bid == null) {
            throw new NullPointerException("Cannot add null Bid");
        }
        if (bid.getItem() != null) {
            throw new IllegalStateException("Bid is already assigned to an Item");
        }
        bids.add(bid);
        bid.setItem(this);
    }

    public MonetaryAmount getBuyNowPrice() {
        return buyNowPrice;
    }

    public void setBuyNowPrice(MonetaryAmount buyNowPrice) {
        this.buyNowPrice = buyNowPrice;
    }
}
