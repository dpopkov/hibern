package learn.hibern.jpwh.jpwh05.model;

public class SwissZipcode extends ZipCode {
    public static final int LENGTH = 4;

    public SwissZipcode(String value) {
        super(value);
    }
}
