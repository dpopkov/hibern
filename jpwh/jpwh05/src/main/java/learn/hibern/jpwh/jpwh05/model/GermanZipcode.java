package learn.hibern.jpwh.jpwh05.model;

public class GermanZipcode extends ZipCode {
    public static final int LENGTH = 5;

    public GermanZipcode(String value) {
        super(value);
    }
}
