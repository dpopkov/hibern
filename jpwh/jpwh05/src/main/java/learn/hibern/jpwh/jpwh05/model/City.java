package learn.hibern.jpwh.jpwh05.model;

import learn.hibern.jpwh.jpwh05.model.converters.ZipcodeConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class City {
    @NotNull
    @Column(nullable = false, length = 5)
    @Convert(
            converter = ZipcodeConverter.class
    )
    protected ZipCode zipcode;
    @NotNull
    @Column(nullable = false)
    protected String name;
    @NotNull
    @Column(nullable = false)
    protected String country;

    public City() {
    }

    public City(@NotNull ZipCode zipcode, @NotNull String name, @NotNull String country) {
        this.zipcode = zipcode;
        this.name = name;
        this.country = country;
    }

    public ZipCode getZipcode() {
        return zipcode;
    }

    public void setZipcode(ZipCode zipcode) {
        this.zipcode = zipcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
