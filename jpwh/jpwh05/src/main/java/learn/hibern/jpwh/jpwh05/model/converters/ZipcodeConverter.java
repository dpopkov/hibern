package learn.hibern.jpwh.jpwh05.model.converters;

import learn.hibern.jpwh.jpwh05.model.GermanZipcode;
import learn.hibern.jpwh.jpwh05.model.SwissZipcode;
import learn.hibern.jpwh.jpwh05.model.ZipCode;

import javax.persistence.AttributeConverter;

/*
    German zip codes are five numbers long, Swiss are four.
 */
public class ZipcodeConverter implements AttributeConverter<ZipCode, String> {
    @Override
    public String convertToDatabaseColumn(ZipCode attribute) {
        return attribute.getValue();
    }

    @Override
    public ZipCode convertToEntityAttribute(String dbData) {
        if (dbData.length() == GermanZipcode.LENGTH) {
            return new GermanZipcode(dbData);
        } else if (dbData.length() == SwissZipcode.LENGTH) {
            return new SwissZipcode(dbData);
        }
        throw new IllegalArgumentException("Unsupported zipcode in database: " + dbData);
    }
}
