package learn.hibern.jpwh.jpwh05.model.temporal;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "TEMPORAL_ITEM")
public class TemporalItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    protected String name;

    @Column(name = "CREATED_ON")
    @org.hibernate.annotations.CreationTimestamp
    protected LocalDateTime createdOn;

    @Column(name = "MODIFIED_ON")
    @org.hibernate.annotations.UpdateTimestamp
    protected LocalDateTime modifiedOn;

    public TemporalItem() {
    }

    public TemporalItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public LocalDateTime getModifiedOn() {
        return modifiedOn;
    }
}
