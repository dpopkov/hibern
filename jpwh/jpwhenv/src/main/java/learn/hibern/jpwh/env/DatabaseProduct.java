package learn.hibern.jpwh.env;

import bitronix.tm.resource.jdbc.PoolingDataSource;
import learn.hibern.jpwh.jpwhshared.ImprovedH2Dialect;

@SuppressWarnings("Convert2Lambda")
public enum DatabaseProduct {
    H2(
            new DataSourceConfiguration() {
                @Override
                public void configure(PoolingDataSource ds, String connectionURL) {
                    ds.setClassName("org.h2.jdbcx.JdbcDataSource");

                    // External instance: jdbc:h2:tcp://localhost/mem:test;USER=sa
                    ds.getDriverProperties().put(
                            "URL",
                            connectionURL != null
                                    ? connectionURL :
                                    "jdbc:h2:mem:test"
                    );

                    // TODO: http://code.google.com/p/h2database/issues/detail?id=502
                    ds.getDriverProperties().put("user", "sa");

                    // TODO: Don't trace log values larger than X bytes (especially useful for
                    // debugging LOBs, which are accessed in toString()!)
                    // System.setProperty("h2.maxTraceDataLength", "256"); 256 bytes, default is 64 kilobytes
                }
            },
            ImprovedH2Dialect.class.getName()
    );

    public final DataSourceConfiguration configuration;
    public final String hibernateDialect;

    DatabaseProduct(DataSourceConfiguration configuration, String hibernateDialect) {
        this.configuration = configuration;
        this.hibernateDialect = hibernateDialect;
    }

    public interface DataSourceConfiguration {
        void configure(PoolingDataSource ds, String connectionURL);
    }
}
