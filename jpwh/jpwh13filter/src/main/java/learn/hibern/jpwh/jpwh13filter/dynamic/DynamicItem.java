package learn.hibern.jpwh.jpwh13filter.dynamic;

import javax.persistence.*;

import static learn.hibern.jpwh.jpwh13filter.dynamic.Filtering.*;

@Entity
@Table(name = "DYNAMIC_ITEM")
/* Defining Dynamic Filter */
@org.hibernate.annotations.FilterDef(
        name = LIMIT_BY_USER_RANK,
        parameters = {
                @org.hibernate.annotations.ParamDef(name = CURRENT_USER_RANK_PARAM, type = "int")
        }
)
/* Applying the Filter */
@org.hibernate.annotations.Filter(
        name = LIMIT_BY_USER_RANK,
        condition =
                ":" + CURRENT_USER_RANK_PARAM + " >= "
                        + "("
                        + "select u.rank from DYNAMIC_USER u "
                        + "where u.id = seller_id"
                        + ")"
)
public class DynamicItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @ManyToOne(fetch = FetchType.LAZY)
    protected DynamicUser seller;

    protected DynamicItem() {
    }

    public DynamicItem(String name, DynamicUser seller) {
        this.name = name;
        this.seller = seller;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DynamicUser getSeller() {
        return seller;
    }

    @Override
    public String toString() {
        return "DynamicItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", seller.id=" + seller.getId() +
                '}';
    }
}
