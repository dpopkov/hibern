package learn.hibern.jpwh.jpwh13filter.dynamic;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "DYNAMIC_USER")
public class DynamicUser {
    @Id
    @GeneratedValue
    protected Long id;
    @NotNull
    protected String name;
    @NotNull
    protected int rank = 0;

    protected DynamicUser() {
    }

    public DynamicUser(String name, int rank) {
        this.name = name;
        this.rank = rank;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRank() {
        return rank;
    }

    @Override
    public String toString() {
        return "DynamicUser{" +
                "id=" + id +
                ", name='" + name + '\'' +
                "rank=" + rank +
                '}';
    }
}
