package learn.hibern.jpwh.jpwh13filter.cascade.refresh;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "REFRESH_CREDIT_CARD")
public class RefreshCreditCard extends RefreshBillingDetails {
    @NotNull
    protected String cardNumber;

    @NotNull
    protected String expMonth;

    @NotNull
    protected String expYear;

    protected RefreshCreditCard() {
    }

    public RefreshCreditCard(@NotNull String owner, @NotNull String cardNumber,
                             @NotNull String expMonth, @NotNull String expYear) {
        super(owner);
        this.cardNumber = cardNumber;
        this.expMonth = expMonth;
        this.expYear = expYear;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public String getExpYear() {
        return expYear;
    }
}
