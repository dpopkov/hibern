package learn.hibern.jpwh.jpwh13filter.cascade.merge;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MERGING_ITEM")
public class MergingItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @OneToMany(
            mappedBy = "item",
            cascade = {CascadeType.DETACH, CascadeType.MERGE})
    protected Set<MergingBid> bids = new HashSet<>();

    protected MergingItem() {
    }

    public MergingItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public Set<MergingBid> getBids() {
        return bids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
