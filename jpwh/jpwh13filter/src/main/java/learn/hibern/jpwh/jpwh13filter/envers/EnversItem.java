package learn.hibern.jpwh.jpwh13filter.envers;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@org.hibernate.envers.Audited
@Entity
@Table(name = "ENVERS_ITEM")
public class EnversItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @org.hibernate.envers.NotAudited
    @OneToMany(mappedBy = "item")
    protected Set<EnversBid> bids = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SELLER_ID", nullable = false)
    protected EnversUser seller;

    protected EnversItem() {
    }

    public EnversItem(String name, EnversUser seller) {
        this.name = name;
        this.seller = seller;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EnversUser getSeller() {
        return seller;
    }

    public Set<EnversBid> getBids() {
        return bids;
    }

    @Override
    public String toString() {
        return "EnversItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", seller.id=" + seller.getId() +
                '}';
    }
}
