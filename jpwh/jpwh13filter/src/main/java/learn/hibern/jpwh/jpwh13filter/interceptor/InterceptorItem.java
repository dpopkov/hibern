package learn.hibern.jpwh.jpwh13filter.interceptor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "INTERCEPTOR_ITEM")
public class InterceptorItem implements Auditable {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    protected InterceptorItem() {
    }

    public InterceptorItem(String name) {
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "InterceptorItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
