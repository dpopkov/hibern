package learn.hibern.jpwh.jpwh13filter.callback;

import java.util.ArrayList;

/**
 * Singleton.
 */
public class Mail extends ArrayList<String> {

    public static final Mail INSTANCE = new Mail();

    private Mail() {
    }

    public void send(String message) {
        add(message);
    }
}
