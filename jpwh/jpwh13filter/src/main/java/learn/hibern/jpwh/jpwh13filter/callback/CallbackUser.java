package learn.hibern.jpwh.jpwh13filter.callback;

import javax.persistence.*;

@Entity
@Table(name = "CALLBACK_USER")
public class CallbackUser {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    protected CallbackUser() {
    }

    public CallbackUser(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "CallbackUser{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @PostPersist
    public void notifyAdmin() {
        CallbackUser user = CurrentCallbackUser.INSTANCE.get();
        Mail.INSTANCE.send("Entity instance persisted by "
                + user.getName()
                + ": "
                + this);
    }
}
