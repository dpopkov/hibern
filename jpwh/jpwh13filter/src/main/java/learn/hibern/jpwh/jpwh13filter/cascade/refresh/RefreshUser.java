package learn.hibern.jpwh.jpwh13filter.cascade.refresh;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "REFRESH_USER")
public class RefreshUser {
    @Id
    @GeneratedValue
    protected Long id;
    @NotNull
    protected String username;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "USER_ID", nullable = false)
    protected Set<RefreshBillingDetails> billingDetails = new HashSet<>();

    protected RefreshUser() {
    }

    public RefreshUser(@NotNull String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<RefreshBillingDetails> getBillingDetails() {
        return billingDetails;
    }
}
