package learn.hibern.jpwh.jpwh13filter.cascade.refresh;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "REFRESH_BANK_ACCOUNT")
public class RefreshBankAccount extends RefreshBillingDetails {
    @NotNull
    protected String account;

    @NotNull
    protected String bankname;

    @NotNull
    protected String swift;

    protected RefreshBankAccount() {
    }

    public RefreshBankAccount(@NotNull String owner, @NotNull String account,
                              @NotNull String bankname, @NotNull String swift) {
        super(owner);
        this.account = account;
        this.bankname = bankname;
        this.swift = swift;
    }

    public String getAccount() {
        return account;
    }

    public String getBankname() {
        return bankname;
    }

    public String getSwift() {
        return swift;
    }
}
