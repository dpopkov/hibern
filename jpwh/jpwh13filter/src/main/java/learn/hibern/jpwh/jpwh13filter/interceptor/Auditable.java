package learn.hibern.jpwh.jpwh13filter.interceptor;

public interface Auditable {
    Long getId();
}
