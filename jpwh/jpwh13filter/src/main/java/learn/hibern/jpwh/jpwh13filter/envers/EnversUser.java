package learn.hibern.jpwh.jpwh13filter.envers;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Audited
@Entity
@Table(name = "ENVERS_USER")
public class EnversUser {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    protected EnversUser() {
    }

    public EnversUser(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "EnversUser{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
