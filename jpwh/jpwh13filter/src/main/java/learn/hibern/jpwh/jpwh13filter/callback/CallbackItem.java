package learn.hibern.jpwh.jpwh13filter.callback;

import javax.persistence.*;

@Entity
@Table(name = "CALLBACK_ITEM")
@EntityListeners(
        PersistEntityListener.class
)
public class CallbackItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    protected CallbackItem() {
    }

    public CallbackItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "CallbackItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
