package learn.hibern.jpwh.jpwh13filter.callback;

/**
 * Thread-local
 */
public class CurrentCallbackUser extends ThreadLocal<CallbackUser> {
    public static CurrentCallbackUser INSTANCE = new CurrentCallbackUser();

    private CurrentCallbackUser() {
    }
}
