package learn.hibern.jpwh.jpwh13filter.cascade.merge;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "MERGING_BID")
public class MergingBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    protected BigDecimal amount;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    protected MergingItem item;

    protected MergingBid() {
    }

    public MergingBid(BigDecimal amount, MergingItem item) {
        this.amount = amount;
        this.item = item;
        item.getBids().add(this);
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public MergingItem getItem() {
        return item;
    }

    @Override
    public String toString() {
        return "MergingBid{" +
                "id=" + id +
                ", amount=" + amount +
                ", itemId=" + item.getId() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MergingBid)) return false;

        MergingBid that = (MergingBid) o;

        if (getAmount() != null ? !getAmount().equals(that.getAmount()) : that.getAmount() != null) return false;
        return getItem() != null ? getItem().getId().equals(that.getItem().getId()) : that.getItem() == null;
    }

    @Override
    public int hashCode() {
        int result = getAmount() != null ? getAmount().hashCode() : 0;
        result = 31 * result + ((getItem() != null && getItem().getId() != null) ? getItem().getId().hashCode() : 0);
        return result;
    }
}
