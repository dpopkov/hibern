package learn.hibern.jpwh.jpwh13filter.interceptor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "INTERCEPTOR_USER")
public class InterceptorUser {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    protected InterceptorUser() {
    }

    public InterceptorUser(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "InterceptorUser{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
