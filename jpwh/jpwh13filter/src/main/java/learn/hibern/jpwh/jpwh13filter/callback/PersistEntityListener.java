package learn.hibern.jpwh.jpwh13filter.callback;

import javax.persistence.PostPersist;

public class PersistEntityListener {

    @PostPersist
    public void notifyAdmin(Object entityInstance) {
        CallbackUser currentUser = CurrentCallbackUser.INSTANCE.get();
        Mail.INSTANCE.send("Entity instance persisted by "
                + currentUser.getName()
                + ": "
                + entityInstance);
    }
}
