package learn.hibern.jpwh.jpwh13filter.interceptor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "INTERCEPTOR_AUDIT_LOG_RECORD")
public class InterceptorAuditLogRecord {
    @Id
    @GeneratedValue
    protected Long id;
    @NotNull
    protected String message;
    @NotNull
    protected Long entityId;
    @NotNull
    protected Class<?> entityClass;
    @NotNull
    protected Long userId;
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdOn = new Date();

    protected InterceptorAuditLogRecord() {
    }

    public InterceptorAuditLogRecord(@NotNull String message, @NotNull Auditable entityInstance, @NotNull Long userId) {
        this.message = message;
        this.entityId = entityInstance.getId();
        this.entityClass = entityInstance.getClass();
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public Long getEntityId() {
        return entityId;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }

    public Long getUserId() {
        return userId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }
}
