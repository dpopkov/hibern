package learn.hibern.jpwh.jpwh13filter;

public class Constants {
    public static final String PERSISTENCE_UNIT_NAME = "ch13filter";
    public static final String PERSISTENCE_CALLBACK_UNIT_NAME = "ch13callback";
    public static final String PERSISTENCE_INTERCEPT_UNIT_NAME = "ch13intercept";
    public static final String PERSISTENCE_ENVERS_UNIT_NAME = "ch13envers";
    public static final String PERSISTENCE_DYNAMIC_UNIT_NAME = "ch13dynamic";
}
