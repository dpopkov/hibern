package learn.hibern.jpwh.jpwh13filter.interceptor;

import learn.hibern.jpwh.jpwh13filter.AbstractJpaTest;
import learn.hibern.jpwh.jpwh13filter.Constants;
import org.hibernate.Interceptor;
import org.hibernate.Session;
import org.hibernate.engine.spi.SessionImplementor;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

public class AuditLogInterceptorTest extends AbstractJpaTest {

    @Test
    public void testWritingAuditLog() {
        final Long currentUserId = persistTheUser();

        /* Prepare Interceptor */
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(Constants.PERSISTENCE_INTERCEPT_UNIT_NAME);
        Map<String, String> properties = new HashMap<>();
        properties.put(org.hibernate.jpa.AvailableSettings.SESSION_INTERCEPTOR, AuditLogInterceptor.class.getName());
//        properties.put(AvailableSettings.SESSION_SCOPED_INTERCEPTOR, AuditLogInterceptor.class.getName());
        EntityManager em = emf.createEntityManager(properties);
        Session session = em.unwrap(Session.class);
        // todo: fix ClassCastException below
        final Interceptor interceptor1 = ((SessionImplementor) session).getInterceptor();
        System.out.println(">>>>>>>>>>>>>>>>>>>> interceptor1.getClass().getName() = " + interceptor1.getClass().getName());
        AuditLogInterceptor interceptor = (AuditLogInterceptor) interceptor1;
        interceptor.setCurrentSession(session);
        interceptor.setCurrentUserId(currentUserId);

        /* Persist one Item */
        em.getTransaction().begin();
        InterceptorItem item = new InterceptorItem("Notebook");
        em.persist(item);
        em.getTransaction().commit();
        em.clear();

        /* Check the audit log */
        em.getTransaction().begin();
        List<InterceptorAuditLogRecord> logs = em.createQuery(
                "select lr from InterceptorAuditLogRecord lr", InterceptorAuditLogRecord.class)
                .getResultList();
        assertEquals(logs.size(), 1);
        var firstLog = logs.get(0);
        assertEquals(firstLog.getMessage(), "insert");
        assertEquals(firstLog.getEntityClass(), InterceptorItem.class);
        assertEquals(firstLog.getEntityId(), item.getId());
        assertEquals(firstLog.getUserId(), currentUserId);
        em.getTransaction().commit();

        // This test is not finished.
        // Can not fix Exception:
        // java.lang.ClassCastException: class org.hibernate.EmptyInterceptor cannot be cast to class learn.hibern.jpwh.jpwh13filter.interceptor.AuditLogInterceptor (org.hibernate.EmptyInterceptor and learn.hibern.jpwh.jpwh13filter.interceptor.AuditLogInterceptor are in unnamed module of loader 'app')

        em.close();
    }

    private Long persistTheUser() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_INTERCEPT_UNIT_NAME);
        InterceptorUser user = new InterceptorUser("Jane Doe");
        entityManager.persist(user);
        commitTransactionAndCloseEntityManager();
        return user.getId();
    }
}
