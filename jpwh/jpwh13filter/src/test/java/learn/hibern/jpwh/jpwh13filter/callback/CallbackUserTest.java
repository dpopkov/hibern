package learn.hibern.jpwh.jpwh13filter.callback;

import learn.hibern.jpwh.jpwh13filter.AbstractJpaTest;
import learn.hibern.jpwh.jpwh13filter.Constants;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CallbackUserTest extends AbstractJpaTest {

    @Test
    public void testPersistCallback() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_CALLBACK_UNIT_NAME);
        CallbackUser user = new CallbackUser("Jane Doe");
        CurrentCallbackUser.INSTANCE.set(user);
        entityManager.persist(user);
        final Mail mail = Mail.INSTANCE;
        assertEquals(mail.size(), 0);
        entityManager.flush();
        assertEquals(mail.size(), 1);
        final String message = mail.get(0);
        System.out.println("message for User = " + message);
        assertTrue(message.contains("Jane Doe"));

        mail.clear();
        CallbackItem item = new CallbackItem("Notebook");
        entityManager.persist(item);
        assertEquals(mail.size(), 0);
        entityManager.flush();
        assertEquals(mail.size(), 1);
        final String message2 = mail.get(0);
        System.out.println("message for Item = " + message2);
        assertTrue(message2.contains("Jane Doe"));

        commitTransactionAndCloseEntityManager();
        CurrentCallbackUser.INSTANCE.set(null);
    }
}
