package learn.hibern.jpwh.jpwh13filter.cascade.merge;

import learn.hibern.jpwh.jpwh13filter.AbstractJpaTest;
import org.testng.annotations.Test;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class MergingItemTest extends AbstractJpaTest {

    @Test
    public void testDetachAndMerge() {
        final Long itemId = persistTestData();

        createEntityManagerAndBeginTransaction();
        MergingItem item = entityManager.find(MergingItem.class, itemId);
        assertEquals(item.getBids().size(), 2);

        entityManager.detach(item);
        item.setName(item.getName() + "-changed");
        new MergingBid(TEN.add(TEN), item);

        MergingItem mergedItem = entityManager.merge(item);
        for (MergingBid b : mergedItem.getBids()) {
            assertNotNull(b.getId());
        }
        commitTransactionAndCloseEntityManager();

        createEntityManagerAndBeginTransaction();
        MergingItem foundItem = entityManager.find(MergingItem.class, itemId);
        assertEquals(foundItem.getBids().size(), 3);
        commitTransactionAndCloseEntityManager();
    }

    private Long persistTestData() {
        MergingItem itemToSave = new MergingItem("item-1");
        MergingBid bid1 = new MergingBid(ONE, itemToSave);
        MergingBid bid2 = new MergingBid(TEN, itemToSave);
        createEntityManagerAndBeginTransaction();
        entityManager.persist(itemToSave);
        final Long itemId = itemToSave.getId();
        entityManager.persist(bid1);
        entityManager.persist(bid2);
        commitTransactionAndCloseEntityManager();
        return itemId;
    }
}
