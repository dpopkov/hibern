package learn.hibern.jpwh.jpwh13filter.envers;

import learn.hibern.jpwh.jpwh13filter.AbstractJpaTest;
import learn.hibern.jpwh.jpwh13filter.Constants;
import org.hibernate.ReplicationMode;
import org.hibernate.Session;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.hibernate.envers.query.criteria.MatchMode;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;

import static org.testng.Assert.*;

public class EnversUserTest extends AbstractJpaTest {

    private static final String PERSISTENCE_UNIT = Constants.PERSISTENCE_ENVERS_UNIT_NAME;

    private Long userId;
    private Long itemId;
    private Date timestampCreate;
    private Date timestampUpdate;
    private Date timestampDelete;
    private AuditReader auditReader;
    private Number revisionNumberCreate;
    private Number revisionNumberUpdate;
    private Number revisionNumberDelete;

    @Test
    public void testAuditLogging() {
        createAnAuditTrail();

        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);

        findRevisions();
        queryRevisions();
        accessHistoricalData();
        queryingHistoricalEntityInstances();
        rollbackEntityToOlderVersion();

        commitTransactionAndCloseEntityManager();
    }

    private void createAnAuditTrail() {
        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);
        EnversUser user = new EnversUser("Jane Doe");
        entityManager.persist(user);
        userId = user.getId();
        EnversItem itemToSave = new EnversItem("Notebook", user);
        entityManager.persist(itemToSave);
        itemId = itemToSave.getId();
        commitTransactionAndCloseEntityManager();
        timestampCreate = new Date();

        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);
        EnversItem foundItem = entityManager.find(EnversItem.class, itemId);
        foundItem.setName("Server");
        foundItem.getSeller().setName("Doe Jane");
        commitTransactionAndCloseEntityManager();
        timestampUpdate = new Date();

        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);
        foundItem = entityManager.find(EnversItem.class, itemId);
        entityManager.remove(foundItem);
        commitTransactionAndCloseEntityManager();
        timestampDelete = new Date();
    }

    private void findRevisions() {
        auditReader = AuditReaderFactory.get(entityManager);
        /*
           Given a timestamp, you can find the revision number of a change set,
           made before or on that timestamp.
         */
        revisionNumberCreate = auditReader.getRevisionNumberForDate(timestampCreate);
        revisionNumberUpdate = auditReader.getRevisionNumberForDate(timestampUpdate);
        revisionNumberDelete = auditReader.getRevisionNumberForDate(timestampDelete);
        System.out.println("timestampCreate = " + timestampCreate.getTime());
        System.out.println("timestampUpdate = " + timestampUpdate.getTime());
        System.out.println("timestampDelete = " + timestampDelete.getTime());
        System.out.println("revisionNumberCreate = " + revisionNumberCreate);
        System.out.println("revisionNumberUpdate = " + revisionNumberUpdate);
        System.out.println("revisionNumberDelete = " + revisionNumberDelete);

        /*
           If you don't have a timestamp, you can get all revision numbers in which a
           particular audited entity instance was involved. This operation finds all
           change sets where the given Item< was created, modified, or deleted.
         */
        List<Number> itemRevisions = auditReader.getRevisions(EnversItem.class, itemId);
        assertEquals(itemRevisions.size(), 3);
        for (Number itemRevision : itemRevisions) {
            Date itemRevisionTimestamp = auditReader.getRevisionDate(itemRevision);
            System.out.printf("itemRevision = %d, itemRevisionTimestamp = %d%n",
                    itemRevision.longValue(), itemRevisionTimestamp.getTime());
        }

        List<Number> userRevisions = auditReader.getRevisions(EnversUser.class, userId);
        assertEquals(userRevisions.size(), 2);
        entityManager.clear();
    }

    private void queryRevisions() {
    /*
       If you don't know modification timestamps or revision numbers, you can write
       a query to obtain all audit trail details of a particular entity.
     */
        AuditQuery query = auditReader.createQuery().forRevisionsOfEntity(
                EnversItem.class, false, false);
        @SuppressWarnings("unchecked")
        List<Object[]> result = query.getResultList();
        for (Object[] tuple : result) {
            /*
                Each result tuple contains the entity instance for a particular revision,
                the revision details (including revision number and timestamp),
                as well as the revision type.
             */
            EnversItem item = (EnversItem) tuple[0];
            DefaultRevisionEntity revision = (DefaultRevisionEntity) tuple[1];
            RevisionType revisionType = (RevisionType) tuple[2];

            if (revision.getId() == 1) {
                assertEquals(revisionType, RevisionType.ADD);
                assertEquals(item.getName(), "Notebook");
            } else if (revision.getId() == 2) {
                assertEquals(revisionType, RevisionType.MOD);
                assertEquals(item.getName(), "Server");
            } else if (revision.getId() == 3) {
                assertEquals(revisionType, RevisionType.DEL);
                assertNull(item);
            }
        }
        entityManager.clear();
    }

    private void accessHistoricalData() {
        EnversItem createdItem = auditReader.find(EnversItem.class, itemId, revisionNumberCreate);
        assertEquals(createdItem.getName(), "Notebook");
        assertEquals(createdItem.getSeller().getName(), "Jane Doe");

        EnversItem modifiedItem = auditReader.find(EnversItem.class, itemId, revisionNumberUpdate);
        assertEquals(modifiedItem.getName(), "Server");
        assertEquals(modifiedItem.getSeller().getName(), "Doe Jane");

        EnversItem deletedItem = auditReader.find(EnversItem.class, itemId, revisionNumberDelete);
        assertNull(deletedItem);

        EnversUser sellerOfDeletedItem = auditReader.find(EnversUser.class, userId, revisionNumberDelete);
        assertEquals(sellerOfDeletedItem.getName(), "Doe Jane");

        entityManager.clear();
    }

    private void queryingHistoricalEntityInstances() {
        // This query returns instances restricted to a particular revision and change set
        AuditQuery query = auditReader.createQuery().forEntitiesAtRevision(EnversItem.class, revisionNumberUpdate);
        // Adding further restrictions to the query
        query.add(AuditEntity.property("name").like("Ser", MatchMode.START));
        // Restrictions can include entity associations
        query.add(AuditEntity.relatedId("seller").eq(userId));
        // Ordering query results
        query.addOrder(AuditEntity.property("name").desc());
        // Paginate
        query.setFirstResult(0);
        query.setMaxResults(10);

        assertEquals(query.getResultList().size(), 1);
        EnversItem result = (EnversItem) query.getResultList().get(0);
        assertEquals(result.getSeller().getName(), "Doe Jane");

        // Projection
        query = auditReader.createQuery().forEntitiesAtRevision(EnversItem.class, revisionNumberUpdate);
        query.addProjection(AuditEntity.property("name"));
        assertEquals(query.getResultList().size(), 1);
        String name = (String) query.getSingleResult();
        assertEquals(name, "Server");
    }

    private void rollbackEntityToOlderVersion() {
        EnversUser user = auditReader.find(EnversUser.class, userId, revisionNumberCreate);

        entityManager.unwrap(Session.class).replicate(user, ReplicationMode.OVERWRITE);
        entityManager.flush();
        entityManager.clear();

        user = entityManager.find(EnversUser.class, userId);
        assertEquals(user.getName(), "Jane Doe");
    }
}
