package learn.hibern.jpwh.jpwh13filter.dynamic;

import learn.hibern.jpwh.jpwh13filter.AbstractJpaTest;
import learn.hibern.jpwh.jpwh13filter.Constants;
import org.hibernate.Session;
import org.testng.annotations.Test;

import javax.persistence.TypedQuery;
import java.util.List;

import static learn.hibern.jpwh.jpwh13filter.dynamic.Filtering.*;
import static org.testng.Assert.*;

public class DynamicUserTest extends AbstractJpaTest {

    private static final String PERSISTENCE_UNIT = Constants.PERSISTENCE_DYNAMIC_UNIT_NAME;

    @Test
    public void testPersist() {
        /* Persist test data */
        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);
        DynamicUser user1 = new DynamicUser("Jane Doe", 1);
        DynamicUser user2 = new DynamicUser("Jack Sparrow", 2);
        entityManager.persist(user1);
        entityManager.persist(user2);
        entityManager.persist(new DynamicItem("Notebook", user1));
        entityManager.persist(new DynamicItem("Server", user2));
        entityManager.persist(new DynamicItem("Server2", user2));
        commitTransactionAndCloseEntityManager();

        createEntityManagerAndBeginTransaction(PERSISTENCE_UNIT);
        TypedQuery<DynamicItem> itemQuery = entityManager.createQuery(
                "select i from DynamicItem i", DynamicItem.class);
        /* Enable the Filter */
        org.hibernate.Filter filterByRank = entityManager.unwrap(Session.class).enableFilter(LIMIT_BY_USER_RANK);

        filterByRank.setParameter(CURRENT_USER_RANK_PARAM, 1);
        List<DynamicItem> oneItem = itemQuery.getResultList();
        assertEquals(oneItem.size(), 1);

        filterByRank.setParameter(CURRENT_USER_RANK_PARAM, 2);
        List<DynamicItem> threeItems = itemQuery.getResultList();
        assertEquals(threeItems.size(), 3);

        commitTransactionAndCloseEntityManager();
    }
}
