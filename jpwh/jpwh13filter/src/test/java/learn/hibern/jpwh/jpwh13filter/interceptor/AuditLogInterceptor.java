package learn.hibern.jpwh.jpwh13filter.interceptor;

import org.hibernate.EmptyInterceptor;
import org.hibernate.Session;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.*;

public class AuditLogInterceptor extends EmptyInterceptor {

    protected Session currentSession;
    protected Long currentUserId;
    protected List<Auditable> inserts = new ArrayList<>();
    protected List<Auditable> updates = new ArrayList<>();

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public void setCurrentUserId(Long userId) {
        this.currentUserId = userId;
    }

    /**
     * This method is called when an entity instance is made persistent.
     */
    @Override
    public boolean onSave(Object entity, Serializable id,
                          Object[] state, String[] propertyNames,
                          Type[] types) {
        if (entity instanceof Auditable) {
            inserts.add((Auditable) entity);
        }
        return false;   // the current state was not modified
    }

    /**
     * This method is called when an entity instance is detected as dirty during flushing of
     * the persistence context.
     */
    @Override
    public boolean onFlushDirty(Object entity, Serializable id,
                                Object[] currentState,
                                Object[] previousState,
                                String[] propertyNames,
                                Type[] types) {
        if (entity instanceof Auditable) {
            updates.add((Auditable) entity);
        }
        return false;   // the current state was not modified
    }

    /**
     * This method is called after flushing of the persistence context is complete.
     * It writes the audit log records for all insertions and updates collected earlier.
     */
    @Override
    public void postFlush(Iterator iterator) {
        /*
            You can’t access the original persistence context: the Session that is currently
            executing this interceptor. The Session is in a fragile state during interceptor calls.
            Hibernate lets you create a new Session that inherits some information from the original
            Session with the sessionWithOptions() method. The new temporary Session works
            with the same transaction and database connection as the original Session.
         */
        try (Session tempSession = currentSession.sessionWithOptions()
                .connection()
                .openSession()) {
            for (Auditable entity : inserts) {
                tempSession.persist(new InterceptorAuditLogRecord("insert", entity, currentUserId));
            }
            for (Auditable entity : updates) {
                tempSession.persist(new InterceptorAuditLogRecord("update", entity, currentUserId));
            }
            /* You flush and close the temporary Session independently from the original Session. */
            tempSession.flush();
        } finally {
            inserts.clear();
            updates.clear();
        }
    }
}
