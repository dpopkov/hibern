package learn.hibern.jpwh.jpwh13filter.cascade.refresh;

import learn.hibern.jpwh.jpwh13filter.AbstractJpaTest;
import learn.hibern.jpwh.jpwh13filter.Constants;
import learn.hibern.jpwh.jpwhutil.JpaEntityManagerUtil;
import org.hibernate.Session;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.sql.PreparedStatement;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import static org.testng.Assert.*;

public class RefreshUserTest extends AbstractJpaTest {

    private static final String OWNER_NAME = "Jane Doe";
    private static final String CHANGED_OWNER_NAME = "Jack Sparrow";

    @Test
    public void testRefresh() {
        final Long userId = persistTestData();

        createEntityManagerAndBeginTransaction();
        RefreshUser user = entityManager.find(RefreshUser.class, userId);
        assertEquals(user.getBillingDetails().size(), 2);
        for (var bd : user.getBillingDetails()) {
            assertEquals(bd.getOwner(), OWNER_NAME);
        }

        modifyBillingInformationInDatabase(userId);

        /*
            When you refresh() the managed User instance, Hibernate cascades the operation to
            the managed BillingDetails and refreshes each with an SQL SELECT.
            Then, Hibernate refreshes the User instance and eagerly loads the entire billingDetails
            collection to discover any new BillingDetails.
         */
        entityManager.refresh(user);
        for (var bd : user.getBillingDetails()) {
            assertEquals(bd.getOwner(), CHANGED_OWNER_NAME);
        }
        commitTransactionAndCloseEntityManager();
    }

    private void modifyBillingInformationInDatabase(final Long userId) {
        try {
            Executors.newSingleThreadExecutor().submit(() -> {
                EntityManager em = JpaEntityManagerUtil.getEntityManager(Constants.PERSISTENCE_UNIT_NAME);
                em.getTransaction().begin();
                em.unwrap(Session.class).doWork(connection -> {
                    PreparedStatement ps = connection.prepareStatement(
                            "update REFRESH_BILLING_DETAILS set owner = ? where USER_ID = ?");
                    ps.setString(1, CHANGED_OWNER_NAME);
                    ps.setLong(2, userId);
                    ps.executeUpdate();
                });
                em.getTransaction().commit();
                em.close();
                return null;
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException("Failed update", e);
        }
    }

    private Long persistTestData() {
        createEntityManagerAndBeginTransaction();
        RefreshUser user = new RefreshUser(OWNER_NAME);
        user.getBillingDetails().add(new RefreshBankAccount(
                OWNER_NAME, "account", "bankname", "swift"));
        user.getBillingDetails().add(new RefreshCreditCard(
                OWNER_NAME, "cardNumber", "expMonth", "expYear"));
        entityManager.persist(user);
        final Long userId = user.getId();
        commitTransactionAndCloseEntityManager();
        return userId;
    }
}
