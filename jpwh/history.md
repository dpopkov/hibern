History
-------

### Configuring a persistence unit
PATH: `/jpwhmodel/src/main/resources/META-INF/persistence.xml`

### Logging SQL
```xml
<properties>
    <property name="hibernate.format_sql" value="true"/>
    <property name="hibernate.use_sql_comments" value="true"/>
</properties>
```
Set the categories `org.hibernate.SQL` and `org.hibernate.type.descriptor.sql.BasicBinder` to 
the finest debug level.

### Bean Validation
Dependencies:
* javax.validation:validation-api
* org.hibernate.validator:hibernate-validator

Annotations:
* @NotNull
* @Size(min,max,message)
* @Future

The Hibernate toolset for automatic SQL schema generation understands many constraints 
and generates SQL DDL -equivalent constraints. 
For example, an @NotNull annotation translates into an SQL NOT NULL constraint, and an
@Size(n) rule defines the number of characters in a VARCHAR(n) -typed column.

### Mapping
* Add Entities (package 'simple')
* Mapping entities with identity
* Configuring key generators and generator strategies
* Mapping embeddable components
