package learn.hibern.jpwh.jpwh15languages.restriction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "RESTRICTION_ITEM")
public class RestrictionItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;
    protected LocalDateTime auctionEnd;
    @Enumerated(value = EnumType.STRING)
    protected AuctionType auctionType = AuctionType.HIGHEST_BID;
    protected BigDecimal buyNowPrice;

    public RestrictionItem() {
    }

    public RestrictionItem(String name) {
        this.name = name;
    }

    public RestrictionItem(String name, BigDecimal buyNowPrice) {
        this.name = name;
        this.buyNowPrice = buyNowPrice;
    }

    public RestrictionItem(String name, LocalDateTime auctionEnd) {
        this.name = name;
        this.auctionEnd = auctionEnd;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getAuctionEnd() {
        return auctionEnd;
    }

    public AuctionType getAuctionType() {
        return auctionType;
    }

    public void setAuctionType(AuctionType auctionType) {
        this.auctionType = auctionType;
    }
}
