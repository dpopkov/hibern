package learn.hibern.jpwh.jpwh15languages.selection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SELECTION_ITEM")
public class SelectionItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;

    public SelectionItem() {
    }

    public SelectionItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }
}
