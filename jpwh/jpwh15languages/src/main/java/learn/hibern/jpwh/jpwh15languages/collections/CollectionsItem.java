package learn.hibern.jpwh.jpwh15languages.collections;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "COLLECTIONS_ITEM")
public class CollectionsItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;
    protected LocalDateTime auctionEnd;
    protected BigDecimal buyNowPrice;

    @ManyToMany(mappedBy = "items")
    protected Set<CollectionsCategory> categories = new HashSet<>();

    @ElementCollection
    @CollectionTable(name = "COLLECTIONS_IMAGES")
    protected Map<String, CollectionsImage> images = new HashMap<>();

    public CollectionsItem() {
    }

    public CollectionsItem(String name) {
        this.name = name;
    }

    public CollectionsItem(String name, BigDecimal buyNowPrice) {
        this.name = name;
        this.buyNowPrice = buyNowPrice;
    }

    public CollectionsItem(String name, LocalDateTime auctionEnd) {
        this.name = name;
        this.auctionEnd = auctionEnd;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getAuctionEnd() {
        return auctionEnd;
    }

    public Set<CollectionsCategory> getCategories() {
        return categories;
    }

    public Map<String, CollectionsImage> getImages() {
        return images;
    }

    public void addImage(CollectionsImage image) {
        images.put(image.getTitle(), image);
    }
}
