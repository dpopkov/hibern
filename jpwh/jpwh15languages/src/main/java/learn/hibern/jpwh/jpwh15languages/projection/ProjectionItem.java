package learn.hibern.jpwh.jpwh15languages.projection;

import javax.persistence.*;

@Entity
@Table(name = "PROJECTION_ITEM")
public class ProjectionItem {
    @Id
    @GeneratedValue
    protected Long id;
    protected String name;

    public ProjectionItem() {
    }

    public ProjectionItem(String name) {
        this.name = name;
    }


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ProjectionItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
