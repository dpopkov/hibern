package learn.hibern.jpwh.jpwh15languages.collections;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "COLLECTIONS_CATEGORY")
public class CollectionsCategory {
    @Id
    @GeneratedValue
    protected Long id;
    @NotEmpty
    protected String name;

    @ManyToMany
    protected Set<CollectionsItem> items = new HashSet<>();

    public CollectionsCategory() {
    }

    public CollectionsCategory(@NotEmpty String name) {
        this.name = name;
    }

    public void addItem(CollectionsItem item) {
        items.add(item);
        item.getCategories().add(this);
    }
}
