package learn.hibern.jpwh.jpwh15languages.collections;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * Embeddable component capturing the properties of an image.
 */
@Embeddable
public class CollectionsImage {
    @NotNull
    @Column(nullable = false)
    protected String title;
    protected int width;
    protected int height;

    public CollectionsImage() {
    }

    public CollectionsImage(String title, int width, int height) {
        this.title = title;
        this.width = width;
        this.height = height;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CollectionsImage moeImage = (CollectionsImage) o;

        if (width != moeImage.width) return false;
        if (height != moeImage.height) return false;
        return title.equals(moeImage.title);
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + width;
        result = 31 * result + height;
        return result;
    }
}
