package learn.hibern.jpwh.jpwh15languages.restriction;

public enum AuctionType {
    HIGHEST_BID,
    LOWEST_BID,
    FIXED_PRICE
}
