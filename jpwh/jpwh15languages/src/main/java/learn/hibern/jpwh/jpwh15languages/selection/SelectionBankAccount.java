package learn.hibern.jpwh.jpwh15languages.selection;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "SELECTION_BANK_ACCOUNT")
public class SelectionBankAccount extends SelectionBillingDetails {

    @NotNull
    protected String account;
    @NotNull
    protected String bankname;
    @NotNull
    protected String swift;

    protected SelectionBankAccount() {
    }

    public SelectionBankAccount(@NotNull String owner, @NotNull String account,
                                @NotNull String bankname, @NotNull String swift) {
        super(owner);
        this.account = account;
        this.bankname = bankname;
        this.swift = swift;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }
}
