package learn.hibern.jpwh.jpwh15languages.projection;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "PROJECTION_BID")
public class ProjectionBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    protected ProjectionItem item;

    public ProjectionBid() {
    }

    public ProjectionBid(@NotNull BigDecimal amount, ProjectionItem item) {
        this.amount = amount;
        this.item = item;
    }

    @Override
    public String toString() {
        return "ProjectionBid{" +
                "id=" + id +
                ", amount=" + amount +
                ", item.id=" + item.getId() +
                '}';
    }
}
