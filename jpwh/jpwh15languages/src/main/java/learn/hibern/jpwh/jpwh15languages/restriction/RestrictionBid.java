package learn.hibern.jpwh.jpwh15languages.restriction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "RESTRICTION_BID")
public class RestrictionBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    @Column(nullable = false)
    protected BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    protected RestrictionItem item;

    public RestrictionBid() {
    }

    public RestrictionBid(@NotNull BigDecimal amount, RestrictionItem item) {
        this.amount = amount;
        this.item = item;
    }
}
