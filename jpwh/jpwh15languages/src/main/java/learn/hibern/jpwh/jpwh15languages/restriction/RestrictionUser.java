package learn.hibern.jpwh.jpwh15languages.restriction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RESTRICTION_USER")
public class RestrictionUser {
    @Id
    @GeneratedValue
    protected Long id;
    protected String username;

    public RestrictionUser() {
    }

    public RestrictionUser(String username) {
        this.username = username;
    }
}
