package learn.hibern.jpwh.jpwh15languages.selection;

import learn.hibern.jpwh.jpwh15languages.AbstractJpaTest;
import learn.hibern.jpwh.jpwh15languages.Constants;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;

import static org.testng.Assert.*;

public class SelectionTest extends AbstractJpaTest {

    @BeforeMethod
    public void setUp() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_UNIT_NAME_LANGUAGES);
    }

    @AfterMethod
    public void tearDown() {
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 1)
    public void populate() {
        entityManager.persist(new SelectionItem("item-1"));
        entityManager.persist(new SelectionItem("item-2"));
    }

    @SuppressWarnings("rawtypes")
    @Test(priority = 2)
    public void testSelectJpql() {
        Query query = entityManager.createQuery("select i from SelectionItem i");
        final List list = query.getResultList();
        assertEquals(list.size(), 2);
    }

    @Test(priority = 2)
    public void testSelectCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<SelectionItem> criteria = cb.createQuery(SelectionItem.class);
        Root<SelectionItem> root = criteria.from(SelectionItem.class);
        criteria.select(root);
        TypedQuery<SelectionItem> query = entityManager.createQuery(criteria);

        final List<SelectionItem> list = query.getResultList();
        assertEquals(list.size(), 2);
    }

    @Test(priority = 2)
    public void testSelectCriteriaMoreConciseVersion() {
        CriteriaQuery<SelectionItem> criteria = entityManager.getCriteriaBuilder().createQuery(SelectionItem.class);
        criteria.select(criteria.from(SelectionItem.class));

        final List<SelectionItem> list = entityManager.createQuery(criteria).getResultList();
        assertEquals(list.size(), 2);
    }
}
