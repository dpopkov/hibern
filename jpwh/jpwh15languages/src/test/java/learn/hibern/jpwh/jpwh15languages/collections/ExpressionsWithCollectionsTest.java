package learn.hibern.jpwh.jpwh15languages.collections;

import learn.hibern.jpwh.jpwh15languages.AbstractJpaTest;
import learn.hibern.jpwh.jpwh15languages.Constants;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.Collection;
import java.util.List;

import static org.testng.Assert.*;

@SuppressWarnings("rawtypes")
public class ExpressionsWithCollectionsTest extends AbstractJpaTest {

    private static Long item1Id;
    private static Long item3Id;

    @BeforeMethod
    public void setUp() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_UNIT_NAME_COLLECTIONS);
    }

    @AfterMethod
    public void tearDown() {
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 1)
    public void populate() {
        CollectionsCategory category = new CollectionsCategory("Test1");
        CollectionsItem item1 = new CollectionsItem("item-1");
        item1.addImage(new CollectionsImage("screen1.jpg", 100, 100));
        item1.addImage(new CollectionsImage("screen2.png", 100, 100));
        CollectionsItem item2 = new CollectionsItem("item-2");
        item2.addImage(new CollectionsImage("screen3.jpg", 100, 100));
        CollectionsItem item3 = new CollectionsItem("item-3");
        category.addItem(item1);
        category.addItem(item2);
        entityManager.persist(new CollectionsCategory("Test2"));
        entityManager.persist(category);
        entityManager.persist(item1);
        item1Id = item1.getId();
        entityManager.persist(item2);
        entityManager.persist(item3);
        item3Id = item3.getId();
    }

    @Test(priority = 2)
    public void testSelectNotEmptyJpql() {
        Query query = entityManager.createQuery("select c from CollectionsCategory c");
        List list = query.getResultList();
        assertEquals(list.size(), 2);

        query = entityManager.createQuery("select c from CollectionsCategory c where c.items is not empty");
        list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testSelectNotEmptyCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<CollectionsCategory> criteria = cb.createQuery(CollectionsCategory.class);
        Root<CollectionsCategory> c = criteria.from(CollectionsCategory.class);
        criteria.select(c).where(
                cb.isNotEmpty(c.<Collection>get("items"))
        );

        Query query = entityManager.createQuery(criteria);
        List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testSelectSizeJpql() {
        Query query = entityManager.createQuery("select c from CollectionsCategory c where size(c.items) > 0");
        List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testSelectSizeCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<CollectionsCategory> criteria = cb.createQuery(CollectionsCategory.class);
        Root<CollectionsCategory> c = criteria.from(CollectionsCategory.class);
        criteria.select(c).where(
                cb.gt(
                        cb.size(c.<Collection>get("items")),
                        0
                )
        );

        Query query = entityManager.createQuery(criteria);
        List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testSelectMemberJpql() {
        CollectionsItem item1 = entityManager.find(CollectionsItem.class, item1Id);
        Query query = entityManager.createQuery(
                "select c from CollectionsCategory c where :item member of c.items");
        query.setParameter("item", item1);
        List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testSelectMemberCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<CollectionsCategory> criteria = cb.createQuery(CollectionsCategory.class);
        Root<CollectionsCategory> c = criteria.from(CollectionsCategory.class);
        criteria.select(c).where(
                cb.isMember(
                        cb.parameter(CollectionsItem.class, "item"),
                        c.<Collection<CollectionsItem>>get("items")
                )
        );

        Query query = entityManager.createQuery(criteria);
        CollectionsItem item1 = entityManager.getReference(CollectionsItem.class, item1Id);
        query.setParameter("item", item1);

        List list = query.getResultList();
        assertEquals(list.size(), 1);

        CollectionsItem item3 = entityManager.getReference(CollectionsItem.class, item3Id);
        query.setParameter("item", item3);
        list = query.getResultList();
        assertEquals(list.size(), 0);
    }

    /*
    Page 379
    select value(img)
        from Item i join i.images img
        where key(img) like '%.jpg'
     */

    @Test(priority = 2)
    public void testMapKeyValues() {
        Query query = entityManager.createQuery(
                "select value(img) from CollectionsItem i join i.images img where key(img) like '%.jpg'");
        List list = query.getResultList();
        assertEquals(list.size(), 2);

        query = entityManager.createQuery(
                "select value(img) from CollectionsItem i join i.images img where key(img) like '%.png'");
        list = query.getResultList();
        assertEquals(list.size(), 1);
    }
}
