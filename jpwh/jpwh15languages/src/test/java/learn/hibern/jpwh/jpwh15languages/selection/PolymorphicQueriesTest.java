package learn.hibern.jpwh.jpwh15languages.selection;

import learn.hibern.jpwh.jpwh15languages.AbstractJpaTest;
import learn.hibern.jpwh.jpwh15languages.Constants;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import java.util.List;

import static org.testng.Assert.*;

@SuppressWarnings("rawtypes")
public class PolymorphicQueriesTest extends AbstractJpaTest {

    private static Long accountId;
    private static Long cardId;

    @BeforeMethod
    public void setUp() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_UNIT_NAME_LANGUAGES);
    }

    @AfterMethod
    public void tearDown() {
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 1)
    public void populate() {
        SelectionBillingDetails account = new SelectionBankAccount(
                "Owner", "12345", "BankName", "Swift");
        SelectionBillingDetails card = new SelectionCreditCard(
                "Owner", "987654321", "12", "2023");
        entityManager.persist(account);
        accountId = account.getId();
        entityManager.persist(card);
        cardId = card.getId();
    }

    @Test(priority = 2)
    public void testSelectJpql() {
        Query query = entityManager.createQuery("select bd from SelectionBillingDetails bd");

        final List list = query.getResultList();
        assertBillingDetails(list);
    }

    @Test(priority = 2)
    public void testSelectCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<SelectionBillingDetails> criteria = cb.createQuery(SelectionBillingDetails.class);
        criteria.select(criteria.from(SelectionBillingDetails.class));
        TypedQuery<SelectionBillingDetails> query = entityManager.createQuery(criteria);

        final List list = query.getResultList();
        assertBillingDetails(list);
    }

    @Test(priority = 2)
    public void testSelectAccountsJpql() {
        Query query = entityManager.createQuery("select acc from SelectionBankAccount acc");
        final List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testSelectAccountsCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<SelectionBankAccount> criteria = cb.createQuery(SelectionBankAccount.class);
        criteria.select(criteria.from(SelectionBankAccount.class));
        TypedQuery<SelectionBankAccount> query = entityManager.createQuery(criteria);

        final List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testSelectCardsJpql() {
        Query query = entityManager.createQuery("select c from SelectionCreditCard c");
        final List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testSelectCardsCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<SelectionCreditCard> criteria = cb.createQuery(SelectionCreditCard.class);
        criteria.select(criteria.from(SelectionCreditCard.class));
        TypedQuery<SelectionCreditCard> query = entityManager.createQuery(criteria);

        final List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    private void assertBillingDetails(List list) {
        assertEquals(list.size(), 2);
        final Object first = list.get(0);
        boolean accountChecked = false;
        boolean cardChecked = false;
        if (first instanceof SelectionBankAccount) {
            SelectionBankAccount bankAccount = (SelectionBankAccount) first;
            assertEquals(bankAccount.getId(), accountId);
            accountChecked = true;
        } else if (first instanceof SelectionCreditCard) {
            SelectionCreditCard creditCard = (SelectionCreditCard) first;
            assertEquals(creditCard.getId(), cardId);
            cardChecked = true;
        }
        final Object second = list.get(1);
        if (second instanceof SelectionBankAccount) {
            SelectionBankAccount bankAccount = (SelectionBankAccount) second;
            assertEquals(bankAccount.getId(), accountId);
            accountChecked = true;
        } else if (second instanceof SelectionCreditCard) {
            SelectionCreditCard creditCard = (SelectionCreditCard) second;
            assertEquals(creditCard.getId(), cardId);
            cardChecked = true;
        }
        assertTrue(accountChecked);
        assertTrue(cardChecked);
    }
}
