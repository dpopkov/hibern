package learn.hibern.jpwh.jpwh15languages.projection;

import learn.hibern.jpwh.jpwh15languages.AbstractJpaTest;
import learn.hibern.jpwh.jpwh15languages.Constants;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ProjectionTest extends AbstractJpaTest {

    @BeforeMethod
    public void setUp() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_UNIT_NAME_PROJECTIONS);
    }

    @AfterMethod
    public void tearDown() {
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 1)
    public void populate() {
        ProjectionItem item1 = new ProjectionItem("item-1");
        ProjectionItem item2 = new ProjectionItem("item-2");
        ProjectionBid bid11 = new ProjectionBid(BigDecimal.ONE, item1);
        ProjectionBid bid12 = new ProjectionBid(BigDecimal.TEN, item1);
        ProjectionBid bid21 = new ProjectionBid(BigDecimal.ONE, item2);
        ProjectionBid bid22 = new ProjectionBid(BigDecimal.TEN, item2);
        entityManager.persist(item1);
        entityManager.persist(item2);
        entityManager.persist(bid11);
        entityManager.persist(bid12);
        entityManager.persist(bid21);
        entityManager.persist(bid22);
    }

    @Test(priority = 2)
    public void testCartesianProductJpql() {
        Query query = entityManager.createQuery("select i, b from ProjectionItem i, ProjectionBid b");
        final List<?> list = query.getResultList();
        DataSet dataSet = new DataSet(list);
        int numItems = dataSet.items.size();
        int numBids = dataSet.bids.size();
        assertEquals(numItems, 2);
        assertEquals(numBids, 4);
        assertEquals(list.size(), numItems * numBids);
        printRows(list);
    }

    @Test(priority = 2)
    public void testCartesianProductCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Object> criteria = cb.createQuery();
        Root<ProjectionItem> i = criteria.from(ProjectionItem.class);
        Root<ProjectionBid> b = criteria.from(ProjectionBid.class);
        criteria.select(cb.tuple(i, b));

        Query query = entityManager.createQuery(criteria);
        final List<?> list = query.getResultList();
        DataSet dataSet = new DataSet(list);
        int numItems = dataSet.items.size();
        int numBids = dataSet.bids.size();
        assertEquals(numItems, 2);
        assertEquals(numBids, 4);
        assertEquals(list.size(), numItems * numBids);
        printRows(list);
    }

    @Test(priority = 2)
    public void testCartesianProductCriteriaTupleQuery() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteria = cb.createTupleQuery();
        criteria.multiselect(
                criteria.from(ProjectionItem.class).alias("i"),
                criteria.from(ProjectionBid.class).alias("b")
        );
        TypedQuery<Tuple> query = entityManager.createQuery(criteria);
        final List<Tuple> list = query.getResultList();
        for (Tuple tuple : list) {
            ProjectionItem item = tuple.get(0, ProjectionItem.class);   // Indexed
            ProjectionBid bid = tuple.get(1, ProjectionBid.class);

            item = tuple.get("i", ProjectionItem.class);    // Alias
            bid = tuple.get("b", ProjectionBid.class);

            /*
            for (TupleElement<?> element : tuple.getElements()) {
                Class<?> clazz = element.getJavaType();     // Meta
                String alias = element.getAlias();
                Object value = tuple.get(element);
            }
            */
        }
    }

    private static class DataSet {
        Set<ProjectionItem> items = new HashSet<>();
        Set<ProjectionBid> bids = new HashSet<>();

        DataSet(List<?> rows) {
            for (Object row : rows) {
                addRow(row);
            }
        }

        void addRow(Object row) {
            Object[] columns = (Object[]) row;
            assertTrue(columns[0] instanceof ProjectionItem);
            items.add((ProjectionItem) columns[0]);
            assertTrue(columns[1] instanceof ProjectionBid);
            bids.add((ProjectionBid) columns[1]);
        }
    }

    private void printRows(List<?> list) {
        for (Object row : list) {
            if (row instanceof Object[]) {
                Object[] columns = (Object[]) row;
                for (Object column : columns) {
                    System.out.print(column);
                    System.out.print(" | ");
                }
                System.out.println();
            }
        }
    }
}
