package learn.hibern.jpwh.jpwh15languages;

public class Constants {
    public static final String PERSISTENCE_UNIT_NAME_LANGUAGES = "ch15languages";
    public static final String PERSISTENCE_UNIT_NAME_RESTRICTION = "ch15restriction";
    public static final String PERSISTENCE_UNIT_NAME_COLLECTIONS = "ch15collections";
    public static final String PERSISTENCE_UNIT_NAME_PROJECTIONS = "ch15projections";
}
