package learn.hibern.jpwh.jpwh15languages.restriction;

import learn.hibern.jpwh.jpwh15languages.AbstractJpaTest;
import learn.hibern.jpwh.jpwh15languages.Constants;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.List;

import static org.testng.Assert.*;

@SuppressWarnings("rawtypes")
public class ComparisonTest extends AbstractJpaTest {

    private static final BigDecimal TWENTY = BigDecimal.TEN.add(BigDecimal.TEN);

    @BeforeMethod
    public void setUp() {
        createEntityManagerAndBeginTransaction(Constants.PERSISTENCE_UNIT_NAME_RESTRICTION);
    }

    @AfterMethod
    public void tearDown() {
        commitTransactionAndCloseEntityManager();
    }

    @Test(priority = 1)
    public void populate() {
        RestrictionItem item = new RestrictionItem("item-1", BigDecimal.ZERO);
        RestrictionItem item2 = new RestrictionItem("item-2");
        item2.setAuctionType(AuctionType.FIXED_PRICE);
        RestrictionBid bid1 = new RestrictionBid(BigDecimal.ONE, item);
        RestrictionBid bid2 = new RestrictionBid(BigDecimal.TEN, item);
        RestrictionBid bid3 = new RestrictionBid(TWENTY, item);
        entityManager.persist(item);
        entityManager.persist(item2);
        entityManager.persist(bid1);
        entityManager.persist(bid2);
        entityManager.persist(bid3);
        entityManager.persist(new RestrictionUser("john-1"));
        entityManager.persist(new RestrictionUser("jane-2"));
        entityManager.persist(new RestrictionUser("june-3"));
    }

    @Test(priority = 2)
    public void testRangeJpql() {
        Query query = entityManager.createQuery("select b from RestrictionBid b where b.amount between 1 and 10");
        List list = query.getResultList();
        assertEquals(list.size(), 2);

        query = entityManager.createQuery("select b from RestrictionBid b where b.amount > 10");
        list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testRangeCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<RestrictionBid> criteria = cb.createQuery(RestrictionBid.class);
        Root<RestrictionBid> root = criteria.from(RestrictionBid.class);
        criteria.select(root).where(
                cb.between(
                        root.get("amount"),
                        BigDecimal.ONE, BigDecimal.TEN
                )
        );

        Query query = entityManager.createQuery(criteria);
        List list = query.getResultList();
        assertEquals(list.size(), 2);

        criteria = cb.createQuery(RestrictionBid.class);
        root = criteria.from(RestrictionBid.class);
        criteria.select(root).where(
                cb.gt(
                        root.<BigDecimal>get("amount"),
                        BigDecimal.TEN
                )
        );
        query = entityManager.createQuery(criteria);
        list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testInJpql() {
        Query query = entityManager.createQuery(
                "select u from RestrictionUser u where u.username in ('jane-2', 'june-3')");
        final List list = query.getResultList();
        assertEquals(list.size(), 2);
    }

    @Test(priority = 2)
    public void testInCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<RestrictionUser> criteria = cb.createQuery(RestrictionUser.class);
        Root<RestrictionUser> u = criteria.from(RestrictionUser.class);
        criteria.select(u).where(
                cb.in(u.<String>get("username"))
                .value("jane-2")
                .value("june-3")
        );

        Query query = entityManager.createQuery(criteria);
        final List list = query.getResultList();
        assertEquals(list.size(), 2);
    }

    @Test(priority = 2)
    public void testEnumJpql() {
        Query query = entityManager.createQuery(
                "select i from RestrictionItem i where i.auctionType = learn.hibern.jpwh.jpwh15languages.restriction.AuctionType.HIGHEST_BID");
        final List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testEnumCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<RestrictionItem> criteria = cb.createQuery(RestrictionItem.class);
        Root<RestrictionItem> i = criteria.from(RestrictionItem.class);
        criteria.select(i).where(
                cb.equal(
                        i.<AuctionType>get("auctionType"),
                        AuctionType.FIXED_PRICE
                )
        );

        Query query = entityManager.createQuery(criteria);
        final List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testIsNullJpql() {
        Query query = entityManager.createQuery("select i from RestrictionItem i where i.buyNowPrice is null");
        final List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testIsNullCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<RestrictionItem> criteria = cb.createQuery(RestrictionItem.class);
        Root<RestrictionItem> i = criteria.from(RestrictionItem.class);
        criteria.select(i).where(cb.isNull(i.get("buyNowPrice")));
        Query query = entityManager.createQuery(criteria);
        final List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testIsNotNullJpql() {
        Query query = entityManager.createQuery("select i from RestrictionItem i where i.buyNowPrice is not null");
        final List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testIsNotNullCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<RestrictionItem> criteria = cb.createQuery(RestrictionItem.class);
        Root<RestrictionItem> i = criteria.from(RestrictionItem.class);
        criteria.select(i).where(cb.isNotNull(i.get("buyNowPrice")));
        Query query = entityManager.createQuery(criteria);
        final List list = query.getResultList();
        assertEquals(list.size(), 1);
    }

    @Test(priority = 2)
    public void testLikeJpql() {
        Query query = entityManager.createQuery("select u from RestrictionUser u where u.username like 'j%'");
        List list = query.getResultList();
        assertEquals(list.size(), 3);

        query = entityManager.createQuery("select u from RestrictionUser u where u.username like '%ne-%'");
        list = query.getResultList();
        assertEquals(list.size(), 2);
    }

    @Test(priority = 2)
    public void testLikeCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<RestrictionUser> criteria = cb.createQuery(RestrictionUser.class);
        Root<RestrictionUser> u = criteria.from(RestrictionUser.class);
        criteria.select(u).where(
                cb.like(u.get("username"),
                        "j%")

        );
        Query query = entityManager.createQuery(criteria);
        List list = query.getResultList();
        assertEquals(list.size(), 3);

        criteria = cb.createQuery(RestrictionUser.class);
        u = criteria.from(RestrictionUser.class);
        criteria.select(u).where(
                cb.like(u.get("username"),
                        "%ne-%")

        );
        query = entityManager.createQuery(criteria);
        list = query.getResultList();
        assertEquals(list.size(), 2);
    }

    @Test(priority = 2)
    public void testCombineExpressionsJpql() {
        Query query = entityManager.createQuery("select i from RestrictionItem i "
                + "where (i.name like '%-1' and i.buyNowPrice is not null) or i.name = 'item-2'");
        final List list = query.getResultList();
        assertEquals(list.size(), 2);
    }

    @Test(priority = 2)
    public void testCombineExpressionsCriteria() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<RestrictionItem> criteria = cb.createQuery(RestrictionItem.class);
        Root<RestrictionItem> i = criteria.from(RestrictionItem.class);
        Predicate predicate = cb.and(
                cb.like(i.get("name"), "%-1"),
                cb.isNotNull(i.get("buyNowPrice"))
        );
        predicate = cb.or(
                predicate,
                cb.equal(i.get("name"), "item-2")
        );
        criteria.select(i).where(predicate);

        Query query = entityManager.createQuery(criteria);
        final List list = query.getResultList();
        assertEquals(list.size(), 2);
    }

    @Test(priority = 2)
    public void testCombineAndExpressionsCriteriaFluent() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<RestrictionItem> criteria = cb.createQuery(RestrictionItem.class);
        Root<RestrictionItem> i = criteria.from(RestrictionItem.class);
        criteria.select(i).where(
                cb.like(i.get("name"), "%-1"),
                // AND
                cb.isNotNull(i.get("buyNowPrice")),
                // AND
                cb.equal(i.get("auctionType"), AuctionType.HIGHEST_BID)
        );

        Query query = entityManager.createQuery(criteria);
        final List list = query.getResultList();
        assertEquals(list.size(), 1);
    }
}
