package learn.hibern.jpwh.jpwh12fetch.eager;

import learn.hibern.jpwh.jpwh12fetch.AbstractJpaTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.List;

import static org.testng.Assert.*;

public class EagerItemTest extends AbstractJpaTest {

    private static PersistenceUtil persistenceUtil;

    @BeforeClass
    public void beforeClass() {
        persistenceUtil = Persistence.getPersistenceUtil();
    }

    @BeforeMethod
    public void setUp() {
        persistTestData();
    }

    private void persistTestData() {
        EagerUser seller = new EagerUser("Seller");
        EagerItem item = new EagerItem("item-1", seller);
        EagerBid bid1 = new EagerBid(BigDecimal.ONE, item);
        EagerBid bid2 = new EagerBid(BigDecimal.TEN, item);

        createEntityManagerAndBeginTransaction();
        entityManager.persist(seller);
        entityManager.persist(item);
        entityManager.persist(bid1);
        entityManager.persist(bid2);
        commitTransactionAndCloseEntityManager();
    }

    @Test
    public void testQuery() {
        createEntityManagerAndBeginTransaction();
        TypedQuery<EagerItem> query = entityManager.createQuery("select i from EagerItem i join fetch i.seller",
                EagerItem.class);
        final List<EagerItem> items = query.getResultList();
        commitTransactionAndCloseEntityManager();

        for (EagerItem i : items) {
            assertFalse(persistenceUtil.isLoaded(i, "bids"));
            assertTrue(persistenceUtil.isLoaded(i, "seller"));
            assertNotNull(i.getSeller().getName());
        }
    }

    @Test
    public void testCriteriaQuery() {
        createEntityManagerAndBeginTransaction();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<EagerItem> criteria = builder.createQuery(EagerItem.class);
        Root<EagerItem> root = criteria.from(EagerItem.class);
        root.fetch("seller");
        criteria.select(root);

        final List<EagerItem> items = entityManager.createQuery(criteria).getResultList();
        commitTransactionAndCloseEntityManager();

        for (EagerItem i : items) {
            assertFalse(persistenceUtil.isLoaded(i, "bids"));
            assertTrue(persistenceUtil.isLoaded(i, "seller"));
            assertNotNull(i.getSeller().getName());
        }
    }

    @Test
    public void testQueryCollection() {
        createEntityManagerAndBeginTransaction();
        TypedQuery<EagerItem> query = entityManager.createQuery("select i from EagerItem i left join fetch i.bids",
                EagerItem.class);
        final List<EagerItem> items = query.getResultList();
        commitTransactionAndCloseEntityManager();

        for (EagerItem i : items) {
            assertTrue(persistenceUtil.isLoaded(i, "bids"));
            assertFalse(persistenceUtil.isLoaded(i, "seller"));
            assertTrue(i.getBids().size() > 0);
        }
    }

    @Test
    public void testCriteriaQueryCollection() {
        createEntityManagerAndBeginTransaction();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<EagerItem> criteria = builder.createQuery(EagerItem.class);
        Root<EagerItem> root = criteria.from(EagerItem.class);
        root.fetch("bids", JoinType.LEFT);
        criteria.select(root);

        TypedQuery<EagerItem> query = entityManager.createQuery(criteria);
        final List<EagerItem> items = query.getResultList();
        commitTransactionAndCloseEntityManager();

        for (EagerItem i : items) {
            assertTrue(persistenceUtil.isLoaded(i, "bids"));
            assertFalse(persistenceUtil.isLoaded(i, "seller"));
            assertTrue(i.getBids().size() > 0);
        }
    }
}
