package learn.hibern.jpwh.jpwh12fetch.proxy;

import learn.hibern.jpwh.jpwh12fetch.AbstractJpaTest;
import org.hibernate.proxy.HibernateProxyHelper;
import org.testng.annotations.Test;

import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;

import static org.testng.Assert.*;

public class ProxyItemTest extends AbstractJpaTest {

    @Test
    public void testGetReference() {
        ProxyUser sellerToSave = new ProxyUser("Seller");
        ProxyItem itemToSave = new ProxyItem("item-1", sellerToSave);
        createEntityManagerAndBeginTransaction();
        entityManager.persist(sellerToSave);
        entityManager.persist(itemToSave);
        final Long itemId = itemToSave.getId();
        commitTransactionAndCloseEntityManager();

        createEntityManagerAndBeginTransaction();
        ProxyItem item = entityManager.getReference(ProxyItem.class, itemId);
        assertEquals(item.getId(), itemId);

        Class<? extends ProxyItem> generatedProxyClass = item.getClass();
        assertNotEquals(generatedProxyClass, ProxyItem.class);
        Class<?> actualClass = HibernateProxyHelper.getClassWithoutInitializingProxy(item);
        assertEquals(actualClass, ProxyItem.class);

        PersistenceUtil persistenceUtil = Persistence.getPersistenceUtil();
        assertFalse(persistenceUtil.isLoaded(item));
        assertFalse(persistenceUtil.isLoaded(item, "seller"));

        ProxyUser seller = item.getSeller();
        assertTrue(persistenceUtil.isLoaded(item, "seller"));
        System.out.println("seller = " + seller);

        commitTransactionAndCloseEntityManager();
    }
}
