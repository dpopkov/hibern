package learn.hibern.jpwh.jpwh12fetch.collections;

import learn.hibern.jpwh.jpwh12fetch.AbstractJpaTest;
import org.testng.annotations.Test;

import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.*;

public class CollectionsItemTest extends AbstractJpaTest {

    private Long itemId;

    @Test
    public void testCollectionIsNotLoaded() {
        persistTestData();

        createEntityManagerAndBeginTransaction();
        CollectionsItem found = entityManager.find(CollectionsItem.class, itemId);
        final Set<CollectionsBid> bids = found.getBids();

        PersistenceUtil persistenceUtil = Persistence.getPersistenceUtil();
        assertFalse(persistenceUtil.isLoaded(found, "bids"));
        assertTrue(Set.class.isAssignableFrom(bids.getClass()));
        assertNotEquals(bids.getClass(), HashSet.class);
        assertEquals(bids.getClass(), org.hibernate.collection.internal.PersistentSet.class);

        commitTransactionAndCloseEntityManager();
    }

    @Test(expectedExceptions = org.hibernate.LazyInitializationException.class)
    public void testLazyInitializationException() {
        persistTestData();

        createEntityManagerAndBeginTransaction();
        CollectionsItem found = entityManager.find(CollectionsItem.class, itemId);
        final Set<CollectionsBid> bids = found.getBids();
        commitTransactionAndCloseEntityManager();

        final int bidsSize = bids.size();
        assertEquals(bidsSize, 2);
    }

    private void persistTestData() {
        createEntityManagerAndBeginTransaction();
        CollectionsItem item = new CollectionsItem("item-1");
        entityManager.persist(item);
        itemId = item.getId();
        entityManager.persist(new CollectionsBid(BigDecimal.ONE, item));
        entityManager.persist(new CollectionsBid(BigDecimal.TEN, item));
        commitTransactionAndCloseEntityManager();
    }
}
