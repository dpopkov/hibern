package learn.hibern.jpwh.jpwh12fetch.collections;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "COLLECTIONS_ITEM")
public class CollectionsItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @OneToMany(mappedBy = "item")
    protected Set<CollectionsBid> bids = new HashSet<>();

    protected CollectionsItem() {
    }

    public CollectionsItem(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public Set<CollectionsBid> getBids() {
        return bids;
    }
}
