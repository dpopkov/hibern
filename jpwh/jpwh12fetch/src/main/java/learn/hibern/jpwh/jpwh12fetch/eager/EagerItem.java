package learn.hibern.jpwh.jpwh12fetch.eager;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "EAGER_ITEM")
public class EagerItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @ManyToOne(fetch = FetchType.LAZY)
    protected EagerUser seller;

    @OneToMany(mappedBy = "item")
    protected Set<EagerBid> bids = new HashSet<>();

    protected EagerItem() {
    }

    public EagerItem(String name, EagerUser seller) {
        this.name = name;
        this.seller = seller;
    }

    public Long getId() {
        return id;
    }

    public Set<EagerBid> getBids() {
        return bids;
    }

    public EagerUser getSeller() {
        return seller;
    }
}
