package learn.hibern.jpwh.jpwh12fetch.proxy;

import javax.persistence.*;

@Entity
@Table(name = "PROXY_ITEM")
public class ProxyItem {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    @ManyToOne
    protected ProxyUser seller;

    protected ProxyItem() {
    }

    public ProxyItem(String name, ProxyUser seller) {
        this.name = name;
        this.seller = seller;
    }

    public Long getId() {
        return id;
    }

    public ProxyUser getSeller() {
        return seller;
    }
}
