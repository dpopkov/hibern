package learn.hibern.jpwh.jpwh12fetch.eager;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EAGER_USER")
public class EagerUser {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    protected EagerUser() {
    }

    public EagerUser(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "EagerUser{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
