package learn.hibern.jpwh.jpwh12fetch.collections;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "COLLECTIONS_BID")
public class CollectionsBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    protected BigDecimal amount;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    protected CollectionsItem item;

    protected CollectionsBid() {
    }

    public CollectionsBid(BigDecimal amount, CollectionsItem item) {
        this.amount = amount;
        this.item = item;
        item.getBids().add(this);
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public CollectionsItem getItem() {
        return item;
    }

    @Override
    public String toString() {
        return "CollectionsBid{" +
                "id=" + id +
                ", amount=" + amount +
                ", itemId=" + item.getId() +
                '}';
    }
}
