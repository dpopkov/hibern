package learn.hibern.jpwh.jpwh12fetch.eager;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "EAGER_BID")
public class EagerBid {
    @Id
    @GeneratedValue
    protected Long id;

    @NotNull
    protected BigDecimal amount;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    protected EagerItem item;

    protected EagerBid() {
    }

    public EagerBid(BigDecimal amount, EagerItem item) {
        this.amount = amount;
        this.item = item;
        item.getBids().add(this);
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public EagerItem getItem() {
        return item;
    }

    @Override
    public String toString() {
        return "EagerBid{" +
                "id=" + id +
                ", amount=" + amount +
                ", itemId=" + item.getId() +
                '}';
    }
}
