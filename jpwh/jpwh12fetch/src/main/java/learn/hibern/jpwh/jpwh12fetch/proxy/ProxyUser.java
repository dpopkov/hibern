package learn.hibern.jpwh.jpwh12fetch.proxy;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PROXY_USER")
public class ProxyUser {
    @Id
    @GeneratedValue
    protected Long id;

    protected String name;

    protected ProxyUser() {
    }

    public ProxyUser(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ProxyUser{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
