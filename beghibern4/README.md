Beginning Hibernate 5
=====================

Chapter 6: Mapping with Annotations
-----------------------------------
* Entity beans with @Entity.
* Primary Keys with @Id and @GeneratedValue.
* Compound Keys with @Embeddable as @Id.
* Using @Column annotation with attributes (name, length, nullable, unique).

Chapter 4: The Persistence Life Cycle
-------------------------------------
* Entities and Associations: Example of broken association without an owning object.
* Entities and Associations: Example of mapped association with an owning object.
* Saving Entities.
* Object Equality and Identity. Loading Entities.
* Merging and Refreshing Entities.
* Cascading Operations, Orphan Removal.

Chapter 3: Building a Simple Application
----------------------------------------
* Mark entity with @Entity annotation
* Mark columns with @Column annotation
* Add artificial key with @Id and @GeneratedValue annotation
* Use HQL for reading data
* Add util module containing SessionUtil singleton
* Create RankingService with methods for adding and getting Ranking
* Add methods for updating and removing to RankingService
* Add methods for finding all rankings for a subject and highest ranked subject

Chapter 2: Integrating and Configuring Hibernate
------------------------------------------------
* Using hibernate.cfg.xml to configure Hibernate
* Using Connection Pooling
* Example of how to configure JNDI as a Datasource