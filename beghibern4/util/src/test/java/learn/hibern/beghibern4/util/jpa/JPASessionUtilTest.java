package learn.hibern.beghibern4.util.jpa;

import learn.hibern.beghibern4.util.model.Thing;
import org.hibernate.Session;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import static org.testng.Assert.*;

public class JPASessionUtilTest {

    public static final String PERSISTENCE_UNIT_NAME = "utiljpa";

    @Test
    public void testGetEntityManager() {
        EntityManager em = JPASessionUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        assertNotNull(em);
        em.close();
    }

    @Test(expectedExceptions = {javax.persistence.PersistenceException.class})
    public void nonexistentEntityManagerName() {
        JPASessionUtil.getEntityManager("nonexistent");
        fail("We shouldn't be able to acquire an EntityManager here");
    }

    @Test
    public void getSession() {
        Session session = JPASessionUtil.getSession("utiljpa");
        assertNotNull(session);
        session.close();
    }

    @Test(expectedExceptions = {javax.persistence.PersistenceException.class})
    public void nonexistentSessionName() {
        JPASessionUtil.getSession("nonexistent");
        fail("We shouldn't be able to acquire a Session here");
    }

    @Test
    public void testEntityManager() {
        EntityManager em = JPASessionUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        Thing t = new Thing();
        t.setName("Thing 1");
        em.persist(t);
        em.getTransaction().commit();
        em.close();

        em = JPASessionUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        TypedQuery<Thing> q = em.createQuery("from Thing t where t.name=:name", Thing.class);
        q.setParameter("name", "Thing 1");
        Thing result = q.getSingleResult();
        assertNotNull(result);
        assertEquals(t, result);
        em.remove(result);
        em.getTransaction().commit();
        em.close();
    }
}
