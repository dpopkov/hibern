package learn.hibern.beghibern4.util.hibernate;

import learn.hibern.beghibern4.util.model.Thing;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SessionUtilTest {

    @Test
    public void testOpenSession() {
        try (Session session = SessionUtil.openSession()) {
            assertNotNull(session);
        }
    }

    @Test
    public void testSession() {
        final String name = "Thing " + (int)(Math.random() * Integer.MAX_VALUE);
        Integer thingId;
        try (Session session = SessionUtil.openSession()) {
            final Transaction tx = session.beginTransaction();
            Thing t = new Thing();
            t.setName(name);
            session.save(t);
            thingId = t.getId();
            assertNotNull(thingId);
            tx.commit();
        }
        try (Session session = SessionUtil.openSession()) {
            final Transaction tx = session.beginTransaction();
            Thing found = session.get(Thing.class, thingId);
            assertNotNull(found);
            assertEquals(found.getName(), name);
            tx.commit();
        }
    }
}
