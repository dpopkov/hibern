package learn.hibern.beghibern4.ch07.validation;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
public class ValidatedSimplePerson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    @NotNull
    @Size(min = 2, max = 20)
    private String fname;
    @Column(nullable = false)
    @NotNull
    @Size(min = 2, max = 20)
    private String lname;
    @Column
    @Min(value = 10)
    private Integer age;
}
