package learn.hibern.beghibern4.ch07.validation;

import learn.hibern.beghibern4.util.jpa.JPASessionUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolationException;

import static org.testng.Assert.*;

public class ValidationTest {

    public static final String PERSISTENCE_UNIT_NAME = "ch07validation";

    @Test
    public void testCreateValidPerson() {
        ValidatedSimplePerson person = ValidatedSimplePerson.builder()
                .age(15)
                .fname("Johnny")
                .lname("McYoungster").build();
        person = persist(person);
        assertNotNull(person.getId());
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void createValidatedUnderagePerson() {
        ValidatedSimplePerson person = ValidatedSimplePerson.builder()
                .age(5)
                .fname("Johnny")
                .lname("McYoungster").build();
        persist(person);
        fail("Should have failed validation");
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void createValidatedPoorFNamePerson2() {
        persist(ValidatedSimplePerson.builder()
                .age(14)
                .fname("J")
                .lname("McYoungster2").build());
        fail("Should have failed validation");
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void createValidatedNoFNamePerson() {
        persist(ValidatedSimplePerson.builder()
                .age(14)
                .lname("McYoungster2").build());
        fail("Should have failed validation");
    }

    private ValidatedSimplePerson persist(ValidatedSimplePerson person) {
        EntityManager em = JPASessionUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(person);
        em.getTransaction().commit();
        em.close();
        return person;
    }
}
