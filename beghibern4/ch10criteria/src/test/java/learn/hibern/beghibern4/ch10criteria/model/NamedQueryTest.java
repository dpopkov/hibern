package learn.hibern.beghibern4.ch10criteria.model;

import learn.hibern.beghibern4.ch10criteria.Config;
import learn.hibern.beghibern4.util.jpa.JPASessionUtil;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.util.List;

import static org.testng.Assert.*;

public class NamedQueryTest {

    private EntityManager entityManager;

    @BeforeMethod
    public void setup() {
        populateData();

        this.entityManager = JPASessionUtil.getEntityManager(Config.PERSISTENCE_UNIT_NAME);
        this.entityManager.getTransaction().begin();
    }

    @AfterMethod
    public void closeSession() {
        entityManager.createQuery("delete from Product").executeUpdate();
        entityManager.createQuery("delete from Supplier ").executeUpdate();
        if (entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().commit();
        }
        if (entityManager.isOpen()) {
            entityManager.close();
        }
    }

    @Test
    public void testNamedQuery() {
        Query query = entityManager.createNamedQuery("supplier.findAll");
        @SuppressWarnings("rawtypes") final List suppliers = query.getResultList();
        assertEquals(suppliers.size(), 2);
    }

    private void populateData() {
        EntityManager em = JPASessionUtil.getEntityManager(Config.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();

        Supplier supplier = new Supplier("Hardware, Inc.");
        supplier.getProducts().add(new Product(supplier, "Optical Wheel Mouse","Mouse",  5.0));
        supplier.getProducts().add(new Product(supplier, "Trackball Mouse", "Mouse", 5.0));
        em.persist(supplier);

        supplier = new Supplier("Software, Inc.");
        supplier.getProducts().add(new Software(supplier, "SuperDetect", "Antivirus", 14.95, "1.0"));
        supplier.getProducts().add(new Software(supplier, "Wildcat", "Browser", 19.95, "2.2"));
        supplier.getProducts().add(new Product(supplier, "AxeGrinder", "Gaming Mouse", 42.0));
        em.persist(supplier);

        em.getTransaction().commit();
        em.close();
    }
}
