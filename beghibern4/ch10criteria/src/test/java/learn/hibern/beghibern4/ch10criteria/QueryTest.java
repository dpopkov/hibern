package learn.hibern.beghibern4.ch10criteria;

import learn.hibern.beghibern4.ch10criteria.model.Product;
import learn.hibern.beghibern4.ch10criteria.model.Product_;
import learn.hibern.beghibern4.ch10criteria.model.Software;
import learn.hibern.beghibern4.ch10criteria.model.Supplier;
import learn.hibern.beghibern4.util.jpa.JPASessionUtil;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.function.Consumer;

import static org.testng.Assert.*;

public class QueryTest {

    @BeforeMethod
    public void populateData() {
        doWithEntityManager((em) -> {
            Supplier supplier = new Supplier("Hardware, Inc.");
            supplier.getProducts().add(new Product(supplier, "Optical Wheel Mouse", "Mouse", 5.00));
            supplier.getProducts().add(new Product(supplier, "Trackball Mouse", "Mouse", 22.00));
            em.persist(supplier);

            supplier = new Supplier("Hardware Are We");
            supplier.getProducts().add(new Software(supplier, "SuperDetect", "Antivirus", 14.95, "1.0"));
            supplier.getProducts().add(new Software(supplier, "Wildcat", "Browser", 19.95, "2.2"));
            supplier.getProducts().add(new Product(supplier, "AxeGrinder", "Gaming Mouse", 42.00));
            supplier.getProducts().add(new Product(supplier, "I5 Tablet", "Computer", 849.99));
            supplier.getProducts().add(new Product(supplier, "I7 Desktop", "Computer", 1599.99));
            em.persist(supplier);
        });
    }

    @AfterMethod
    public void cleanup() {
        doWithEntityManager((em) -> {
            em.createQuery("delete from Software").executeUpdate();
            em.createQuery("delete from Product").executeUpdate();
            em.createQuery("delete from Supplier").executeUpdate();
        });
    }

    @Test
    public void testSimpleCriteriaQuery() {
        doWithEntityManager((em) -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);
            Root<Product> root = criteria.from(Product.class);
            criteria.select(root);

            assertEquals(em.createQuery(criteria).getResultList().size(), 7);
        });
    }

    @Test
    public void testSimpleEqQuery() {
        doWithEntityManager((em) -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);

            Metamodel m = em.getMetamodel();
            EntityType<Product> product = m.entity(Product.class);
            Root<Product> root = criteria.from(product);
            criteria.select(root);
            criteria.where(
                    builder.equal(
                            root.get(Product_.description),
                            builder.parameter(String.class, "description")
                    )
            );

            criteria.select(root);

            assertEquals(em.createQuery(criteria)
                    .setParameter("description", "Mouse")
                    .getResultList().size(), 2);
        });
    }

    @Test
    public void testSimpleNeQuery() {
        doWithEntityManager((em) -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);

            Metamodel m = em.getMetamodel();
            EntityType<Product> product = m.entity(Product.class);
            Root<Product> root = criteria.from(product);
            criteria.select(root);
            criteria.where(
                    builder.notEqual(
                            root.get(Product_.description),
                            builder.parameter(String.class, "description")
                    )
            );

            criteria.select(root);

            assertEquals(em.createQuery(criteria)
                    .setParameter("description", "Mouse")
                    .getResultList().size(), 5);
        });
    }

    @Test
    public void testSimpleLikeQuery() {
        doWithEntityManager((em) -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);

            Metamodel m = em.getMetamodel();
            EntityType<Product> product = m.entity(Product.class);
            Root<Product> root = criteria.from(product);
            criteria.select(root);
            criteria.where(
                    builder.like(
                            root.get(Product_.description),
                            builder.parameter(String.class, "description")
                    )
            );

            criteria.select(root);

            assertEquals(em.createQuery(criteria)
                    .setParameter("description", "%Mouse")
                    .getResultList().size(), 3);
        });
    }

    @Test
    public void testIsNullQuery() {
        doWithEntityManager((em) -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);

            Metamodel m = em.getMetamodel();
            EntityType<Product> product = m.entity(Product.class);
            Root<Product> root = criteria.from(product);
            criteria.select(root);
            criteria.where(builder.isNull(root.get(Product_.description)));

            criteria.select(root);

            assertEquals(em.createQuery(criteria).getResultList().size(), 0);
        });
    }

    @Test
    public void testAndQuery() {
        doWithEntityManager((em) -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);

            Metamodel m = em.getMetamodel();
            EntityType<Product> product = m.entity(Product.class);
            Root<Product> root = criteria.from(product);
            criteria.select(root);
            criteria.where(
                    builder.and(
                            builder.lessThanOrEqualTo(
                                    root.get(Product_.price),
                                    builder.parameter(Double.class, "price")
                            ),
                            builder.like(
                                    builder.lower(
                                            root.get(Product_.description)
                                    ),
                                    builder.lower(
                                        builder.parameter(String.class, "description")
                                    )
                            )
                    )
            );

            criteria.select(root);

            assertEquals(em.createQuery(criteria)
                    .setParameter("price", 10.0)
                    .setParameter("description", "%mOUse")
                    .getResultList().size(), 1);
        });
    }

    @Test
    public void testOrQuery() {
        doWithEntityManager((em) -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);

            Metamodel m = em.getMetamodel();
            EntityType<Product> product = m.entity(Product.class);
            Root<Product> root = criteria.from(product);
            criteria.select(root);
            criteria.where(
                    builder.or(
                            builder.lessThanOrEqualTo(
                                    root.get(Product_.price),
                                    builder.parameter(Double.class, "price")
                            ),
                            builder.like(
                                    builder.lower(
                                            root.get(Product_.description)
                                    ),
                                    builder.lower(
                                            builder.parameter(String.class, "description")
                                    )
                            )
                    )
            );

            criteria.select(root);

            assertEquals(em.createQuery(criteria)
                    .setParameter("price", 10.0)
                    .setParameter("description", "%mOUse")
                    .getResultList().size(), 3);
        });
    }

    @Test
    public void testDisjunctionQuery() {
        doWithEntityManager((em) -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);

            Metamodel m = em.getMetamodel();
            EntityType<Product> product = m.entity(Product.class);
            Root<Product> root = criteria.from(product);
            criteria.select(root);
            criteria.where(
                    builder.or(
                            builder.and(
                                    builder.lessThanOrEqualTo(
                                            root.get(Product_.price),
                                            builder.parameter(Double.class, "price_1")
                                    ),
                                    builder.like(
                                            builder.lower(
                                                    root.get(Product_.description)
                                            ),
                                            builder.lower(
                                                    builder.parameter(String.class, "desc_1")
                                            )
                                    )
                            ),
                            builder.and(
                                    builder.greaterThan(
                                            root.get(Product_.price),
                                            builder.parameter(Double.class, "price_2")
                                    ),
                                    builder.like(
                                            builder.lower(
                                                    root.get(Product_.description)
                                            ),
                                            builder.lower(
                                                    builder.parameter(String.class, "desc_2")
                                            )
                                    )
                            )
                    )
            );

            criteria.select(root);

            assertEquals(em.createQuery(criteria)
                    .setParameter("price_1", 10.0)
                    .setParameter("desc_1", "%mOUse")
                    .setParameter("price_2", 999.0)
                    .setParameter("desc_2", "Computer")
                    .getResultList().size(), 2);
        });
    }

    @Test
    public void testPagination() {
        doWithEntityManager((em) -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);

            Root<Product> root = criteria.from(Product.class);
            criteria.select(root);
            TypedQuery<Product> query = em.createQuery(criteria);
            query.setFirstResult(2);
            query.setMaxResults(2);

            assertEquals(query.getResultList().size(), 2);
        });
    }

    @Test
    public void testUniqueResult() {
        doWithEntityManager((em) -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);

            Root<Product> root = criteria.from(Product.class);
            criteria.select(root);
            criteria.where(builder.equal(root.get(Product_.price),
                    builder.parameter(Double.class, "price")));

            assertEquals(em.createQuery(criteria)
                    .setParameter("price", 14.95)
                    .getSingleResult().getName(), "SuperDetect");
        });
    }

    @Test(expectedExceptions = NonUniqueResultException.class)
    public void testUniqueResultWithException() {
        doWithEntityManager((em) -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Product> criteria = builder.createQuery(Product.class);

            Root<Product> root = criteria.from(Product.class);
            criteria.select(root);
            criteria.where(builder.greaterThan(root.get(Product_.price),
                    builder.parameter(Double.class, "price")));

            em.createQuery(criteria)
                    .setParameter("price", 14.95)
                    .getSingleResult();
            fail("Should have thrown an exception");
        });
    }

    private void doWithEntityManager(Consumer<EntityManager> command) {
        EntityManager em = JPASessionUtil.getEntityManager(Config.PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();

        command.accept(em);
        if (em.getTransaction().isActive()) {
            if (!em.getTransaction().getRollbackOnly()) {
                em.getTransaction().commit();
            } else {
                em.getTransaction().rollback();
            }
        }
        em.close();
    }
}
