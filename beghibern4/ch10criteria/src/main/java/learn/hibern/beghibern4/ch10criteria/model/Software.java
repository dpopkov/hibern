package learn.hibern.beghibern4.ch10criteria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
public class Software extends Product implements Serializable {
    @Column(nullable = false)
    @NotNull
    private String version;

    public Software() {
    }

    public Software(Supplier supplier, @NotNull String name, @NotNull String description,
                    @NotNull Double price, @NotNull String version) {
        super(supplier, name, description, price);
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Software)) return false;
        if (!super.equals(o)) return false;

        Software software = (Software) o;

        return getVersion() != null ? getVersion().equals(software.getVersion()) : software.getVersion() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getVersion() != null ? getVersion().hashCode() : 0);
        return result;
    }
}
