package learn.hibern.beghibern4.ch07.events;

import javax.persistence.PrePersist;

public class UserAccountListener {
    @PrePersist
    public void setPasswordHash(Object o) {
        UserAccount u = (UserAccount) o;
        if (u.getSalt() == null || u.getSalt() == 0) {
            u.setSalt((int) (Math.random() * 65535));
        }
        u.setPasswordHash(u.getPassword().hashCode() * u.getSalt());
    }
}
