package learn.hibern.beghibern4.ch07.events;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
@EntityListeners({UserAccountListener.class})
public class UserAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    @Transient
    private String password;
    private Integer salt;
    private Integer passwordHash;

    public boolean validPassword(String password) {
        return password.hashCode() * salt == passwordHash;
    }
}
