package learn.hibern.beghibern4.ch07.events;

import learn.hibern.beghibern4.util.jpa.JPASessionUtil;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.*;

public class UserAccountListenerTest {
    public static final String PERSISTENCE_UNIT_NAME = "ch07jpa";

    @Test
    public void testSetPasswordHash() {
        UserAccount u = new UserAccount();
        u.setName("Alice");
        u.setPassword("123");
        assertNull(u.getSalt());
        assertNull(u.getPasswordHash());

        EntityManager em = JPASessionUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        em.persist(u);
        final Integer userAccountId = u.getId();
        assertNotNull(userAccountId);
        em.getTransaction().commit();
        em.close();

        em = JPASessionUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        UserAccount u2 = em.find(UserAccount.class, userAccountId);
        assertNotNull(u2);
        assertEquals(u2.getName(), "Alice");
        assertNotNull(u2.getSalt());
        assertNotNull(u2.getPasswordHash());
        assertTrue(u2.validPassword("123"));
        assertFalse(u2.validPassword("1234"));
        em.getTransaction().commit();
        em.close();
    }
}
