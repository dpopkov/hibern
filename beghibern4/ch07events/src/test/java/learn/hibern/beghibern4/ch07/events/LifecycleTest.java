package learn.hibern.beghibern4.ch07.events;

import learn.hibern.beghibern4.util.jpa.JPASessionUtil;
import org.testng.Reporter;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.*;

public class LifecycleTest {

    public static final String PERSISTENCE_UNIT_NAME = "ch07jpa";

    @Test
    public void testLifecycle() {
        Integer id;

        /* Saving Entity */
        EntityManager em = JPASessionUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        LifecycleThing thing1 = new LifecycleThing();
        thing1.setName("Thing 1");
        em.persist(thing1);
        id = thing1.getId();
        em.getTransaction().commit();
        em.close();

        /* Loading non-existing Entity */
        em = JPASessionUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        LifecycleThing thing2 = em.find(LifecycleThing.class, id + 1000);
        assertNull(thing2);
        Reporter.log("attempted to load nonexistent reference");

        /* Loading existing entity and updating */
        thing2 = em.find(LifecycleThing.class, id);
        assertNotNull(thing2);
        assertEquals(thing1, thing2);
        thing2.setName("Thing 2");
        em.getTransaction().commit();
        em.close();

        em = JPASessionUtil.getEntityManager(PERSISTENCE_UNIT_NAME);
        em.getTransaction().begin();
        LifecycleThing thing3 = em.find(LifecycleThing.class, id);
        assertNotNull(thing3);
        assertEquals("Thing 2", thing3.getName());
        assertEquals(thing2, thing3);
        em.remove(thing3);
        em.getTransaction().commit();
        em.close();

        /* Assert that every lifecycle method has been called at least once. */
        assertEquals(LifecycleThing.lifecycleCalls.nextClearBit(0), 7);
    }
}
