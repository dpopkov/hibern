package learn.hibern.beghibern4.ch06mapping.primarykey;

import learn.hibern.beghibern4.util.hibernate.SessionUtil;
import org.hibernate.Session;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class BookTest {

    @Test
    public void testSave() {
        Book book = new Book("Beginning Hibernate", 234);
        try (Session session = SessionUtil.openSession()) {
            session.beginTransaction();
            session.save(book);
            session.getTransaction().commit();
        }
        System.out.println("Saved: " + book);
    }
}
