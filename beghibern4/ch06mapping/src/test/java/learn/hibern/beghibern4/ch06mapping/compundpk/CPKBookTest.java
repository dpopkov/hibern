package learn.hibern.beghibern4.ch06mapping.compundpk;

import learn.hibern.beghibern4.util.hibernate.SessionUtil;
import org.hibernate.Session;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CPKBookTest {

    @Test
    public void testSave() {
        ISBN isbn = new ISBN();
        isbn.setGroup(1);
        isbn.setPublisher(2);
        isbn.setTitle(3);
        isbn.setCheckdigit(4);
        CPKBook cpkBook = new CPKBook();
        cpkBook.setName("BookName");
        cpkBook.setId(isbn);
        try (Session session = SessionUtil.openSession()) {
            session.beginTransaction();
            session.save(cpkBook);
            session.getTransaction().commit();
        }

        try (Session session = SessionUtil.openSession()) {
            CPKBook book2 = session.get(CPKBook.class, isbn);
            assertNotNull(book2);
            assertEquals("BookName", book2.getName());
            assertEquals(1, book2.getId().getGroup());
            assertEquals(2, book2.getId().getPublisher());
            assertEquals(3, book2.getId().getTitle());
            assertEquals(4, book2.getId().getCheckdigit());
        }
        System.out.println("Done");
    }
}
