package learn.hibern.beghibern4.ch06mapping.onetomanywithlinkingtable;

import learn.hibern.beghibern4.util.hibernate.SessionUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class PublisherBook2Test {

    @Test
    public void testSave() {
        Publisher publisher = new Publisher();
        Long publisherId;
        publisher.setName("OReilly");
        publisher.addBook(new Book2("Thinking in Java"));
        publisher.addBook(new Book2("Effective Java"));
        publisher.addBook(new Book2("Core Java"));
        try (Session session = SessionUtil.openSession()) {
            final Transaction tx = session.beginTransaction();
            session.save(publisher);
            publisherId = publisher.getId();
            tx.commit();
        }

        try (Session session = SessionUtil.openSession()) {
            Publisher publisher2 = session.get(Publisher.class, publisherId);
            assertNotNull(publisher2);
            assertEquals("OReilly", publisher2.getName());

            List<Book2> books = publisher2.getBooks();
            assertEquals(3, books.size());
            assertEquals("Core Java", books.get(0).getTitle());
            assertEquals("Effective Java", books.get(1).getTitle());
            assertEquals("Thinking in Java", books.get(2).getTitle());
        }
    }
}
