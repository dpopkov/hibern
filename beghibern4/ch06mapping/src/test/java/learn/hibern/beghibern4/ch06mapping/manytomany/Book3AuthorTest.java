package learn.hibern.beghibern4.ch06mapping.manytomany;

import learn.hibern.beghibern4.util.hibernate.SessionUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class Book3AuthorTest {

    @Test
    public void testSave() {
        final Book3 book = new Book3("Core Java");
        final Author horstmann = new Author("Cay Horstmann");
        final Author gosling = new Author("James Gosling");
        book.addAuthor(horstmann);
        book.addAuthor(gosling);
        Long bookId;
        try (Session session = SessionUtil.openSession()) {
            final Transaction tx = session.beginTransaction();

            session.save(book);
            bookId = book.getId();
            assertNotNull(bookId);
            assertNotNull(horstmann.getId());
            assertNotNull(gosling.getId());

            tx.commit();
        }

        try (Session session = SessionUtil.openSession()) {
            Book3 b = session.get(Book3.class, bookId);
            assertNotNull(b);
            assertEquals("Core Java", b.getTitle());
            assertEquals(2, b.getAuthors().size());
        }
    }
}
