package learn.hibern.beghibern4.ch06mapping.onetoone;

import learn.hibern.beghibern4.util.hibernate.SessionUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class UserAddressTest {

    private Long userId;
    private Long addressId;

    @Test
    public void testSave() {
        Address address = new Address("City", "Street");
        User user = new User("Username");
        user.addAddress(address);

        try (Session session = SessionUtil.openSession()) {
            final Transaction tx = session.beginTransaction();

            session.save(user);
            userId = user.getId();
            assertNotNull(userId);
            addressId = user.getAddress().getId();

            tx.commit();
        }

        try (Session session = SessionUtil.openSession()) {
            User user2 = session.get(User.class, userId);
            assertEquals("Username", user2.getName());

            Address address2 = user2.getAddress();
            assertNotNull(address2);
            assertEquals(addressId, address2.getId());
            assertEquals("City", address2.getCity());
        }
        System.out.println("testSave finished");
    }

    @Test(dependsOnMethods = "testSave")
    public void testRemove() {
        try (Session session = SessionUtil.openSession()) {
            final Transaction tx = session.beginTransaction();

            User user = session.get(User.class, userId);
            session.delete(user);

            tx.commit();
        }

        try (Session session = SessionUtil.openSession()) {
            User user2 = session.get(User.class, userId);
            assertNull(user2);

            Address address2 = session.get(Address.class, addressId);
            assertNull(address2);
        }
        System.out.println("testRemove finished");
    }
}
